﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour {

    public VideoPlayer video;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StopVideo ()
    {
        video.Stop();
    }

    public void PlayVideo ()
    {
        if (video.isPlaying)
        {

        }
        else
        {
            video.Play();
        }
    }

    IEnumerator StopThenPlay ()
    {
        StopVideo();
        yield return null;
        PlayVideo();
        yield return null;
    }
}
