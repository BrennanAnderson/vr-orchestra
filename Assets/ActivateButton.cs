﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateButton : MonoBehaviour {

    public Button myButton;
    public bool pressHToRecord = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (Input.GetKeyDown(KeyCode.H))
        {
            if (pressHToRecord)
            {
                PressButton();
            }
        }
	}

    void PressButton ()
    {
        myButton.onClick.Invoke();
    }

}
