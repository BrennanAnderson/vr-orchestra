﻿using UnityEngine;
using UnityEngine.UI;

namespace Weelco.VRKeyboard {

    public class VRKeyboardDemo : MonoBehaviour {

        public int maxOutputChars = 30;

        public Text inputFieldLabel;
        private VRKeyboardFull keyboardFull;
        private VRKeyboardExtra keyboardExtra;
        private VRKeyboardLite keyboardLite;        

        void Start() {
            //inputFieldLabel = transform.FindChild("InputField/Text").GetComponent<Text>();
            keyboardFull = transform.Find("VR_Keyboard_Full").GetComponent<VRKeyboardFull>();
            //keyboardExtra = transform.Find("VR_Keyboard_Extra").GetComponent<VRKeyboardExtra>();
            //keyboardLite = transform.Find("VR_Keyboard_Lite").GetComponent<VRKeyboardLite>();

            keyboardFull.Init();
            //keyboardExtra.Init();
            //keyboardLite.Init();

            keyboardFull.OnVRKeyboardBtnClick += HandleClick;
            //keyboardExtra.OnVRKeyboardBtnClick += HandleClick;
            //keyboardLite.OnVRKeyboardBtnClick += HandleClick;
        }

        void OnDestroy() {
            if (keyboardFull) {
                keyboardFull.OnVRKeyboardBtnClick -= HandleClick;
            }

            if (keyboardExtra) {
                keyboardExtra.OnVRKeyboardBtnClick -= HandleClick;
            }

            if (keyboardLite) {
                keyboardLite.OnVRKeyboardBtnClick -= HandleClick;
            }
        }        

//        void OnGUI() {
//            if (GUI.Button(new Rect(10, 10, 120, 30), "VR Keyboard Full")) {
//                keyboardFull.gameObject.SetActive(true);
//                keyboardExtra.gameObject.SetActive(false);
//                keyboardLite.gameObject.SetActive(false);
//            }
//
//            if (GUI.Button(new Rect(140, 10, 130, 30), "VR Keyboard Extra")) {
//                keyboardExtra.gameObject.SetActive(true);
//                keyboardFull.gameObject.SetActive(false);
//                keyboardLite.gameObject.SetActive(false);
//            }
//
//            if (GUI.Button(new Rect(280, 10, 120, 30), "VR Keyboard Lite")) {
//                keyboardLite.gameObject.SetActive(true);
//                keyboardFull.gameObject.SetActive(false);
//                keyboardExtra.gameObject.SetActive(false);
//            }
//        }

        private void HandleClick(string value) {
            if (value.Equals(VRKeyboardData.BACK)) {
                BackspaceKey();
            }
            else if (value.Equals(VRKeyboardData.ENTER)) {
                EnterKey();
            }
            else {
                TypeKey(value);
            }
        }

        private void BackspaceKey() {
            if (inputFieldLabel.text.Length >= 1) {
                inputFieldLabel.text = inputFieldLabel.text.Remove(inputFieldLabel.text.Length - 1, 1);
            }
        }    

        private void EnterKey() {
            // Add enter key handler
        }

        private void TypeKey(string value) {
            char[] letters = value.ToCharArray();
            for (int i = 0; i < letters.Length; i++) {
                TypeKey(letters[i]);
            }
        }

        private void TypeKey(char key) {
            if (inputFieldLabel.text.Length < maxOutputChars) {
                inputFieldLabel.text += key.ToString();
            }
        }   
 
        public void SetInputField (Text text)
        {
            inputFieldLabel = text;
        }
    }
}