﻿//using UnityEditor;
//using UnityEngine;
//using UnityEngine.EventSystems;
//using UnityEngine.UI;
//
//namespace Weelco.Utils {
//
//    public class VRInputModule : BaseInputModule {
//        
//        public float timeToLookPress;
//        public Image circleProgressBar;
//
//        private PointerEventData lookData;
//        private GameObject lastActiveButton;
//        private float lookTimer;
//
//        private bool guiRaycastHit;
//
//        private GameObject currentLook;
//        private GameObject currentPressed;
//        private float nextAxisActionTime;
//        
//        protected bool pressed = false;        
//                
//        public void SimulatePress() {
//            ClearSelection();
//
//            lookData.pressPosition = lookData.position;
//            lookData.pointerPressRaycast = lookData.pointerCurrentRaycast;
//            lookData.pointerPress = null;
//
//            if (currentLook != null) {
//                currentPressed = currentLook;
//                GameObject newPressed = null;
//                newPressed = ExecuteEvents.ExecuteHierarchy(currentPressed, lookData, ExecuteEvents.pointerDownHandler);
//
//                if (newPressed == null) {
//                    newPressed = ExecuteEvents.ExecuteHierarchy(currentPressed, lookData, ExecuteEvents.pointerClickHandler);
//                    if (newPressed != null) {
//                        currentPressed = newPressed;
//                    }
//                }
//                else {
//                    currentPressed = newPressed;
//                    ExecuteEvents.Execute(newPressed, lookData, ExecuteEvents.pointerClickHandler);
//                }
//
//                ExecutePointerUp();
//            }
//        }
//        
//        public void ExecutePointerUp() {
//            if (!pressed)
//                ExecuteEvents.ExecuteHierarchy(currentPressed, lookData, ExecuteEvents.pointerUpHandler);
//        }
//
//        public override void Process() {
//
//            SendUpdateEventToSelectedObject();
//
//            PointerEventData lookData = GetLookPointerEventData();
//            currentLook = lookData.pointerCurrentRaycast.gameObject;
//
//            if (currentLook == null) {
//                ClearSelection();
//            }
//
//            HandlePointerExitAndEnter(lookData, currentLook);
//
//            if ((currentLook != null) && (timeToLookPress > 0)) {
//                bool clickable = false;
//                if (currentLook.transform.gameObject.GetComponent<Button>() != null) clickable = true;
//                if (currentLook.transform.parent != null) {
//                    if (currentLook.transform.parent.gameObject.GetComponent<Button>() != null) clickable = true;
//                    if (currentLook.transform.parent.gameObject.GetComponent<Toggle>() != null) clickable = true;
//                    if (currentLook.transform.parent.gameObject.GetComponent<Slider>() != null) clickable = true;
//                    if (currentLook.transform.parent.parent != null) {
//                        if (currentLook.transform.parent.parent.gameObject.GetComponent<Slider>() != null) {
//                            if (currentLook.name != "Handle") clickable = true;
//                        }
//                        if (currentLook.transform.parent.parent.gameObject.GetComponent<Toggle>() != null) clickable = true;
//                    }
//                }
//
//                if (clickable) {
//                    if (lastActiveButton == currentLook) {
//                        if (circleProgressBar) {
//                            if (circleProgressBar.isActiveAndEnabled) {
//                                circleProgressBar.fillAmount = (Time.realtimeSinceStartup - lookTimer) / timeToLookPress;
//                            }
//                            else if (Time.realtimeSinceStartup - lookTimer > 0) {
//                                circleProgressBar.fillAmount = 0f;
//                                circleProgressBar.gameObject.SetActive(true);
//                            }
//                        }
//
//                        if (Time.realtimeSinceStartup - lookTimer > timeToLookPress) {
//                            if (circleProgressBar) {
//                                circleProgressBar.gameObject.SetActive(false);
//                            }
//                            SimulatePress();
//                            lookTimer = Time.realtimeSinceStartup + timeToLookPress * 3f;
//                        }
//                    }
//                    else {
//                        lastActiveButton = currentLook;
//                        lookTimer = Time.realtimeSinceStartup;
//                        if (circleProgressBar && circleProgressBar.isActiveAndEnabled) {
//                            circleProgressBar.gameObject.SetActive(false);
//                        }
//                    }
//                }
//                else {
//                    lastActiveButton = null;
//                    if (circleProgressBar && circleProgressBar.isActiveAndEnabled) {
//                        circleProgressBar.gameObject.SetActive(false);
//                    }
//                    ClearSelection();
//                }
//            }
//            else {
//                if (circleProgressBar) {
//                    circleProgressBar.gameObject.SetActive(false);
//                }
//                lastActiveButton = null;
//                ClearSelection();
//            }
//        }
//
//        private PointerEventData GetLookPointerEventData() {
//            if (lookData == null) {
//                lookData = new PointerEventData(eventSystem);
//            }
//            lookData.Reset();
//            lookData.delta = Vector2.zero;
//            lookData.position = getLookPosition();
//            lookData.scrollDelta = Vector2.zero;
//            eventSystem.RaycastAll(lookData, m_RaycastResultCache);
//            lookData.pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
//            if (lookData.pointerCurrentRaycast.gameObject != null) {
//                guiRaycastHit = true;
//            }
//            else {
//                guiRaycastHit = false;
//            }
//            m_RaycastResultCache.Clear();
//            return lookData;
//        }
//
//        private Vector2 getLookPosition() {
//            Vector2 point;
//            point = new Vector2(Screen.width / 2, Screen.height / 2);
//            if (PlayerSettings.virtualRealitySupported) {
//                point = new Vector2(UnityEngine.VR.VRSettings.eyeTextureWidth / 2, UnityEngine.VR.VRSettings.eyeTextureHeight / 2);
//            }
//            else {
//                point = new Vector2(Screen.width / 2, Screen.height / 2);
//            }
//            return point;
//        }
//
//        private void ClearSelection() {
//            if (eventSystem.currentSelectedGameObject) {
//                eventSystem.SetSelectedGameObject(null);
//            }
//        }
//
//        private bool SendUpdateEventToSelectedObject() {
//
//            if (eventSystem.currentSelectedGameObject == null)
//                return false;
//            BaseEventData data = GetBaseEventData();
//            ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);
//            return data.used;
//        }
//    }
//}