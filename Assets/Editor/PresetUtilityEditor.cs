﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PresetUtility))]
public class PresetUtilityEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PresetUtility myScript = (PresetUtility)target;
        if (GUILayout.Button("Save Instrument Set"))
        {
            myScript.SaveInstrumentSet();
        }
        if (GUILayout.Button("Save Song"))
        {
            myScript.SaveSong();
        }
    }
}
