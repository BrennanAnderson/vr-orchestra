﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstrumentModeBehaviors : MonoBehaviour {

    private ModeManager.controllerModeEnum controllerMode;

    void Awake ()
    {
        controllerMode = ModeManager.Instance.GetControllerMode();
    }

    // Use this for initialization
    void Start()
    {
        SetControllerModeBehavior();
    }

    // Update is called once per frame
    void Update()
    {
        CheckControllerMode();
    }

    // TODO - make this into an event so I don't need to check in update
    void CheckControllerMode()
    {
        if (controllerMode != ModeManager.Instance.GetControllerMode())
        {
            SetControllerModeBehavior();
        }
    }

    // Have all of the behaviors in their own function rather than in the check function. This lets us set the behaviors at any time and not be dependent on the check function
    // This is needed for initialization so that the instruments have the correct behaviors when you load them in.
    void SetControllerModeBehavior()
    {
        controllerMode = ModeManager.Instance.GetControllerMode();
        switch (controllerMode)
        {
            case ModeManager.controllerModeEnum.grabMode:
                GetComponent<InteractableInstrument>().isGrabbable = true;
                GetComponent<InteractableInstrument>().isUsable = false;
                GetComponent<InteractableInstrument>().touchHighlightColor = Color.cyan;
                transform.GetChild(5).Find("VolumeSlider").gameObject.SetActive(false);   // This deactivates the volume fader on the instrument
                break;

            case ModeManager.controllerModeEnum.parameterMode:
                GetComponent<InteractableInstrument>().isGrabbable = true;
                GetComponent<InteractableInstrument>().isUsable = true;
                GetComponent<InteractableInstrument>().touchHighlightColor = Color.red;
                transform.GetChild(5).Find("VolumeSlider").gameObject.SetActive(true);   // This activates the volume fader on the instrument
                break;

            case ModeManager.controllerModeEnum.automationMode:
                GetComponent<InteractableInstrument>().isGrabbable = true;
                GetComponent<InteractableInstrument>().isUsable = true;
                GetComponent<InteractableInstrument>().touchHighlightColor = Color.green;
                transform.GetChild(5).Find("VolumeSlider").gameObject.SetActive(false);   // This deactivates the volume fader on the instrument
                break;
        }
    }
}
