﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuantizedUIScroll : MonoBehaviour {

	public RectTransform panel;
	public RectTransform activeRow;
	public RectTransform activeItem;
    public Text header;
	public float normalScale;
	public float magnification = 1.25f;
	public float scrollTime = 0.05f;

	private float scrollSpeed = 1f;
	private bool isMoving = false;
    private float yDifference;
    private float xDifference;
	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < panel.childCount; i++)
		{
            DuplicateFirstAndMakeLast(panel.GetChild(i));
            DuplicateSecondToLastAndMakeFirst(panel.GetChild(i));

			for (int k = 0; k < panel.GetChild(i).childCount; k++)
			{
				Color currentColor = panel.GetChild(i).GetChild(k).GetComponent<Image>().color;
				panel.GetChild(i).GetChild(k).GetComponent<Image>().color = new Color(currentColor.r, currentColor.g, currentColor.b, 0.5f);
                panel.GetChild(i).GetChild(k).localScale = new Vector3(normalScale, normalScale, normalScale);
			}
		}

        activeRow = panel.GetChild(1).GetComponent<RectTransform>();
        activeItem = activeRow.GetChild(2).GetComponent<RectTransform>();

        activeItem.localScale = new Vector3(magnification, magnification, magnification);
		Color myColor = activeItem.GetComponent<Image>().color;
		activeItem.GetComponent<Image>().color = new Color (myColor.r, myColor.g, myColor.b, 1f);
        activeItem.localScale = new Vector3(magnification, magnification, magnification);

        yDifference = Mathf.Abs(panel.GetComponent<VerticalLayoutGroup>().padding.top);
        xDifference = Mathf.Abs(panel.GetChild(0).GetComponent<HorizontalLayoutGroup>().padding.left) * 0.5f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!isMoving)
		{
            SetCorrectScaleAndColor();

            header.text = activeRow.name + ":";
		}
	}

    public void MoveSelectionUp()
    {
        if (!isMoving)
        {
            StartCoroutine(MoveUp());
        }
    }

    public void MoveSelectionDown()
    {
        if (!isMoving)
		{
            StartCoroutine(MoveDown());
        }
    }

    public void MoveSelectionLeft()
    {
        if (!isMoving)
		{
            StartCoroutine(MoveLeft());
        }
    }

    public void MoveSelectionRight()
    {
        if (!isMoving)
		{
            StartCoroutine(MoveRight());
        }
    }

	IEnumerator MoveRight ()
	{
        DeleteLast(activeRow);
        DuplicateSecondAndMakeLast(activeRow);

		isMoving = true;
		Vector3 startPos = activeRow.anchoredPosition3D;
        Vector3 endPos = new Vector3(activeRow.anchoredPosition3D.x - xDifference, activeRow.anchoredPosition3D.y, activeRow.anchoredPosition3D.z);
		float currentTime = 0f;
		float normalizedValue = 0f;

		// Set scale of item to small before we move
		activeItem.localScale = new Vector3 (normalScale, normalScale, normalScale);
		Color currentColor = activeItem.GetComponent<Image>().color;
		activeItem.GetComponent<Image>().color = new Color (currentColor.r, currentColor.g, currentColor.b, 0.5f);

		// Move the row
		while (currentTime <= scrollTime) 
		{ 
			currentTime += Time.deltaTime; 
			normalizedValue=currentTime/scrollTime;
			activeRow.anchoredPosition3D = Vector3.Lerp(startPos, endPos, normalizedValue);
			yield return null;
		}

		activeRow.anchoredPosition3D = endPos;

		// change order of transforms
		for (int i = 0; i < activeRow.transform.childCount; i++)
		{
			int siblingIndex = activeRow.transform.GetChild(i).GetSiblingIndex();
			activeRow.transform.GetChild(i).SetSiblingIndex(siblingIndex + 1);
		}

        // Since we changed the order of the transforms, and they are under a layout, their positioning will be shifted.
        // Shift the position back now that we have animated the item moving
		activeRow.anchoredPosition3D = startPos;

        // Set the active item to the item in the middle
        activeItem = activeRow.GetChild(2).GetComponent<RectTransform>();

		isMoving = false;

        

		yield return null;
	}

	IEnumerator MoveLeft ()
	{
        DeleteFirst(activeRow);
        DuplicateSecondToLastAndMakeFirst(activeRow);

		isMoving = true;
		Vector3 startPos = activeRow.anchoredPosition3D;
        Vector3 endPos = new Vector3(activeRow.anchoredPosition3D.x + xDifference, activeRow.anchoredPosition3D.y, activeRow.anchoredPosition3D.z);
		float currentTime = 0f;
		float normalizedValue = 0f;

		activeItem.localScale = new Vector3 (normalScale, normalScale, normalScale);
		Color currentColor = activeItem.GetComponent<Image>().color;
		activeItem.GetComponent<Image>().color = new Color (currentColor.r, currentColor.g, currentColor.b, 0.5f);

		while (currentTime <= scrollTime) 
		{ 
			currentTime += Time.deltaTime; 
			normalizedValue=currentTime/scrollTime;
			activeRow.anchoredPosition3D = Vector3.Lerp(startPos, endPos, normalizedValue);
			yield return null;
		}

		activeRow.anchoredPosition3D = endPos;

		for (int i = activeRow.transform.childCount - 1; i > 0; i--)
		{
			int siblingIndex = activeRow.transform.GetChild(i).GetSiblingIndex();
			activeRow.transform.GetChild(i).SetSiblingIndex(siblingIndex - 1);
		}
		activeRow.anchoredPosition3D = startPos;
		activeItem = activeRow.GetChild(2).GetComponent<RectTransform>();
		

		isMoving = false;

        
        
		yield return null;
	}

	IEnumerator MoveDown()
	{
		isMoving = true;
		Vector3 startPos = panel.anchoredPosition3D;
        Vector3 endPos = new Vector3(panel.anchoredPosition3D.x, panel.anchoredPosition3D.y + yDifference, panel.anchoredPosition3D.z);
		float currentTime = 0f;
		float normalizedValue = 0f;

		activeItem.localScale = new Vector3 (normalScale, normalScale, normalScale);
		Color currentColor = activeItem.GetComponent<Image>().color;
		activeItem.GetComponent<Image>().color = new Color (currentColor.r, currentColor.g, currentColor.b, 0.5f);

		while (currentTime <= scrollTime) 
		{ 
			currentTime += Time.deltaTime; 
			normalizedValue=currentTime/scrollTime;
			panel.anchoredPosition3D = Vector3.Lerp(startPos, endPos, normalizedValue);
			yield return null;
		}
		panel.anchoredPosition3D = endPos;

		for (int i = 0; i < panel.transform.childCount; i++)
		{
			int siblingIndex = panel.GetChild(i).GetSiblingIndex();
			panel.GetChild(i).SetSiblingIndex(siblingIndex + 1);
		}
		panel.anchoredPosition3D = startPos;
		isMoving = false;
		activeRow = panel.GetChild(1).GetComponent<RectTransform>();
		activeItem = activeRow.GetChild(2).GetComponent<RectTransform>();
		
		yield return null;
	}

	IEnumerator MoveUp()
	{
		isMoving = true;
		Vector3 startPos = panel.anchoredPosition3D;
        Vector3 endPos = new Vector3(panel.anchoredPosition3D.x, panel.anchoredPosition3D.y - yDifference, panel.anchoredPosition3D.z);
		float currentTime = 0f;
		float normalizedValue = 0f;

		activeItem.localScale = new Vector3 (normalScale, normalScale, normalScale);
		Color currentColor = activeItem.GetComponent<Image>().color;
		activeItem.GetComponent<Image>().color = new Color (currentColor.r, currentColor.g, currentColor.b, 0.5f);

		while (currentTime <= scrollTime) 
		{ 
			currentTime += Time.deltaTime; 
			normalizedValue=currentTime/scrollTime;
			panel.anchoredPosition3D = Vector3.Lerp(startPos, endPos, normalizedValue);
			yield return null;
		}
		panel.anchoredPosition3D = endPos;

		for (int i = panel.transform.childCount - 1; i > 0; i--)
		{
			int siblingIndex = panel.GetChild(i).GetSiblingIndex();
			panel.GetChild(i).SetSiblingIndex(siblingIndex - 1);
		}
		panel.anchoredPosition3D = startPos;
		isMoving = false;
		activeRow = panel.GetChild(1).GetComponent<RectTransform>();
		activeItem = activeRow.GetChild(2).GetComponent<RectTransform>();
		
		yield return null;
	}

    void SetCorrectScaleAndColor ()
    {
        // Set active item and increase its scale
        
        activeItem.localScale = new Vector3(magnification, magnification, magnification);
        Color currentColor = activeItem.GetComponent<Image>().color;
        activeItem.GetComponent<Image>().color = new Color(currentColor.r, currentColor.g, currentColor.b, 1f);
    }

    void DuplicateFirstAndMakeLast (Transform row)
    {
        GameObject duplicate = Instantiate(row.GetChild(0).gameObject);
        duplicate.transform.SetParent(row, false);
        duplicate.transform.SetAsLastSibling();
        duplicate.transform.localScale = new Vector3(normalScale, normalScale, normalScale);
        Color myColor = duplicate.GetComponent<Image>().color;
        duplicate.GetComponent<Image>().color = new Color(myColor.r, myColor.g, myColor.b, 0.5f);
        duplicate.GetComponent<Image>().enabled = true;
        duplicate.name = duplicate.name.Replace("(Clone)", "");
    }

    void DuplicateSecondAndMakeLast(Transform row)
    {
        GameObject duplicate = Instantiate(row.GetChild(1).gameObject);
        duplicate.transform.SetParent(row, false);
        duplicate.transform.SetAsLastSibling();
        duplicate.transform.localScale = new Vector3(normalScale, normalScale, normalScale);
        Color myColor = duplicate.GetComponent<Image>().color;
        duplicate.GetComponent<Image>().color = new Color(myColor.r, myColor.g, myColor.b, 0.5f);
        duplicate.GetComponent<Image>().enabled = true;
        duplicate.name = duplicate.name.Replace("(Clone)", "");
    }
    void DuplicateSecondToLastAndMakeFirst(Transform row)
    {
        GameObject duplicate = Instantiate(row.GetChild(row.childCount-2).gameObject);
        duplicate.transform.SetParent(row, false);
        duplicate.transform.SetAsFirstSibling();
        duplicate.transform.localScale = new Vector3(normalScale, normalScale, normalScale);
        Color myColor = duplicate.GetComponent<Image>().color;
        duplicate.GetComponent<Image>().color = new Color(myColor.r, myColor.g, myColor.b, 0.5f);
        duplicate.GetComponent<Image>().enabled = true;
        duplicate.name = duplicate.name.Replace("(Clone)", "");
    }

    void DeleteFirst(Transform row)
    {
        Destroy(row.GetChild(0).gameObject);
    }
    void DeleteLast(Transform row)
    {
        Destroy(row.GetChild(row.childCount - 1).gameObject);
    }

}
