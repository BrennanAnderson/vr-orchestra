﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PresetUtility : MonoBehaviour {

	private FileInfo[] instrumentSetsInfo;
    private FileInfo[] songsInfo;
	private DirectoryInfo instrumentSetsDir;
    private DirectoryInfo songsDir;

	[SerializeField] PresetData presetData;

	void Start ()
	{
		instrumentSetsDir = new DirectoryInfo(Application.dataPath + "/Resources/InstrumentSets");
        songsDir = new DirectoryInfo(Application.dataPath + "/Resources/Songs");
		Refresh();
	}

	void Update ()
	{
		/*if (Input.GetKeyDown(KeyCode.Space))
		{
			SaveInstrumentSet();
		}

		if (Input.GetKeyDown(KeyCode.Return))
		{
            SaveSong();
		}*/
	}

	void Refresh ()
	{
		instrumentSetsInfo = instrumentSetsDir.GetFiles("*.asset");
        songsInfo = songsDir.GetFiles("*.asset");
	}

	public void SaveInstrumentSet ()
	{
		string filePath = Application.dataPath + "/Resources/InstrumentSets";
		if (!Directory.Exists(filePath))
			CreateSaveDirectoryInstrumentSets ();

		PresetData instrumentConfiguration = ScriptableObject.CreateInstance<PresetData>();
		instrumentConfiguration.instrument = new List<Instrument>();
		instrumentConfiguration.position = new List<Vector3>();
        instrumentConfiguration.instrumentColor = new List<Color>();
		for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
		{
			instrumentConfiguration.instrument.Add( PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentInfo>().instrument );
			instrumentConfiguration.position.Add( PresetManager.Instance.instrumentsContainer.GetChild(i).transform.position );
            instrumentConfiguration.instrumentColor.Add( PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentInfo>().color );
		}

		string fileName = string.Format("Assets/Resources/InstrumentSets/{1}.asset", filePath, PresetManager.Instance.currentPresetText.text);
		AssetDatabase.CreateAsset(instrumentConfiguration, fileName);
		Refresh();
	}

    public void SaveSong()
    {
        string filePath = Application.dataPath + "/Resources/Songs";
        if (!Directory.Exists(filePath))
            CreateSaveDirectoryInstrumentSets();

        PresetData instrumentConfiguration = ScriptableObject.CreateInstance<PresetData>();
        instrumentConfiguration.instrument = new List<Instrument>();
        instrumentConfiguration.audioEventName = new List<string>();
        instrumentConfiguration.audioFileName = new List<string>();
        instrumentConfiguration.audioDelay = new List<float>();
        instrumentConfiguration.is3D = new List<int>();
        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            instrumentConfiguration.instrument.Add(PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentInfo>().instrument);
            instrumentConfiguration.audioEventName.Add(PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentInfo>().audioEventName);
            instrumentConfiguration.audioFileName.Add(PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentInfo>().audioFileName);
            instrumentConfiguration.audioDelay.Add(PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentInfo>().audioDelay);
            instrumentConfiguration.is3D.Add(PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentInfo>().is3D);
        }

        string fileName = string.Format("Assets/Resources/Songs/{1}.asset", filePath, SongManager.Instance.currentSongText.text);
        AssetDatabase.CreateAsset(instrumentConfiguration, fileName);
        Refresh();
    }

	void CreateSaveDirectoryInstrumentSets ()
	{
		string filePath = Application.dataPath + "/Resources";
		if (!Directory.Exists(filePath))
			AssetDatabase.CreateFolder("Assets", "Resources");
		filePath += "/InstrumentSets";
		if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets/Resources", "InstrumentSets");
		AssetDatabase.Refresh();
	}

    void CreateSaveDirectorySongs()
    {
        string filePath = Application.dataPath + "/Resources";
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets", "Resources");
        filePath += "/Songs";
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets/Resources", "Songs");
        AssetDatabase.Refresh();
    }
}

#endif
