﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTipManager : MonoBehaviour {

    public Text toolNameText = null;
    public Text toolTipText = null;
    public string toolNameString = "Tool Tips!";
    public string toolTipString = "Useful info about the various tools will be displayed here once you hover over them.";

    public static ToolTipManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
