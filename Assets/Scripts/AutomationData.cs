﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomationData : MonoBehaviour {

	public GameObject AutomationNodePrefab;
	public GameObject AutomationLinePrefab;
    public GameObject AutomationNodesTransformPrefab;
    public GameObject AutomationLinesTransformPrefab;
	public Transform AutomationNodesTransform;
	public Transform AutomationLinesTransform;
	public bool isMoving = false;
	public bool isRecording = false;
	public float recordSampleTime = 0.5f;

    public enum automationMovementTypeEnum
    {
        None,
        Loop,
        PingPong
    }

    public automationMovementTypeEnum automationMovementType = automationMovementTypeEnum.None;

	public List<GameObject> AutomationNodes;
	private int currentNode = 0;
	private float recordStartTime = 0f;
    private bool canContinue = true;
    private int lastNodeCount = 0;
    private bool moveForward = true;

    public int numNodes = 0;


    void Awake ()
    {
        
    }

	// Use this for initialization
	void Start () 
	{
        AutomationNodesTransform = (Instantiate(AutomationNodesTransformPrefab) as GameObject).transform;
        AutomationNodesTransform.name = gameObject.name + "_AutomationNodes";
        AutomationNodesTransform.SetParent(AutomationManager.Instance.AutomationContainer);
        AutomationNodesTransform.gameObject.AddComponent<GripMove>();

        AutomationLinesTransform = (Instantiate(AutomationLinesTransformPrefab) as GameObject).transform;
        AutomationLinesTransform.name = gameObject.name + "_AutomationLines";
        AutomationLinesTransform.SetParent(AutomationManager.Instance.AutomationContainer);
        AutomationLinesTransform.gameObject.AddComponent<GripMove>();

        AutomationNodesTransform.GetComponent<GripMove>().enabled = false;
        AutomationLinesTransform.GetComponent<GripMove>().enabled = false;

		AutomationNodes = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (!isMoving && !isRecording && !LoopScroll.Instance.isTimelineMoving && !LoopScroll.Instance.timelinePlaying)
        {
            if (AutomationNodes.Count > 0)
            {
                AutomationNodes[0].transform.position = transform.position;
            }
        }

        if (AutomationNodes.Count > 1)
        {
            ConnectNodes();
        }

        TryRecordAutomation();
         
	}

	public void CreateAutomationNode ()
	{
		GameObject newNode = Instantiate(AutomationNodePrefab) as GameObject;
		newNode.transform.position = transform.position;
		newNode.transform.SetParent(AutomationNodesTransform);
		AutomationNodes.Add(newNode);

		if (AutomationNodes.Count > 1)
		{
			if (isRecording)
			{
				newNode.GetComponent<AutomationNodeInfo>().timestamp = (Time.time - recordStartTime);
			}
			else
			{
				newNode.GetComponent<AutomationNodeInfo>().timestamp = AutomationNodes[AutomationNodes.Count - 2].GetComponent<AutomationNodeInfo>().timestamp + 1f;
			}
		}

        newNode.GetComponent<AutomationNodeInfo>().myInstrumnent = gameObject;

		OrderNodes();
		ConnectNodes();
        LoopScroll.Instance.SetupNodesOnTimeline();
	}

    public void CreateAutomationNode(Vector3 pos)
    {
        numNodes++;
        GameObject newNode = Instantiate(AutomationNodePrefab) as GameObject;
        newNode.transform.position = pos;
        newNode.transform.SetParent(AutomationNodesTransform);
        AutomationNodes.Add(newNode);

        if (AutomationNodes.Count > 1)
        {
            if (isRecording)
            {
                newNode.GetComponent<AutomationNodeInfo>().timestamp = (Time.time - recordStartTime);
            }
            else
            {
                newNode.GetComponent<AutomationNodeInfo>().timestamp = AutomationNodes[AutomationNodes.Count - 2].GetComponent<AutomationNodeInfo>().timestamp + 1f;
            }
        }

        newNode.GetComponent<AutomationNodeInfo>().myInstrumnent = gameObject;

        OrderNodes();
        ConnectNodes();
        LoopScroll.Instance.SetupNodesOnTimeline();
    }

    public AutomationNodeInfo CreateAndReturnAutomationNode(Vector3 pos, float newNodeTimestamp, int order)
    {
        numNodes++;
        GameObject newNode = Instantiate(AutomationNodePrefab) as GameObject;
        newNode.transform.position = pos;
        newNode.transform.SetParent(AutomationNodesTransform);
        AutomationNodes.Add(newNode);

        newNode.GetComponent<AutomationNodeInfo>().timestamp = newNodeTimestamp;
        newNode.GetComponent<AutomationNodeInfo>().nodePosition = order;
        newNode.GetComponent<AutomationNodeInfo>().myInstrumnent = gameObject;

        for (int i = 0; i < AutomationNodes.Count; i++)
        {
            if (i == 0)
            {
                AutomationNodes[i].GetComponent<MeshRenderer>().material.color = Color.blue;
            }
            else if (AutomationNodes.Count > 1 && i == AutomationNodes.Count - 1)
            {
                AutomationNodes[i].GetComponent<MeshRenderer>().material.color = Color.red;
            }
            else
            {
                AutomationNodes[i].GetComponent<MeshRenderer>().material.color = Color.green;
            }
        }

        ConnectNodes();
        LoopScroll.Instance.SetupNodesOnTimeline();
        return newNode.GetComponent<AutomationNodeInfo>();
    }

    public void DeleteAutiomationNode (GameObject targetNode)
    {
        numNodes--;
        AutomationNodes.Remove(targetNode);
        Destroy(targetNode);
        OrderNodes();
        ConnectNodes();
        LoopScroll.Instance.SetupNodesOnTimeline();
    }

	public void OrderNodes ()
	{
		for (int i = 0; i < AutomationNodes.Count; i++)
		{
            if (i == 0)
            {
                AutomationNodes[i].GetComponent<MeshRenderer>().material.color = Color.blue;
            }
            else if (AutomationNodes.Count > 1 && i == AutomationNodes.Count - 1)
            {
                AutomationNodes[i].GetComponent<MeshRenderer>().material.color = Color.red;
            }
            else
            {
                AutomationNodes[i].GetComponent<MeshRenderer>().material.color = Color.green;
            }

			AutomationNodes[i].GetComponent<AutomationNodeInfo>().nodePosition = i;
		}
	}

	public void ConnectNodes ()
	{
        // This will check to see if we have deleted any nodes. If so, we need to delete and redraw the lines
        if (AutomationNodes.Count < lastNodeCount)
        {
            DeleteAllLines();
        }

        lastNodeCount = AutomationNodes.Count;

        for (int i = 0; i < AutomationNodes.Count; i++)
        {
            if (AutomationLinesTransform.childCount == i - 1)
            {
                if (i > 0)
                {
                    DrawLine(AutomationNodes[i - 1], AutomationNodes[i]);
                }
            }
            else
            {
                if (i > 0 && i < AutomationNodes.Count)
                {
                    // This checks to see if the nodes have moved. If they have, we need to update the line renderer.
                    if (AutomationNodes[i].transform.position != AutomationNodes[i].GetComponent<AutomationNodeInfo>().lastKnownPosition ||
                        AutomationNodes[i - 1].transform.position != AutomationNodes[i - 1].GetComponent<AutomationNodeInfo>().lastKnownPosition)
                    {
                        UpdateLine(AutomationLinesTransform.GetChild(i - 1).gameObject, AutomationNodes[i - 1], AutomationNodes[i]);
                    } 
                }
                
            }
        }
        
	}

	void DrawLine (GameObject node1, GameObject node2)
	{
        GameObject lineObject = Instantiate(AutomationLinePrefab) as GameObject;
        lineObject.transform.SetParent(AutomationLinesTransform);
        LineRenderer line = lineObject.GetComponent<LineRenderer>();

        line.SetPosition(0, node1.transform.position);
        line.SetPosition(1, node2.transform.position);
	}

    void UpdateLine(GameObject lineObject, GameObject node1, GameObject node2)
    {
        LineRenderer line = lineObject.GetComponent<LineRenderer>();

        line.SetPosition(0, node1.transform.position);
        line.SetPosition(1, node2.transform.position);
    }

    void DeleteAllLines ()
    {
        List<GameObject> tmp = new List<GameObject>();

        for (int i = 0; i < AutomationLinesTransform.childCount; i++)
        {
            tmp.Add(AutomationLinesTransform.GetChild(i).gameObject);
        }

        for (int i = 0; i < tmp.Count; i++)
        {
            GameObject tmpObj = tmp[i];
            tmp[i] = null;
            DestroyImmediate(tmpObj);
        }
    }

	public void SwapNodes (int indexA, int indexB)
	{
		GameObject tmp = AutomationNodes[indexA];
		//float tmpTime = AutomationNodes[indexA].GetComponent<AutomationNodeInfo>().timestamp;
		//AutomationNodes[indexA].GetComponent<AutomationNodeInfo>().timestamp = AutomationNodes[indexB].GetComponent<AutomationNodeInfo>().timestamp;
		AutomationNodes[indexA] = AutomationNodes[indexB];
		//AutomationNodes[indexB].GetComponent<AutomationNodeInfo>().timestamp = tmpTime;
		AutomationNodes[indexB] = tmp;

		OrderNodes();
        DeleteAllLines();
        ConnectNodes();
	}

	public void FollowAutomation ()
	{
		// Move object back to the start
		currentNode = 0;
		
        if (AutomationNodes.Count > 1 && !isMoving)
        {
            transform.position = AutomationNodes[0].transform.position;
            StartCoroutine(PlayAutomation());
        }
        else if (isMoving)
        {
            StartCoroutine(StopThenPlay());
        }
	}

    IEnumerator StopThenPlay ()
    {
        StopAutomation();
        yield return null;
        FollowAutomation();
        yield return null;
    }

	IEnumerator PlayAutomation ()
	{
        switch (automationMovementType)
        {
            case automationMovementTypeEnum.None :

                while (currentNode < AutomationNodes.Count - 1 && canContinue)
		        {
			        if (!isMoving)
			        {
				        StartCoroutine(MoveObjectToNode(AutomationNodes[currentNode], AutomationNodes[currentNode + 1]));
			        }
			        yield return null;
		        }
                if (!canContinue)
                {
                    canContinue = true;
                }
		        yield return null;

                break;

            case automationMovementTypeEnum.Loop :

                while (canContinue)
		        {
			        if (!isMoving)
			        {
				        StartCoroutine(MoveObjectToNode(AutomationNodes[currentNode], AutomationNodes[(int)Mathf.Repeat(currentNode + 1, AutomationNodes.Count)]));
			        }
			        yield return null;
		        }
                if (!canContinue)
                {
                    canContinue = true;
                }
		        yield return null;

                break;

            case automationMovementTypeEnum.PingPong :
                
                while (canContinue)
		        {
			        if (!isMoving)
			        {
                        if (moveForward)
                        {
                            if (currentNode == AutomationNodes.Count - 2)
                            {
                                moveForward = false;
                            }
                            StartCoroutine(MoveObjectToNode(AutomationNodes[currentNode], AutomationNodes[currentNode + 1]));
                        }
                        else
                        {
                            if (currentNode == 1)
                            {
                                moveForward = true;
                            }
                            StartCoroutine(MoveObjectToNode(AutomationNodes[currentNode], AutomationNodes[currentNode - 1]));
                        }
			        }
			        yield return null;
		        }
                if (!canContinue)
                {
                    canContinue = true;
                }
		        yield return null;
                break;
        }
        yield return null;
	}

	IEnumerator MoveObjectToNode (GameObject startNode, GameObject endNode)
	{
		// Move the object through a fixed duration
		float timeToMove = endNode.GetComponent<AutomationNodeInfo>().timestamp - startNode.GetComponent<AutomationNodeInfo>().timestamp;

        if (endNode.GetComponent<AutomationNodeInfo>().nodePosition == 0 && automationMovementType == automationMovementTypeEnum.Loop)
        {
            timeToMove = 1f;
        }
        else if (endNode.GetComponent<AutomationNodeInfo>().timestamp < startNode.GetComponent<AutomationNodeInfo>().timestamp)
        {
            timeToMove = startNode.GetComponent<AutomationNodeInfo>().timestamp - endNode.GetComponent<AutomationNodeInfo>().timestamp;
        }

		float t = 0f;
		isMoving = true;
		while (t < 1 && canContinue)
		{
			t += Time.deltaTime / timeToMove;
			transform.position = Vector3.Lerp(startNode.transform.position, endNode.transform.position, t);
			yield return null;
		}
        
		isMoving = false;
        if (!canContinue)
        {
            currentNode = 0;
        }
        else
        {
            currentNode = endNode.GetComponent<AutomationNodeInfo>().nodePosition;
        }

		yield return null;
	}

    public void StopAutomation ()
    {
        canContinue = false;
        if (AutomationNodes.Count > 1)
        {
            transform.position = AutomationNodes[0].transform.position;
        }
        StartCoroutine(StopAutomationCoroutine());
    }
    IEnumerator StopAutomationCoroutine ()
    {
        yield return null;
        canContinue = true;
        moveForward = true;
        yield return null;
    }

    void TryRecordAutomation ()
    {
        // This is called every frame that the instrument is picked up. The automation manager will dictate if the instrument is actually allowed to record automation or not
        if (gameObject == AutomationManager.Instance.GetSelectedInstrument())
        {
            if (GetComponent<SetTouched>().objectGrabbed)
            {
                if (!isRecording)
                {
                    AutomationManager.Instance.RecordAutomation();
                }
            }
            else
            {
                if (isRecording)
                {
                    AutomationManager.Instance.CancelRecord();
                }
            }
        }
    }

    // sampleTime is the time interval after which you record the instrument's position
    public void RecordAutomation (float sampleTime = 0.25f)
    {
        isRecording = true;
        if (isRecording)
        {
            StartCoroutine(RecordAutomationCoroutine(sampleTime));
        }
    }

    public void StopRecordAutomation ()
    {
        isRecording = false;
    }

	IEnumerator RecordAutomationCoroutine (float sampleTime)
	{
		if (AutomationNodes.Count > 1)
		{
			recordStartTime = Time.time - (AutomationNodes[AutomationNodes.Count - 1].GetComponent<AutomationNodeInfo>().timestamp + 1);
		}
		else
		{
			recordStartTime = Time.time;
		}

		while (isRecording)
		{
			CreateAutomationNode();
			yield return new WaitForSeconds(sampleTime);
		}
		yield return null;
	}

    public void ShowAutomation ()
    {
        for (int i = 0; i < AutomationNodesTransform.childCount; i++)
        {
            AutomationNodesTransform.GetChild(i).GetComponent<Renderer>().enabled = true;
            AutomationNodesTransform.GetChild(i).GetComponent<SphereCollider>().enabled = true;
        }

        AutomationLinesTransform.gameObject.SetActive(true);
    }

    public void HideAutomation ()
    {
        for (int i = 0; i < AutomationNodesTransform.childCount; i++)
        {
            AutomationNodesTransform.GetChild(i).GetComponent<Renderer>().enabled = false;
            AutomationNodesTransform.GetChild(i).GetComponent<SphereCollider>().enabled = false;
        }

        AutomationLinesTransform.gameObject.SetActive(false);
    }
}
