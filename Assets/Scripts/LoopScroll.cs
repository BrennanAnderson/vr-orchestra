﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoopScroll : MonoBehaviour {

    public RectTransform uiMask;
    public GameObject timelineImagePrefab;
    public Transform contentTransform;
    public Text timecodeText;
    public ScrollRect scrollRect;
    public GameObject automationNodePrefab;
    public List<GameObject> timelineAutomationNodes;

    public float secondScale = 8f;    // How many seconds exist in the full viewing area. Currently, 8 means that each large line in the graphic is one second.
    public float maskLeftEdge;
    public float maskRightEdge;
    private bool canSpawnImage = false;

    private float rightSpawnThreshold;
    private float leftSpawnThreshold;
    public float thresholdLength;

    private int numberRight;
    private int numberLeft;

    public bool timelinePlaying = false;
    public bool isTimelineMoving = false;

    public static LoopScroll Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

	// Use this for initialization
	void Start () 
    {
        maskLeftEdge = uiMask.rect.xMin;
        maskRightEdge = uiMask.rect.xMax;
        thresholdLength = maskLeftEdge + maskRightEdge;

        rightSpawnThreshold = 0;
        leftSpawnThreshold = 0;

        timelineAutomationNodes = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (contentTransform.localPosition.x > rightSpawnThreshold)
        {
            // re-compute threshold values
            rightSpawnThreshold += thresholdLength;
            leftSpawnThreshold = rightSpawnThreshold - thresholdLength;
            SpawnNewImageLeft();
        }
        else if (contentTransform.localPosition.x < leftSpawnThreshold)
        {
            // re-compute threshold values
            leftSpawnThreshold -= thresholdLength;
            rightSpawnThreshold = leftSpawnThreshold + thresholdLength;
            SpawnNewImageRight();
        }

        SetTimecode();
	}
    
    void LateUpdate ()
    {
        Vector3 pos = new Vector3(Mathf.Clamp(contentTransform.localPosition.x, float.MinValue, LoopScroll.Instance.thresholdLength - LoopScroll.Instance.thresholdLength), contentTransform.localPosition.y, contentTransform.localPosition.z);
        contentTransform.localPosition = pos;
    }

    void SpawnNewImageRight ()
    {
        numberRight++;
        numberLeft--;

        GameObject newImage = Instantiate(timelineImagePrefab) as GameObject;
        newImage.transform.parent = contentTransform.GetChild(0);
        newImage.transform.SetAsLastSibling();
        newImage.GetComponent<RectTransform>().localPosition = new Vector3(-rightSpawnThreshold + thresholdLength + (thresholdLength / 2), -5, 0);
        newImage.GetComponent<RectTransform>().localScale = Vector3.one;
        newImage.GetComponent<RectTransform>().localRotation = Quaternion.identity;

        if (newImage.transform.parent.childCount > 2)
        {
            Destroy(newImage.transform.parent.GetChild(0).gameObject);
        }
    }

    void SpawnNewImageLeft()
    {
        numberLeft++;
        numberRight--;

        GameObject newImage = Instantiate(timelineImagePrefab) as GameObject;
        newImage.transform.parent = contentTransform.GetChild(0);
        newImage.transform.SetAsFirstSibling();
        newImage.GetComponent<RectTransform>().localPosition = new Vector3(-leftSpawnThreshold - (thresholdLength / 2), -5, 0);
        newImage.GetComponent<RectTransform>().localScale = Vector3.one;
        newImage.GetComponent<RectTransform>().localRotation = Quaternion.identity;

        if (newImage.transform.parent.childCount > 2)
        {
            Destroy(newImage.transform.parent.GetChild(newImage.transform.parent.childCount - 1).gameObject);
        }
    }

    void SetTimecode ()
    {
        // Translate x position into seconds using second scale above
        float unitsPerSecond = thresholdLength / secondScale;
        float scaler = 1 / unitsPerSecond;
        float currentSeconds = -(contentTransform.localPosition.x) * scaler;

        // parse seconds into minutes and seconds and milliseconds
        float minutes = Mathf.Floor(currentSeconds / 60);
        float seconds =  Mathf.Floor((currentSeconds % 60));
        float milliseconds =  Mathf.Floor(((currentSeconds * 1000) % 1000));

        // print time to the display
        timecodeText.text = minutes.ToString("00") + ":" + seconds.ToString("00") + "." + milliseconds.ToString("0000");
    }

    public void MoveTimeline (float time)
    {
        contentTransform.localPosition = new Vector3(TimeToPos(time), 0, 0);
    }

    public float TimeToPos (float time)
    {
        float unitsPerSecond = thresholdLength / secondScale;
        float scaler = 1 / unitsPerSecond;
        float posX = -(time/scaler);
        return posX;
    }

    public float PosToTime(float posX)
    {
        float unitsPerSecond = thresholdLength / secondScale;
        float scaler = 1 / unitsPerSecond;
        float currentSeconds = (posX * scaler) - (secondScale/2);
        return currentSeconds;
    }

    public void PlayTimeline ()
    {
        MoveTimeline(0f);
        timelinePlaying = true;
        scrollRect.horizontal = false;
        if (isTimelineMoving)
        {
            StartCoroutine(StopThenPlay());
        }
        else
        {
            StartCoroutine(PlayTimelineCoroutine());
        }
    }

    public void StopTimeline ()
    {
        timelinePlaying = false;
        MoveTimeline(0f);
        scrollRect.horizontal = true;
    }

    IEnumerator StopThenPlay()
    {
        StopTimeline();
        yield return null;
        PlayTimeline();
        yield return null;
    }

    IEnumerator PlayTimelineCoroutine ()
    {
        float unitsPerSecond = (thresholdLength / secondScale);
        float secondsPerUnit = (secondScale / thresholdLength);
        while (timelinePlaying)
        {
            if (!isTimelineMoving)
            {
                StartCoroutine(Translation(contentTransform, contentTransform.localPosition, new Vector3(contentTransform.localPosition.x - unitsPerSecond, 0, 0), 1));
            }
            yield return null;
        }
        yield return null;
    }

    IEnumerator Translation(Transform thisTransform, Vector3 startPos, Vector3 endPos, float timeToMove)
    {
        float rate = 1.0f / timeToMove;
        float t = 0.0f;
        isTimelineMoving = true;
        while (t < 1.0 && timelinePlaying)
        {
            t += Time.deltaTime / timeToMove;
            thisTransform.localPosition = Vector3.Lerp(startPos, endPos, t);
            yield return null;
        }
        isTimelineMoving = false;
    }

    public void SetupNodesOnTimeline ()
    {
        DeleteTimelineNodes();
        if (AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.childCount > 0)
        {
            for (int i = 0; i < AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.childCount; i++)
            {
                float nodeTimestamp = AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i).GetComponent<AutomationNodeInfo>().timestamp;

                GameObject timelineNode = Instantiate(automationNodePrefab) as GameObject;
                timelineNode.transform.SetParent(contentTransform.GetChild(1));
                timelineNode.transform.localPosition = new Vector3(-1 * TimeToPos(nodeTimestamp) + (thresholdLength / 2), -20, 0);
                timelineNode.transform.localScale = new Vector3(15, 15, 15);
                timelineNode.transform.localRotation = Quaternion.identity;
                AutomationNodeTimelineLink timelineLink = timelineNode.GetComponent<AutomationNodeTimelineLink>();
                timelineLink.LinkNodes(timelineNode, AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i).gameObject);
            }

            for (int i = 0; i < contentTransform.GetChild(1).childCount; i++)
            {
                timelineAutomationNodes.Add(contentTransform.GetChild(1).GetChild(i).gameObject);
            }

            for (int i = 0; i < contentTransform.GetChild(1).childCount; i++)
            {
                contentTransform.GetChild(1).GetChild(i).GetComponent<AutomationNodeTimelineLink>().FindNodes();
            }
        }
    }

    public void CallAllNodesToFind ()
    {
        for (int i = 0; i < contentTransform.GetChild(1).childCount; i++)
        {
            contentTransform.GetChild(1).GetChild(i).GetComponent<AutomationNodeTimelineLink>().FindNodes();
        }
    }

    public void DeleteTimelineNodes()
    {
        timelineAutomationNodes.Clear();
        List<GameObject> tmp = new List<GameObject>();

        for (int i = 0; i < contentTransform.GetChild(1).childCount; i++)
        {
            tmp.Add(contentTransform.GetChild(1).GetChild(i).gameObject);
        }

        for (int i = 0; i < tmp.Count; i++)
        {
            GameObject tmpObj = tmp[i];
            tmp[i] = null;
            DestroyImmediate(tmpObj);
        }
    }
}
