﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomationNodeTimelineLink : MonoBehaviour {

    GameObject timelineNode;
    GameObject worldNode;

    public GameObject thisNode;
    public GameObject previousNode;
    public GameObject nextNode;

    private bool settingUpNodes = false;
    private bool isSyncing = false;
    private bool canSync = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (canSync && !isSyncing)
        {
            //StartCoroutine(SyncNodes());
        }
	}
    
    void LateUpdate ()
    {
        Vector3 pos = new Vector3(Mathf.Clamp(transform.localPosition.x, LoopScroll.Instance.thresholdLength/2, float.MaxValue), -20, 0);
        transform.localPosition = pos;

        if (LoopScroll.Instance.contentTransform.localPosition.x + transform.localPosition.x < LoopScroll.Instance.maskLeftEdge ||
            LoopScroll.Instance.contentTransform.localPosition.x + transform.localPosition.x > LoopScroll.Instance.maskRightEdge)
        {
            //disable rendering
            GetComponent<Renderer>().enabled = false;
        }
        else
        {
            //enable rendering
            GetComponent<Renderer>().enabled = true;
        }

    }

    void OnEnable ()
    {
        if (canSync && !isSyncing)
        {
            LinkNodes(timelineNode, worldNode);
        }
    }

    void OnDisable ()
    {
        isSyncing = false;
    }

    public void LinkNodes (GameObject tNode, GameObject wNode)
    {
        timelineNode = tNode;
        worldNode = wNode;
        AutomationNodeInfo timelineNodeInfo = tNode.GetComponent<AutomationNodeInfo>();
        AutomationNodeInfo worldNodeInfo = wNode.GetComponent<AutomationNodeInfo>();

        timelineNodeInfo.timestamp = worldNodeInfo.timestamp;
        timelineNodeInfo.nodePosition = worldNodeInfo.nodePosition;
        gameObject.name = GetComponent<AutomationNodeInfo>().nodePosition.ToString();

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(SyncNodes());
        }
        canSync = true;
        isSyncing = true;
    }

    IEnumerator SyncNodes()
    {
        while (true)
        {
            if (worldNode != null)
            {
                timelineNode.GetComponent<MeshRenderer>().material.color = worldNode.GetComponent<MeshRenderer>().material.color;
                timelineNode.GetComponent<AutomationNodeInfo>().timestamp = Mathf.Clamp(LoopScroll.Instance.PosToTime(transform.localPosition.x), 0, float.MaxValue);
                worldNode.GetComponent<AutomationNodeInfo>().timestamp = timelineNode.GetComponent<AutomationNodeInfo>().timestamp;

                if (GetComponent<SetTouched>().objectGrabbed)
                {
                    AutomationManager.Instance.SetCurrentTimecode(timelineNode.GetComponent<AutomationNodeInfo>().timestamp);
                }

                if (GetComponent<SetTouched>().objectGrabbed && !settingUpNodes)
                {
                    // Check to see if we have physically been moved before the previous node, or after the next node
                    if (previousNode!= null)
                    {
                        if (timelineNode.transform.localPosition.x < previousNode.transform.localPosition.x)
                        {
                            // We have moved before
                            worldNode.GetComponent<AutomationNodeInfo>().myInstrumnent.GetComponent<AutomationData>().SwapNodes(thisNode.GetComponent<AutomationNodeInfo>().nodePosition, previousNode.GetComponent<AutomationNodeInfo>().nodePosition);
                            SwapNodes(thisNode, previousNode);
                        }
                    }

                    if (nextNode != null)
                    {
                        if (timelineNode.transform.localPosition.x > nextNode.transform.localPosition.x)
                        {
                            // We have moved after
                            worldNode.GetComponent<AutomationNodeInfo>().myInstrumnent.GetComponent<AutomationData>().SwapNodes(thisNode.GetComponent<AutomationNodeInfo>().nodePosition, nextNode.GetComponent<AutomationNodeInfo>().nodePosition);
                            SwapNodes(thisNode, nextNode);
                        }
                    }
                }
            }
            yield return null;
        }
    }

    public void FindNodes()
    {
        settingUpNodes = true;
        Transform automationNodes = LoopScroll.Instance.contentTransform.GetChild(1);
        thisNode = gameObject;
        previousNode = null;
        nextNode = null;

        if (GetComponent<AutomationNodeInfo>().nodePosition > 0)
        {
            previousNode = automationNodes.GetChild(GetComponent<AutomationNodeInfo>().nodePosition - 1).gameObject;
        }
        else
        {
            previousNode = null;
        }

        if (automationNodes.childCount > 1 && GetComponent<AutomationNodeInfo>().nodePosition < automationNodes.childCount - 1)
        {
            nextNode = automationNodes.GetChild(GetComponent<AutomationNodeInfo>().nodePosition + 1).gameObject;
        }
        else
        {
            nextNode = null;
        }

        settingUpNodes = false;
    }

    void SwapNodes(GameObject objA, GameObject objB)
    {
        settingUpNodes = true;
        int tmpInt = objA.GetComponent<AutomationNodeInfo>().nodePosition;
        objA.transform.SetSiblingIndex (objB.GetComponent<AutomationNodeInfo>().nodePosition);
        objB.transform.SetSiblingIndex(tmpInt);

        objA.GetComponent<AutomationNodeInfo>().nodePosition = objA.transform.GetSiblingIndex();
        objB.GetComponent<AutomationNodeInfo>().nodePosition = objB.transform.GetSiblingIndex();
        LoopScroll.Instance.CallAllNodesToFind();
    }
}
