﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeManager : MonoBehaviour {

	public static VolumeManager Instance;
	public int numberSoloInstruments;
	public int numberMutedInstruments;
    public bool dopplerOn;

	private bool muteAllToggle = false;
	private bool unSoloToggle = false;

	public Image soloImage;
    public Sprite soloLitSprite;
    public Sprite soloUnlitSprite;
	private bool hasSoloSwitched = false;

	public Image muteBackground;
    public Sprite muteLitSprite;
    public Sprite muteUnlitSprite;
	private bool hasMuteSwitched = false;

	public GameObject groupComponentPrefab;
	public GameObject fabricObjectPrefab;
    public GameObject fabricObject2DPrefab;
	public GameObject groupObject;

    public Transform leftController;
    public Transform rightController;
    public Transform leftTip;
    public Transform rightTip;

    public float dopplerLevel;


	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		} 
		else 
		{
			Destroy(this);
		}

		groupObject = Instantiate(groupComponentPrefab) as GameObject;
	}

	void Start ()
	{
		ReregisterGroupComponent();
        StartCoroutine(SyncFlash());
	}

	public void ReregisterGroupComponent()
	{
		groupObject.GetComponent<Fabric.GroupComponent>().UnregisterWithMainHierarchy();
		groupObject.GetComponent<Fabric.GroupComponent>().RegisterWithMainHierarchy();
	}

	void Update ()
	{
		if (numberSoloInstruments > 0)
		{
			if (!hasSoloSwitched && soloImage != null)
			{
				soloImage.sprite = soloLitSprite;

				hasSoloSwitched = !hasSoloSwitched;
                StartCoroutine("FlashSolo");
			}
		}
		else 
		{
			if (hasSoloSwitched && soloImage != null)
			{
                StopCoroutine("FlashSolo");
				soloImage.sprite = soloUnlitSprite;

				hasSoloSwitched = !hasSoloSwitched;
			}
		}

		if (numberMutedInstruments > 0)
		{
			if (!hasMuteSwitched && muteBackground != null)
			{
				muteBackground.sprite = muteLitSprite;

				hasMuteSwitched = !hasMuteSwitched;
                StartCoroutine("FlashMute");
			}
		}
		else 
		{
			if (hasMuteSwitched && muteBackground != null)
			{
                StopCoroutine("FlashMute");
				muteBackground.sprite = muteUnlitSprite;

				hasMuteSwitched = !hasMuteSwitched;
			}
		}
	}

    IEnumerator FlashSolo ()
    {
        while (true)
        {
            if (flashBool)
            {
                soloImage.sprite = soloUnlitSprite;
            }
            else
            {
                soloImage.sprite = soloLitSprite;
            }
            yield return null;
        }
    }

    IEnumerator FlashMute()
    {
        while (true)
        {
            if (flashBool)
            {
                muteBackground.sprite = muteUnlitSprite;
            }
            else
            {
                muteBackground.sprite = muteLitSprite;
            }
            yield return null;
        }
    }

    private bool flashBool = false;

    IEnumerator SyncFlash (float flashTime = 0.5f)
    {
        float startTime = Time.time;
        float endtime = startTime + flashTime;
        while (true)
        {
            if (Time.time > endtime)
            {
                flashBool = !flashBool;
                startTime = Time.time;
                endtime = Time.time + flashTime;
            }
            yield return null;
        }

        //yield return done;
    }

    // TODO - refactor so this takes a transform as an argument, so that we don't need the helper functions below it.

	public void CheckAudible ()
	{
		for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
		{
			VolumeControls volumeControls = PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<VolumeControls>();

			volumeControls.audible = !volumeControls.mute;

			// IF AND ONLY IF some instruments are soloed, then we override the effect of mute
			if (numberSoloInstruments > 0)
			{
				volumeControls.audible = volumeControls.solo;
			}

			// Now we know whether the instrument is audible or not, we change the color
			if (volumeControls.audible)
			{
				//ConfigurationManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,1f);
				ColorManager.Instance.FadeColor(PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SpriteRenderer>(), 1f, 0.2f);

				// Turn on particle system
				PresetManager.Instance.instrumentsContainer.GetChild(i).transform.GetChild(0).gameObject.SetActive(true);

                // Set the actual audio to audible
                PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().SetAudioVolume(1f);
			}
			else
			{
				//ConfigurationManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,.5f);
				ColorManager.Instance.FadeColor(PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SpriteRenderer>(), 0.1f, 0.2f);

				// Turn off particle system
				PresetManager.Instance.instrumentsContainer.GetChild(i).transform.GetChild(0).gameObject.SetActive(false);

                // Set the actual audio to not audible.
                PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().SetAudioVolume(0f);
            }
		}

        GetAttachTransforms();
        CheckAudibleOnAttachPoint(leftTip);
        CheckAudibleOnAttachPoint(rightTip);
	}

    void CheckAudibleOnAttachPoint (Transform attachPoint)
    {
        if (attachPoint != null && attachPoint.childCount > 0)
        {
            for (int i = 0; i < attachPoint.childCount; i++)
            {
                VolumeControls volumeControls = attachPoint.GetChild(i).GetComponent<VolumeControls>();

                volumeControls.audible = !volumeControls.mute;

                // IF AND ONLY IF some instruments are soloed, then we override the effect of mute
                if (numberSoloInstruments > 0)
                {
                    volumeControls.audible = volumeControls.solo;
                }

                // Now we know whether the instrument is audible or not, we change the color
                if (volumeControls.audible)
                {
                    //ConfigurationManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,1f);
                    ColorManager.Instance.FadeColor(attachPoint.GetChild(i).GetComponent<SpriteRenderer>(), 1f, 0.2f);

                    // Turn on particle system
                    attachPoint.GetChild(i).transform.GetChild(0).gameObject.SetActive(true);

                    // Set the actual audio to audible
                    attachPoint.GetChild(i).GetComponent<InstrumentSetup>().SetAudioVolume(1f);
                }
                else
                {
                    //ConfigurationManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,.5f);
                    ColorManager.Instance.FadeColor(attachPoint.GetChild(i).GetComponent<SpriteRenderer>(), 0.1f, 0.2f);

                    // Turn off particle system
                    attachPoint.GetChild(i).transform.GetChild(0).gameObject.SetActive(false);

                    // Set the actual audio to not audible.
                    attachPoint.GetChild(i).GetComponent<InstrumentSetup>().SetAudioVolume(0f);
                }
            }
        }
    }

    void GetAttachTransforms ()
    {
        string targetObjectString = "tip";

        for (int i = 0; i < leftController.childCount; i++)
        {
            if (leftController.GetChild(i).name == targetObjectString)
            {
                leftTip = leftController.GetChild(i).GetChild(0);
            }
        }

        for (int k = 0; k < rightController.childCount; k++)
        {
            if (rightController.GetChild(k).name == targetObjectString)
            {
                rightTip = rightController.GetChild(k).GetChild(0);
            }
        }
    }

	public void UnMuteTempButton ()
	{
		    if (!muteAllToggle)
		    {
			    for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
			    {
				    VolumeControls volumeControls = PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<VolumeControls>();

				    if (volumeControls.mute)
				    {
					    volumeControls.tempUnMute = true;
					    volumeControls.mute = false;
					    numberMutedInstruments = 0;
				    }
				    CheckAudible();
			    }
		    }
		    else
		    {
			    for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
			    {
				    VolumeControls volumeControls = PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<VolumeControls>();

				    if (volumeControls.tempUnMute)
				    {
					    volumeControls.tempUnMute = false;
					    volumeControls.mute = true;
					    numberMutedInstruments ++;
				    }
				    CheckAudible();
			    }
		    }

		    muteAllToggle = !muteAllToggle;
	}


	public void UnSoloTempButton ()
	{
		if (!unSoloToggle)
		{
			for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
			{
				VolumeControls volumeControls = PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<VolumeControls>();

				if (volumeControls.solo)
				{
					volumeControls.tempUnSolo = true;
					volumeControls.solo = false;
					numberSoloInstruments = 0;
				}
				CheckAudible();
			}
		}
		else
		{
			for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
			{
				VolumeControls volumeControls = PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<VolumeControls>();

				if (volumeControls.tempUnSolo)
				{
					volumeControls.tempUnSolo = false;
					volumeControls.solo = true;
					numberSoloInstruments ++;
				}
			}
			CheckAudible();
		}

		unSoloToggle = !unSoloToggle;
	}

	public void ResetAllButton ()
	{
		for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
		{
			VolumeControls volumeControls = PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<VolumeControls>();
			volumeControls.mute = false;

			if (volumeControls.solo)
			{
				volumeControls.solo = false;
				numberSoloInstruments = 0;
			}
			CheckAudible();
		}
	}

    public void SetDoppler (Toggle toggle)
    {
        dopplerOn = toggle.isOn;

        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().SetDoppler(toggle.isOn);
        }
    }

    public void SetDopplerAmount(Slider slider)
    {
        dopplerLevel = slider.value;
        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().SetDoppler(dopplerLevel);
        }
    }

    public void PlayButton()
    {
        StartCoroutine(PlayButtonCoroutine());
    }

    IEnumerator PlayButtonCoroutine ()
    {
        if (groupObject.GetComponent<Fabric.GroupComponent>().IsComponentActive())
        {
            for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
            {
                PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().StopAudio();
            }

            yield return new WaitForSeconds(0.25f);
        }

        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().SetAudio();
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().PlayAudio();
        }

        yield return null;
    }

    public void StopButton ()
    {
        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().StopAudio();
        }
    }
}
