﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PresetManager : MonoBehaviour {

    public bool deletePresetsOnStart = false;
	public GameObject cameraObject;
	public GameObject instrumentPrefab;
	public Transform instrumentsContainer;
    public int numInstruments;

	public Text selectedPresetText;
	public Text currentPresetText;
	public Text inputSaveName;
	public Text confirmationText;

	private int selectedPresetInt = 1;
	private int currentPresetInt = 1;
	private string currentPresetName = "Orchestra_Traditional";
	public string savePresetName = "Default";
	private string[] presetArray;

    public string currentAudioFolder;
    public Slider spreadSlider;

	public static PresetManager Instance;

	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		} 
		else 
		{
			Destroy(this);
		}
	}

	void Start ()
	{
        if (deletePresetsOnStart)
        {
            PlayerPrefs.DeleteAll();
        }

        cameraObject = ControllersTracker.Instance.cameraRig.gameObject;

		if (!PlayerPrefs.HasKey("PlayedBefore"))
		{
            ES2.Save(new List<string>(), "Preset_List");
			SetupFactoryPresets();
            ES2.Save(new List<string>(), "Song_List");
            SongManager.Instance.SetupFactorySongs();
		}
	}
		
	void SetupFactoryPresets ()
	{
        SetupFactoryHelper(Resources.Load("InstrumentSets/Empty") as PresetData);
        SetupFactoryHelper(Resources.Load("InstrumentSets/8-Bit Frantic Battle") as PresetData);
        SetupFactoryHelper(Resources.Load("InstrumentSets/Bohemian Rhapsody Surround") as PresetData);
        SetupFactoryHelper(Resources.Load("InstrumentSets/Orchestra_Traditional") as PresetData);
        SetupFactoryHelper(Resources.Load("InstrumentSets/Wow_Eagle of Dreanor") as PresetData);
        SetupFactoryHelper(Resources.Load("InstrumentSets/Hide The Sunlight") as PresetData );
        SetupFactoryHelper(Resources.Load("InstrumentSets/New Moon") as PresetData );
        SetupFactoryHelper(Resources.Load("InstrumentSets/Hold On") as PresetData );
        SetupFactoryHelper(Resources.Load("InstrumentSets/Blue Agave") as PresetData );
        SetupFactoryHelper(Resources.Load("InstrumentSets/16-Bit Difficult Battle") as PresetData );
        
		PlayerPrefs.SetInt("PlayedBefore", 1);
	}

	void SetupFactoryHelper (PresetData preset)
	{
		int count = 0;
		foreach (Vector3 v in preset.position)
		{
			// Save the instrument's position into the preset
			string positionKey = preset.instrument[count].ToString() + count.ToString() + "_Preset_" + preset.name + "_";
            ES2.Save(v, positionKey + "Position");

			// Save the instrument into the preset
            ES2.Save(preset.instrument[count].ToString(), "Preset_" + preset.name + "_Instrument_" + count.ToString());

            // Save the color into the instrument preset
            ES2.Save(preset.instrumentColor[count], positionKey + "Color");

            // Save the automation from this preset into an ES2 file
            AutomationPresetData automation = AutomationManager.Instance.GetAutomationAssetName(preset.name, preset.instrument[count].ToString() + count.ToString());
            if (automation != null)
            {
                ES2.Save(automation.timestamps, positionKey + " Automation_Timestamps");
                ES2.Save(automation.nodeOrder, positionKey + " Automation_NodeOrder");
                ES2.Save(automation.position, positionKey + " Automation_Positions");
                ES2.Save(automation.loopType, positionKey + " Automation_LoopType");
            }

			count ++;
		}

		// Save how many instruments are in our preset
        ES2.Save(count, "Preset_" + preset.name + "NumInstruments");

		// Add the preset to our list of presets
        List<string> presetListTmp = ES2.LoadList<string>("Preset_List");
        presetListTmp.Add(preset.name);
        ES2.Save(presetListTmp, "Preset_List");
	}

    public void SetCurrentAudioFolder (Dropdown dropdown)
    {
        currentAudioFolder = dropdown.options[dropdown.value].text;
    }

	public void SetCurrentPreset(int presetInt)
	{
        selectedPresetInt = presetInt;
	}

    public int GetCurrentPresetInt ()
    {
        return currentPresetInt;
    }

    public string[] GetPresetArray ()
    {
        return presetArray;
    }

	void RefreshPresetList()
	{
        presetArray = ES2.LoadList<string>("Preset_List").ToArray();
	}


    // TODO - these two functions create and out of range exception when you delete all player prefs and try to cycle through presets. Fix this
	public void NextPreset ()
	{
		RefreshPresetList();
		selectedPresetInt = (int)Mathf.Repeat(selectedPresetInt + 1, presetArray.Length);
        selectedPresetText.text = presetArray[selectedPresetInt];

	}

	public void PreviousPreset ()
	{
		RefreshPresetList();
		selectedPresetInt = (int)Mathf.Repeat(selectedPresetInt - 1, presetArray.Length);
        selectedPresetText.text = presetArray[selectedPresetInt];
	}

	public void SetPresetName (Text chosenName)
	{
		savePresetName = chosenName.text;
	}

	//
	// Saving Stuff
	//

	public void SavePreset ()
	{
		RefreshPresetList();

		if (currentPresetName != "" || currentPresetName != null)
		{
			currentPresetInt = presetArray.Length;
            selectedPresetInt = presetArray.Length;
			savePresetName = inputSaveName.text;
			currentPresetName = savePresetName;
			selectedPresetText.text = currentPresetName;
			currentPresetText.text = currentPresetName;
            ES2.Save(instrumentsContainer.childCount, "Preset_" + currentPresetName + "NumInstruments");
            List<string> presetListTmp = ES2.LoadList<string>("Preset_List");
            presetListTmp.Add(currentPresetName);
            ES2.Save(presetListTmp, "Preset_List");
            RefreshPresetList();
			SaveInstrumentData();
			confirmationText.text = "Saved new preset: \n" + currentPresetName;
		}
		else
		{
            //TODO - this isn't working for some reason
			confirmationText.text = "Could not save instrument set \"\". Please enter a name when saving...";
		}
	}

	public void SaveOverPreset ()
	{
        if (selectedPresetText.text != "" && selectedPresetText.text != "Empty")
        {
            RefreshPresetList();
            ES2.Save(instrumentsContainer.childCount, "Preset_" + currentPresetName + "NumInstruments");
            SaveInstrumentData();
            confirmationText.text = "Saved existing preset: \n" + currentPresetName;
        }
        else
        {
            confirmationText.text = "Could not save over Empty instrument set. Please choose another...";
        }
	}

	void SaveInstrumentData ()
	{
		for (int i = 0; i < instrumentsContainer.childCount; i++)
		{
			Transform instrumentObject = instrumentsContainer.GetChild(i);
			string positionKey = instrumentObject.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Preset_" + currentPresetName + "_";
            ES2.Save(instrumentObject.position, positionKey + "Position");
            ES2.Save(instrumentObject.GetComponent<InstrumentInfo>().color, positionKey + "Color");
            ES2.Save(instrumentsContainer.GetChild(i).GetComponent<InstrumentInfo>().instrument.ToString(), "Preset_" + currentPresetName + "_Instrument_" + i.ToString());

            // Save the automation
            foreach (Transform nodeContainer in AutomationManager.Instance.AutomationContainer)
            {
                if (nodeContainer.name.Contains("AutomationNodes") && nodeContainer.childCount > 0)
                {
                    ES2.Save(nodeContainer.GetChild(0).GetComponent<AutomationNodeInfo>().myInstrumnent.GetComponent<AutomationData>().automationMovementType, positionKey + " Automation_LoopType");
                    List<float> timestamps = new List<float>();
                    List<int> nodeOrder = new List<int>();
                    List<Vector3> position = new List<Vector3>();
                    foreach (Transform node in nodeContainer)
                    {
                        timestamps.Add(node.GetComponent<AutomationNodeInfo>().timestamp);
                        nodeOrder.Add(node.GetComponent<AutomationNodeInfo>().nodePosition);
                        position.Add(node.transform.position);
                    }

                    ES2.Save(timestamps, positionKey + " Automation_Timestamps");
                    ES2.Save(nodeOrder, positionKey + " Automation_NodeOrder");
                    ES2.Save(position, positionKey + " Automation_Positions");
                }
            }
		}
	}

	//
	// Loading Stuff
	//

	public void LoadPreset ()
	{
        if (ToggleUI.Instance.instrumentSetSelector.activeInHierarchy || ToggleUI.Instance.songSetSelector.activeInHierarchy)
        {
            ToggleUI.Instance.Toggle(true);
        }

		RefreshPresetList();
		selectedPresetText.text = presetArray[currentPresetInt];
		currentPresetInt = selectedPresetInt;
		currentPresetName = presetArray[currentPresetInt];
		selectedPresetText.text = currentPresetName;
		currentPresetText.text = currentPresetName;
		confirmationText.text = "Loaded preset: \n" + currentPresetName;
        numInstruments = ES2.Load<int>("Preset_" + currentPresetName + "NumInstruments");
        spreadSlider.value = 0;
		StartCoroutine(LoadInstrumentData());
        SongManager.Instance.LoadSong();
        AutomationManager.Instance.StopAutomation();
        LoopScroll.Instance.DeleteTimelineNodes();

	}

	IEnumerator LoadInstrumentData ()
	{
		for (int x = 0; x < instrumentsContainer.childCount; x++)
		{
            instrumentsContainer.GetChild(x).GetComponent<InstrumentSetup>().DeleteInstrument();
		}

        yield return null;

        for (int i = 0; i < ES2.Load<int>("Preset_" + currentPresetName + "NumInstruments"); i++)
		{
			GameObject instrument = Instantiate(instrumentPrefab) as GameObject;

            instrument.GetComponent<InstrumentInfo>().instrument = (Instrument)System.Enum.Parse(typeof(Instrument), ES2.Load<string>("Preset_" + currentPresetName + "_Instrument_" + i.ToString()));
            string positionKey = instrument.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Preset_" + currentPresetName + "_";
            instrument.GetComponent<InstrumentInfo>().color = ES2.Load<Color>(positionKey + "Color");
			instrument.GetComponent<InstrumentSetup>().SetVisuals();
			instrument.transform.parent = instrumentsContainer;
			instrument.name = instrument.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString();
            instrument.transform.position = ES2.Load<Vector3>(positionKey + "Position");
			instrument.SetActive(true);
		}

        yield return null;

        // Load the automation
        for (int i = 0; i < ES2.Load<int>("Preset_" + currentPresetName + "NumInstruments"); i++)
        {
            GameObject instrument = instrumentsContainer.GetChild(i).gameObject;
            string positionKey = instrument.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Preset_" + currentPresetName + "_";
            AutomationManager.Instance.SetSelectedInstrument(instrument);
            
            List<float> timestamps = ES2.LoadList<float>(positionKey + " Automation_Timestamps");
            List<int> nodeOrder = ES2.LoadList<int>(positionKey + " Automation_NodeOrder");
            List<Vector3> position = ES2.LoadList<Vector3>(positionKey + " Automation_Positions");
            AutomationData.automationMovementTypeEnum loopType = ES2.Load<AutomationData.automationMovementTypeEnum>(positionKey + " Automation_LoopType");

            int count = 0;
            AutomationNodeInfo aNode = null;
            foreach (Vector3 v in position)
            {
                aNode = instrument.GetComponent<AutomationData>().CreateAndReturnAutomationNode(position[count], timestamps[count], nodeOrder[count]);
                count++;
            }
            aNode.myInstrumnent.GetComponent<AutomationData>().automationMovementType = loopType;
        }
        AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().HideAutomation();

        if (!AutomationManager.Instance.canRecord)
        {
            AutomationManager.Instance.GetSelectedInstrument().transform.GetChild(3).gameObject.SetActive(false);
        }
        yield return null;
	}

    private bool CanGrab(VRTK.VRTK_InteractGrab grabbingController)
    {
        return (grabbingController && grabbingController.GetGrabbedObject() == null && grabbingController.IsGrabButtonPressed());
    }

	//
	// Deleting Stuff
	//

	// Choose carfully the preset we are deleting. Do we want to delete the currently loaded preset, or the currently selected one?

	public void DeletePreset ()
	{
		if (selectedPresetText.text != "" && selectedPresetText.text != "Empty")
		{
            currentPresetInt = 0;
            selectedPresetInt = 0;
			confirmationText.text = "Deleted preset: \n" + selectedPresetText.text;
			DeleteInstrumentData();
            List<string> presetListTmp = ES2.LoadList<string>("Preset_List");
            presetListTmp.Remove(selectedPresetText.text);
            ES2.Save(presetListTmp, "Preset_List");

			if (selectedPresetText.text == currentPresetName)
			{
				currentPresetName = "";
				currentPresetText.text = currentPresetName;
			}
			
			selectedPresetText.text = currentPresetName;
		}
		else
		{
			confirmationText.text = "Could not delete preset. Please choose another...";
		}
	}

	void DeleteInstrumentData ()
	{
		for (int i = 0; i < instrumentsContainer.childCount; i++)
		{
			Transform instrumentObject = instrumentsContainer.GetChild(i);
			string positionKey = instrumentObject.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Preset_" + currentPresetName + "_";

            ES2.Delete(positionKey + "Position");
            ES2.Delete(positionKey + "Color");
            ES2.Delete("Preset_" + currentPresetName + "_Instrument_" + i.ToString());

            // Delete the automation
            ES2.Delete(positionKey + " Automation_Timestamps");
            ES2.Delete(positionKey + " Automation_NodeOrder");
            ES2.Delete(positionKey + " Automation_Positions");
            ES2.Delete(positionKey + " Automation_LoopType");
		}
	}
}
