﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorManager : MonoBehaviour {

	public static ColorManager Instance;

	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		} 
		else 
		{
			Destroy(this);
		}
	}

	public float instrumentAnimationSpeed = 0.15f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void FadeColor (SpriteRenderer spriteRenderer, float aValue, float aTime)
	{
        if (spriteRenderer != null)
        {
            StartCoroutine(FadeColorCoroutine(spriteRenderer, aValue, aTime));
        }
	}

	public void FadeColor (Image image, float aValue, float aTime)
	{
		StartCoroutine(FadeColorCoroutine(image, aValue, aTime));
	}

	IEnumerator FadeColorCoroutine (SpriteRenderer spriteRenderer, float aValue, float aTime)
	{
		float alpha = spriteRenderer.color.a;
        if (alpha != aValue)
		{
			for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
			{
                if (spriteRenderer != null)
                {
                    Color newColor = new Color(spriteRenderer.gameObject.GetComponent<InstrumentInfo>().color.r, spriteRenderer.gameObject.GetComponent<InstrumentInfo>().color.g, spriteRenderer.gameObject.GetComponent<InstrumentInfo>().color.b, Mathf.Lerp(alpha, aValue, t));
                    spriteRenderer.color = newColor;
                }
				yield return null;
			}

            if (spriteRenderer != null)
            {
                Color finalColor = new Color(spriteRenderer.gameObject.GetComponent<InstrumentInfo>().color.r, spriteRenderer.gameObject.GetComponent<InstrumentInfo>().color.g, spriteRenderer.gameObject.GetComponent<InstrumentInfo>().color.b, aValue);
                spriteRenderer.color = finalColor;
            }
			yield return null;
		}
	}

	IEnumerator FadeColorCoroutine (Image image, float aValue, float aTime)
	{
		float alpha = image.color.a;
		if (alpha != aValue)
		{
			for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
			{
                Color newColor = new Color(image.gameObject.GetComponent<InstrumentInfo>().color.r, image.gameObject.GetComponent<InstrumentInfo>().color.g, image.gameObject.GetComponent<InstrumentInfo>().color.b, Mathf.Lerp(alpha, aValue, t));
				image.color = newColor;
				yield return null;
			}

            Color finalColor = new Color(image.gameObject.GetComponent<InstrumentInfo>().color.r, image.gameObject.GetComponent<InstrumentInfo>().color.g, image.gameObject.GetComponent<InstrumentInfo>().color.b, aValue);
			image.color = finalColor;
			yield return null;
		}
	}
}
