﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstrumentInfo : MonoBehaviour {

	public Instrument instrument;
    public string audioEventName;
	public string audioFileName;
    public float audioDelay;
    public int is3D = 1;
    public Color color = Color.white;
    public int sortOrder;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
