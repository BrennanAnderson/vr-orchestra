﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasResize : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ResizeCanvas (float amount)
    {
        GetComponent<RectTransform>().sizeDelta = new Vector2(220, amount);
        GetComponent<BoxCollider>().size = new Vector2(220, amount);
        transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(220, amount);
    }
}
