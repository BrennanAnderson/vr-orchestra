﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventVolumeMeter : MonoBehaviour {

    private float amountOfTimeActive = 0f;
    private float timeActivated = -5f;
    private float rmsVisualScaler = 20f;

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (GetComponent<InstrumentSetup>().fabricObject != null)
        {
            transform.GetChild(4).localScale = new Vector3(GetComponent<InstrumentSetup>().fabricObject.GetComponent<Fabric.VolumeMeter>().volumeMeterState.mRMS *rmsVisualScaler, transform.GetChild(4).localScale.y, transform.GetChild(4).localScale.z);
            if (GetComponent<InstrumentSetup>().fabricObject.GetComponent<Fabric.VolumeMeter>().volumeMeterState.mRMS > 0f)
            {
                if (!transform.GetChild(0).gameObject.activeInHierarchy)
                {
                    //transform.GetChild(0).gameObject.SetActive(true);
                    timeActivated = Time.time;
                }
            }
            else
            {
                if (transform.GetChild(0).gameObject.activeInHierarchy)
                {
                    if (GetAmountOfTimeActive() > 1f)
                    {
                        //transform.GetChild(0).gameObject.SetActive(false);
                        amountOfTimeActive = 0f;
                    }
                }
            }
        }

        
	}

    float GetAmountOfTimeActive ()
    {
        amountOfTimeActive = Time.time - timeActivated;
        return amountOfTimeActive;
    }
}
