﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Bootup : MonoBehaviour {

	public Image img;
	public Canvas can;


	// Use this for initialization
	void Start () 
	{
		//ColorManager.Instance.FadeColor(img, 1f, 3f);
		StartCoroutine(MoveLogo());
		StartCoroutine(WaitThenLoadScene());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator MoveLogo ()
	{
		while (true)
		{
			Vector3 rect = can.transform.position;
			rect -= new Vector3(0f,0f,.05f);
			can.transform.position = rect;

//			Vector3 rect = img.transform.localScale;
//			rect += new Vector3(.001f,.001f,0);
//			img.rectTransform.localScale = rect;

			yield return null;
		}
	}

	IEnumerator WaitThenLoadScene ()
	{
		yield return new WaitForSeconds(3.5f);
		LoadScene();
	}

	void LoadScene ()
	{
		ColorManager.Instance.FadeColor(img, 0f, 1f);
		if (Application.platform == RuntimePlatform.Android)
		{
			if (PlayerPrefs.HasKey("NumPresets"))
			{
				ScreenFader.Instance.EndScene("MainMenu");
			}
			else
			{
				ScreenFader.Instance.EndScene("Tutorial");
			}
		}
		else
		{
			//ScreenFader.Instance.EndScene("Tutorial");
			//ScreenFader.Instance.EndScene("NotAndroid");
			ScreenFader.Instance.EndScene("MainMenu");
		}
	}
}
