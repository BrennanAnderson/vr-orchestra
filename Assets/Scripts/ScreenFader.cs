﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScreenFader : MonoBehaviour
{
	public static ScreenFader Instance;
	public Image FadeImg;
	public float fadeSpeed = 1.5f;
	public bool sceneStarting = true;


	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		} 
		else 
		{
			Destroy(this);
		}

		FadeImg.rectTransform.localScale = new Vector2(Screen.width, Screen.height);
		sceneStarting = true;
	}

	void Update()
	{
		// If the scene is starting...
		if (sceneStarting)
			// ... call the StartScene function.
			StartScene();
	}


	void FadeToClear()
	{
		// Lerp the colour of the image between itself and transparent.
		FadeImg.color = Color.Lerp(FadeImg.color, Color.clear, fadeSpeed * Time.fixedDeltaTime);
	}


	void FadeToBlack()
	{
		// Lerp the colour of the image between itself and black.
		FadeImg.color = Color.Lerp(FadeImg.color, Color.black, fadeSpeed * Time.fixedDeltaTime);
	}


	void StartScene()
	{
		// Fade the texture to clear.
		FadeToClear();

		// If the texture is almost clear...
		if (FadeImg.color.a <= 0.01f)
		{
			// ... set the colour to clear and disable the RawImage.
			FadeImg.color = Color.clear;
			FadeImg.enabled = false;

			// The scene is no longer starting.
			sceneStarting = false;
		}
	}


	public IEnumerator EndSceneRoutine(string SceneName)
	{
		// Make sure the RawImage is enabled.
		FadeImg.enabled = true;
		do
		{
			// Start fading towards black.
			FadeToBlack();

			// If the screen is almost black...
			if (FadeImg.color.a >= 0.95f)
			{
				// ... reload the level
                VRTK.VRTK_SDKManager.instance.UnloadSDKSetup();
				SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
                sceneStarting = true;
				yield break;
			}
			else
			{
				yield return null;
			}
		} while (true);
	}

	public void EndScene(string SceneName)
	{
		sceneStarting = false;
		StartCoroutine("EndSceneRoutine", SceneName);
	}
}   