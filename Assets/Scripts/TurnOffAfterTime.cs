﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffAfterTime : MonoBehaviour {

	public float timeToWait = 1f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable ()
	{
		StartCoroutine(TurnMeOff());
	}

	IEnumerator TurnMeOff ()
	{
		yield return new WaitForSeconds(timeToWait);
		gameObject.SetActive(false);
	}
}
