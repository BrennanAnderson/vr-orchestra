﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;


public class InstrumentSetup : MonoBehaviour {

    private float myAudioTime;
	private Animator animator;
    public GameObject fabricObject;
    public AudioSource fabricObjectAudioSource;
	public RuntimeAnimatorController[] controllers;
	public Text instrumentName;
    public float volumeSliderLevel = 1f;
    public float volumeLevel = 1f;

	private FileInfo[] info;
	private DirectoryInfo dir;
    private DirectoryInfo[] directories;
	private bool audioFileExists = false;
    private List<string> audioFileList;

	void Awake ()
	{
		animator = GetComponent<Animator>();
        audioFileList = new List<string>();
        Refresh();
	}
    void Update ()
    {

    }

    void Refresh ()
    {
        audioFileList.Clear();

        dir = new DirectoryInfo(Application.dataPath + "/StreamingAssets");
        directories = dir.GetDirectories();
        info = dir.GetFiles();
        for (int i = 0; i < info.Length; i++)
        {
            audioFileList.Add(info[i].Name);
        }

        for (int i = 0; i < directories.Length; i++)
        {
            info = directories[i].GetFiles();
            for (int k = 0; k < info.Length; k++)
            {
                audioFileList.Add(directories[i].Name + "/" + info[k].Name);
            }
        }
    }

	public void SetVisuals ()
	{
		for (int i = 0; i < controllers.Length; i++)
		{
			string replace = "_Anim_0";
			string name = controllers[i].name.Replace(replace, "");

			if (name == GetComponent<InstrumentInfo>().instrument.ToString())
			{
				animator.runtimeAnimatorController = controllers[i];
			}
		}

        if (GetComponent<InstrumentInfo>().audioFileName.Length > 0)
        {
            instrumentName.text = GetComponent<InstrumentInfo>().audioFileName.Substring(0, GetComponent<InstrumentInfo>().audioFileName.Length - 4);

            for (int i = 0; i < directories.Length; i++)
            {
                if (instrumentName.text.Contains(directories[i].Name))
                {
                    instrumentName.text = GetComponent<InstrumentInfo>().audioFileName.Substring(0, GetComponent<InstrumentInfo>().audioFileName.Length - 4).Replace(directories[i].Name + "/", "");
                }
            }
        }
        else
        {
            instrumentName.text = GetComponent<InstrumentInfo>().instrument.ToString();
        }

        GetComponent<SpriteRenderer>().color = GetComponent<InstrumentInfo>().color;

        int numInstruments = PresetManager.Instance.instrumentsContainer.childCount;
        GetComponent<InstrumentInfo>().sortOrder = numInstruments;
	}

	// TODO - There should be a sync (OR PLAY BUTTON) that plays them all at the same time since you can add instruments after the start. 
    // Also Have a stop button
    // Only play audio automatically when a user changes the preset or spawns a new instrument

	public void SetAudio ()
	{
        for (int i = 0; i < audioFileList.Count; i++)
		{
            if (audioFileList[i] == GetComponent<InstrumentInfo>().audioFileName)
			{
				audioFileExists = true;
			}
		}

		if (audioFileExists)
		{
	        if (!Fabric.EventManager.Instance._eventList.Contains(GetComponent<InstrumentInfo>().audioEventName))
	        { 
			    Fabric.EventManager.Instance._eventList.Add(GetComponent<InstrumentInfo>().audioEventName);
	        }

	        bool objectAlreadyExists = false;

	        for (int i = 0; i < VolumeManager.Instance.groupObject.transform.childCount; i++)
	        {
	            if (VolumeManager.Instance.groupObject.transform.GetChild(i).name == GetComponent<InstrumentInfo>().audioEventName)
	            {
	                objectAlreadyExists = true;
	            }
	        }

	        if (!objectAlreadyExists)
	        {
                if (GetComponent<InstrumentInfo>().is3D == 1)
                {
                    fabricObject = Instantiate(VolumeManager.Instance.fabricObjectPrefab) as GameObject;
                }
                else
                {
                    fabricObject = Instantiate(VolumeManager.Instance.fabricObject2DPrefab) as GameObject;
                }
	        }

			fabricObject.transform.SetParent(VolumeManager.Instance.groupObject.transform);
			fabricObject.name = GetComponent<InstrumentInfo>().audioEventName;
			fabricObject.GetComponent<Fabric.EventListener>()._eventName = GetComponent<InstrumentInfo>().audioEventName;

            fabricObject.GetComponent<Fabric.WwwAudioComponent>().AudioClipReference = GetComponent<InstrumentInfo>().audioFileName;
            /*if (PresetManager.Instance.currentAudioFolder == "StreamingAssets")
            {
                fabricObject.GetComponent<Fabric.WwwAudioComponent>().AudioClipReference = GetComponent<InstrumentInfo>().audioFileName;
            }
            else
            {
                fabricObject.GetComponent<Fabric.WwwAudioComponent>().AudioClipReference = PresetManager.Instance.currentAudioFolder + "/" + GetComponent<InstrumentInfo>().audioFileName;
            }*/

            // Use this next line to loop the automation
	        //fabricObject.GetComponent<Fabric.WwwAudioComponent>().Loop = true;
            fabricObject.GetComponent<Fabric.WwwAudioComponent>().Delay = GetComponent<InstrumentInfo>().audioDelay;
            fabricObject.GetComponent<Fabric.WwwAudioComponent>().DopplerLevel = VolumeManager.Instance.dopplerLevel;
            fabricObject.GetComponent<Fabric.WwwAudioComponent>().SetVolume(volumeSliderLevel * volumeLevel);
            //Fabric.EventManager.Instance.PostEvent(GetComponent<InstrumentInfo>().audioEventName, Fabric.EventAction.SetVolume, myVolume, gameObject);

            if (GetComponent<InstrumentInfo>().audioFileName.Length > 0)
            {
                instrumentName.text = GetComponent<InstrumentInfo>().audioFileName.Substring(0, GetComponent<InstrumentInfo>().audioFileName.Length - 4);

                for (int i = 0; i < directories.Length; i++)
                {
                    if (instrumentName.text.Contains(directories[i].Name))
                    {
                        instrumentName.text = GetComponent<InstrumentInfo>().audioFileName.Substring(0, GetComponent<InstrumentInfo>().audioFileName.Length - 4).Replace(directories[i].Name + "/", "");
                    }
                }

                transform.GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                instrumentName.text = GetComponent<InstrumentInfo>().instrument.ToString();
            }

	        VolumeManager.Instance.ReregisterGroupComponent();
		}
		else
		{
			GetComponent<VolumeControls>().MuteInstrument();
			transform.GetChild(1).gameObject.SetActive(true);
		}
	}

    public void SetAudioVolume (float volume)
    {
        Fabric.EventManager.Instance.PostEvent(GetComponent<InstrumentInfo>().audioEventName, Fabric.EventAction.SetVolume, volume * volumeSliderLevel, gameObject);
        volumeLevel = volume;
    }

    public void SetAudioVolumeWithSlider(Slider slider)
    {
        Fabric.EventManager.Instance.PostEvent(GetComponent<InstrumentInfo>().audioEventName, Fabric.EventAction.SetVolume, slider.value * volumeLevel, gameObject);
        volumeSliderLevel = slider.value;
    }

    public void StopAudio ()
    {
        
        //myAudioTime = fabricObject.GetComponent<Fabric.WwwAudioComponent>().AudioSource.time;
        Fabric.EventManager.Instance.PostEvent(GetComponent<InstrumentInfo>().audioEventName, Fabric.EventAction.StopSound, gameObject);

        if (fabricObject != null)
        {
            Destroy(fabricObject);
        }

        transform.GetChild(0).gameObject.SetActive(false);
    }

    public void PlayAudio ()
    {
        StartCoroutine(PlayAudioCoroutine());
    }

    IEnumerator PlayAudioCoroutine ()
    {
        if (GetComponent<InstrumentInfo>().audioEventName != "")
        {
            Fabric.EventManager.Instance.PostEvent(GetComponent<InstrumentInfo>().audioEventName, gameObject);
            Fabric.EventManager.Instance.PostEvent(GetComponent<InstrumentInfo>().audioEventName, gameObject);

            // Note - this is a weird, fucked up magic number. Fabric seems to have a bug where the events have to be spaced out in order to work
            // Check audible sets the volume of the Fabric event. If this bug did not exist, I would not be using a coroutine
            yield return new WaitForSeconds(0.1f);
            //fabricObject.GetComponent<Fabric.WwwAudioComponent>().AudioSource.time = myAudioTime + 0.2f + Time.deltaTime;
        }
        VolumeManager.Instance.CheckAudible();
        yield return null;
    }

    public void DeleteInstrument ()
    {
        StopAudio();
        Destroy(fabricObject, 1f);
        Fabric.EventManager.Instance._eventList.Remove(GetComponent<InstrumentInfo>().audioEventName);

        if (GetComponent<VolumeControls>().mute)
        {
            GetComponent<VolumeControls>().MuteInstrument();
        }

        if (GetComponent<VolumeControls>().solo)
        {
            GetComponent<VolumeControls>().SoloInstrument();
        }

        InstrumentIconSorter.Instance.RemoveIconFromPanel(GetComponent<InstrumentInfo>().instrument.ToString());

        Destroy(GetComponent<AutomationData>().AutomationNodesTransform.gameObject);
        Destroy(GetComponent<AutomationData>().AutomationLinesTransform.gameObject);
        Destroy(gameObject);
    }

    public void SetDoppler (bool dopplerTrue)
    {
        if (dopplerTrue)
        {
            fabricObject.GetComponent<Fabric.WwwAudioComponent>().DopplerLevel = 1f;
        }
        else
        {
            fabricObject.GetComponent<Fabric.WwwAudioComponent>().DopplerLevel = 0f;
        }
    }

    public void SetDoppler (float value)
    {
        fabricObject.GetComponent<Fabric.WwwAudioComponent>().DopplerLevel = value;
    }

    public void UnloadAudio ()
    {
        StopAudio();
        Destroy(fabricObject, 1f);
        Fabric.EventManager.Instance._eventList.Remove(GetComponent<InstrumentInfo>().audioEventName);

        if (GetComponent<VolumeControls>().audible)
        {
            GetComponent<VolumeControls>().MuteInstrument();
        }

        GetComponent<InstrumentInfo>().audioEventName = "";
        GetComponent<InstrumentInfo>().audioFileName = "";

        transform.GetChild(1).gameObject.SetActive(true);
    }
}
