﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PositionInfo : MonoBehaviour {

    private Transform listenerObject;
    [SerializeField]
    private float azimuth;
    [SerializeField]
    private float elevation;
    [SerializeField]
    private float distance;

    private Text azimuthText;
    private Text elevationText;
    private Text distanceText;
    private GameObject details;

	// Use this for initialization
	void Start () 
    {
        listenerObject = ControllersTracker.Instance.cameraRig.parent.transform;
        details = transform.GetChild(2).GetChild(1).gameObject;
        azimuthText = transform.GetChild(2).GetChild(1).GetChild(0).GetChild(1).GetComponent<Text>();
        elevationText = transform.GetChild(2).GetChild(1).GetChild(1).GetChild(1).GetComponent<Text>();
        distanceText = transform.GetChild(2).GetChild(1).GetChild(2).GetChild(1).GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        SetAzimuth();
        SetElevation();
        SetDistance();
	}

    // 0 degrtees is directly in front of you 
    // 90 degrees is hard right
    // -90 degrees is hard left
    // 180 degrees is directly behind you
    void SetAzimuth ()
    {
        azimuth = Mathf.Atan2(transform.position.x - listenerObject.position.x, transform.position.z - listenerObject.position.z) * Mathf.Rad2Deg;
        azimuthText.text = azimuth.ToString();
    }

    // 0 degrees is directly in front
    // 90 degrees is directly above
    // -90 degrees is directly below
    void SetElevation()
    {
        elevation = Mathf.Atan2(transform.position.y - listenerObject.position.y, transform.position.z - listenerObject.position.z) * Mathf.Rad2Deg;
        elevationText.text = elevation.ToString();
    }
    
    // Distance is in meters
    void SetDistance()
    {
        distance = (transform.position - listenerObject.position).magnitude;
        distanceText.text = distance.ToString();
    }

    public float GetAzimuth ()
    {
        return azimuth;
    }

    public float GetElevation()
    {
        return elevation;
    }

    public float GetDistance()
    {
        return distance;
    }

    public void ToggleDetails (Toggle toggle)
    {
        details.SetActive(toggle.isOn);
    }
}
