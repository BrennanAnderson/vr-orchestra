﻿

namespace VRTK.Examples
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class ActivateOnHighlight : VRTK_InteractableObject
    {

        public GameObject[] objectsToActivate;
        public GameObject radialMenu;
        public float delayTime = 0.1f;

        private bool canBeActivated;
        private bool hasBeenActivated = false;


        public override void StartUsing(GameObject usingObject)
        {
            base.StartUsing(usingObject);

            //for (int i = 0; i < objectsToActivate.Length; i++)
            //{
           //     objectsToActivate[i].SetActive(true);
           // }
        }

        public override void StopUsing(GameObject usingObject)
        {
            base.StopUsing(usingObject);

            //for (int i = 0; i < objectsToActivate.Length; i++)
            //{
            //    objectsToActivate[i].SetActive(false);
            //}
        }

        protected void Start()
        {
        }

        // TODO - lerp the scale  when enabling and disabling

        void OnEnable ()
        {
            canBeActivated = true;
            StartCoroutine("WaitThenActivate");
        }

        void OnDisable ()
        {
            canBeActivated = false;
            StopCoroutine("WaitThenActivate");

            for (int i = 0; i < objectsToActivate.Length; i++)
            {
                objectsToActivate[i].SetActive(false);
            }

            radialMenu.SetActive(true);
        }

        IEnumerator WaitThenActivate ()
        {
            yield return new WaitForSeconds(delayTime);

            for (int i = 0; i < objectsToActivate.Length; i++)
            {
                objectsToActivate[i].SetActive(true);
            }

            radialMenu.SetActive(false);

        }

        protected override void Update()
        {
           
            
        }
    }
}
