﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class PopulateAudioFolderList : MonoBehaviour
{

    private List<string> audioFolderList;
    private DirectoryInfo[] info;
    private DirectoryInfo dir;

    // Use this for initialization
    void Awake()
    {
        dir = new DirectoryInfo(Application.dataPath + "/StreamingAssets");
        audioFolderList = new List<string>();
        Refresh();
        GetComponent<Dropdown>().AddOptions(audioFolderList);
    }

    void Refresh()
    {
        audioFolderList.Clear();

        info = dir.GetDirectories();
        for (int i = 0; i < info.Length; i++)
        {
            audioFolderList.Add(info[i].Name);
            //if (info[i].Name.EndsWith(".ogg") || info[i].Name.EndsWith(".wav") || info[i].Name.EndsWith(".mp3"))
            //{
            //    audioFolderList.Add(info[i].Name);
            //}
        }

        audioFolderList.Insert(0, "StreamingAssets");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
