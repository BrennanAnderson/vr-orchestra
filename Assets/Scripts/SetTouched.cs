﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTouched : MonoBehaviour {

    public bool objectTouched = false;
    public bool objectGrabbed = false;
    public GameObject touchingObject;

    // Have to initialize this as greater than 1, or we will get bugs where things can be deleted when they should not be
    private float timeSinceLastGrabbed = 2f;

    private GameObject secondaryTouchingObject;

	// Use this for initialization
	void Start () 
    {
        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectTouched += DetermineTouched;
        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectUntouched += DetermineUnTouched;

        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectGrabbed += DetermineGrabbed;
        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectUngrabbed += DetermineUnGrabbed;
	}

    void OnEnable ()
    {
        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectTouched += DetermineTouched;
        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectUntouched += DetermineUnTouched;

        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectGrabbed += DetermineGrabbed;
        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectUngrabbed += DetermineUnGrabbed;
    }

    void OnDisable ()
    {
        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectTouched -= DetermineTouched;
        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectUntouched -= DetermineUnTouched;

        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectGrabbed += DetermineGrabbed;
        GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectUngrabbed += DetermineUnGrabbed;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void DetermineTouched(object sender, VRTK.InteractableObjectEventArgs e)
    {
        if (objectGrabbed == false)
        {
            objectTouched = true;
            secondaryTouchingObject = touchingObject;
            touchingObject = e.interactingObject;
        }
    }

    void DetermineUnTouched(object sender, VRTK.InteractableObjectEventArgs e)
    {
        if (objectGrabbed == false)
        {
            if (e.interactingObject == touchingObject)
            {
                touchingObject = secondaryTouchingObject;
                secondaryTouchingObject = null;
            }
            else
            {
                secondaryTouchingObject = null;
            }

            if (touchingObject == null && secondaryTouchingObject == null)
            {
                objectTouched = false;
            }
        }
    }

    void DetermineGrabbed(object sender, VRTK.InteractableObjectEventArgs e)
    {
        objectGrabbed = true;
        if (touchingObject != null)
        {
            touchingObject.GetComponent<VRTK.VRTK_UIPointer>().enabled = false;
        }
    }

    void DetermineUnGrabbed(object sender, VRTK.InteractableObjectEventArgs e)
    {
        objectGrabbed = false;
        if (touchingObject != null)
        {
            touchingObject.GetComponent<VRTK.VRTK_UIPointer>().enabled = true;
        }
        timeSinceLastGrabbed = Time.time;
    }

    public float GetTimeSinceLastGrabbed ()
    {
        timeSinceLastGrabbed = Time.time - timeSinceLastGrabbed;
        return timeSinceLastGrabbed;
    }
}
