﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestFabric : MonoBehaviour {

    public SpawnInstrument spawn;


    // Instructions for Taz - The problem occurs most apparently when spawning instruments. Pressing 1 will spawn an instrument
    // and sync all of the other instruments, meaning that all of the instruments will be stopped, then played from the beginning.
    // Internally this is deleting fabric components after they are stopped and creating them again to play them. This isn't strictly necesarry (I think)
    // but just how it is currently implemented.

    // If you press 2, the instruments will not sync. The audio components will not be touched and will keep playing while 
    // a new audio component is created and started from the beginning. Whenever a new audio component is created in the external group component, 
    // I have to unregister, then register that group component into the main hierarchy. I imagine this is where the problems are coming from.

    // You can now either press 1 again to get the bug, or you must restart the editor and press 3. The game will spawn an unsynced instrument,
    // then after 5 seconds spawn a synced instrument. It is when spawning a synced instrument after an unsynced instrument that the bug occurs.
    // The StopSound event action is ignored by the components, and when the fabric components are destroyed from the the external group component,
    // the audio sources are not disabled. The bug seems to be that the event action is ignored.

    // You can hear that because the events are then created again, the audio is doubled up, and there is no way to stop the audio sources unless I
    // manually find the audio pool gameobject, loop through the gameobjects, and disable them. I really do not want to have to do that.

	void Update () 
    {
        // This will spawn an instrument and sync all audio together. Fabric will behave as expected
		if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            spawn.syncAudioOnSpawn = true;
            spawn.Spawn();
        }

        // This will spawn an instrument and NOT all audio together. Fabric will behave as expected
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            spawn.syncAudioOnSpawn = false;
            spawn.Spawn();
        }

        // This will spawn an instrument and NOT sync all audio together, then spawn another instrument and try to sync all audio together. Fabric not stop the audio as expected
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            StartCoroutine(SpwanTwice());
        }
	}

    IEnumerator SpwanTwice()
    {
        spawn.syncAudioOnSpawn = false;
        spawn.Spawn();
        yield return new WaitForSeconds(5f);
        spawn.syncAudioOnSpawn = true;
        spawn.Spawn();
    }
}
