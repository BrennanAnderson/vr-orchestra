﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleUI : MonoBehaviour {

	public GameObject creativeUI;
    public GameObject instrumentMenu;
    public GameObject[] mainMenu;
    public GameObject[] setInactive;
    public GameObject instrumentSetSelector;
    public GameObject songSetSelector;

    public static bool isMenuOpen = false;

    Transform rightController;
    Transform leftController;
    VRTK.VRTK_ControllerEvents rightControllerEvents;
    VRTK.VRTK_ControllerEvents leftControllerEvents;

    private float startTime = 0f;
    private float timeElapsed = 0f;
    public float holdTimeThreshold = 0.5f;
    private bool buttonIsPressed;
    private List<GameObject> objectsToReactivate;

    public GameObject modeSelector;

    public static ToggleUI Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        objectsToReactivate = new List<GameObject>();
    }

	// Use this for initialization
	void Start () 
    {
        leftController = ControllersTracker.Instance.leftController;
        rightController = ControllersTracker.Instance.rightController;
        rightControllerEvents = rightController.GetComponent<VRTK.VRTK_ControllerEvents>();
        leftControllerEvents = leftController.GetComponent<VRTK.VRTK_ControllerEvents>();

        rightControllerEvents.ButtonTwoPressed += new VRTK.ControllerInteractionEventHandler(DoButtonTwoClicked);
        leftControllerEvents.ButtonTwoPressed += new VRTK.ControllerInteractionEventHandler(DoButtonTwoClicked);

        rightControllerEvents.ButtonTwoReleased += new VRTK.ControllerInteractionEventHandler(DoButtonTwoReleased);
        leftControllerEvents.ButtonTwoReleased += new VRTK.ControllerInteractionEventHandler(DoButtonTwoReleased);

        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void DoButtonTwoClicked (object sender, VRTK.ControllerInteractionEventArgs e)
    {
        buttonIsPressed = true;
        startTime = Time.time;
        timeElapsed = 0f;
        StartCoroutine (HoldingButton(sender, e));  
    }

    bool instrumentsShowing = true;
    private void DoButtonTwoReleased (object sender, VRTK.ControllerInteractionEventArgs e)
    {
        buttonIsPressed = false;
        timeElapsed = Time.time - startTime;

        if (timeElapsed < holdTimeThreshold)
        {
            // How long we held the button is shorter than the hold time threshold
            //Debug.Log("Button was clicked");
            Toggle(true);
        }
        else
        {
            // We held the button, so it was not a click. Now we must close the menus that the held button opens, not the regular menu
            // as well as set the mode that we selected
            //modeSelector.GetComponent<ModeSelector>().selectedModeGameObject.GetComponent<SetMode>().SetControllerMode();
            //modeSelector.SetActive(false);
            if (instrumentsShowing)
            {
                DeactivateInstruments();
                instrumentsShowing = false;
            }
            else
            {
                ActivateInstruments();
                instrumentsShowing = true;
            }
        }
    }

    // This activates the floating mode selector. (It needs work. Doesn't work 100% of the time)
    IEnumerator HoldingButton(object sender, VRTK.ControllerInteractionEventArgs e)
    {
        while (buttonIsPressed)
        {
            timeElapsed = Time.time - startTime;
            if (timeElapsed > holdTimeThreshold)
            {
                // We know that we have held the button long enough
                if (!modeSelector.activeInHierarchy)
                {
                    //modeSelector.SetActive(true);
                    //modeSelector.transform.position = e.controllerReference.actual.transform.position;
                    //modeSelector.transform.rotation = new Quaternion(0, e.controllerReference.actual.transform.rotation.y, 0, e.controllerReference.actual.transform.rotation.w);
                }
            }
            yield return null;
        }
        yield return null;
    }

	public void Toggle (bool isMainMenu)
	{
        if (isMainMenu)
        {
            if (creativeUI.activeInHierarchy)
            {
                DeactivateMenu(isMainMenu);
            }
            else
            {
                ActivateMenu(isMainMenu);
            }
        }
        /*else
        {
            if (instrumentMenu.activeInHierarchy)
            {
                DeactivateMenu(isMainMenu);
            }
            else
            {
                ActivateMenu(isMainMenu);
            }
        }*/
	}

    public void ActivateMenu (bool isMainMenu)
    {
        isMenuOpen = true;

        if (isMainMenu)
        {
            creativeUI.SetActive(true);
			//StartCoroutine("ScaleUpCoroutine");
        }
        else
        {
            creativeUI.SetActive(false);
			//StartCoroutine("ScaleDownCoroutine");
        }

        objectsToReactivate.Clear();

        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<Renderer>().enabled = false;
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<BoxCollider>().enabled = false;

            for (int x = 0; x < PresetManager.Instance.instrumentsContainer.GetChild(i).childCount; x++)
            {
                if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).GetComponent<Renderer>() != null)
                {
                    PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).GetComponent<Renderer>().enabled = false;
                }

                bool isGameObjectActive;
                isGameObjectActive = PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).gameObject.activeInHierarchy;

                if (isGameObjectActive)
                {
                    //PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).GetChild(k).gameObject.SetActive(false);
                    objectsToReactivate.Add(PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).gameObject);
                }

                foreach (GameObject g in objectsToReactivate)
                {
                    g.SetActive(false);
                }
            }
        }
    }

    public void DeactivateMenu(bool isMainMenu)
    {
        isMenuOpen = false;

        if (isMainMenu)
        {
            creativeUI.SetActive(false);
			//StartCoroutine("ScaleDownCoroutine");
        }
        else
        {
            //instrumentMenu.SetActive(false);
        }

        for (int i = 0; i < setInactive.Length; i++)
        {
            setInactive[i].SetActive(false);
        }

        for (int i = 0; i < mainMenu.Length; i++)
        {
            mainMenu[i].SetActive(true);
        }

        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<Renderer>().enabled = true;
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<BoxCollider>().enabled = true;

            for (int x = 0; x < PresetManager.Instance.instrumentsContainer.GetChild(i).childCount; x++)
            {
                if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).GetComponent<Renderer>() != null)
                {
                    PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).GetComponent<Renderer>().enabled = true;
                }

                foreach (GameObject g in objectsToReactivate)
                {
                    g.SetActive(true);
                }
            }

            objectsToReactivate.Clear();
        }
    }

	IEnumerator ScaleUpCoroutine ()
	{
		creativeUI.SetActive(true);
		StopCoroutine("ScaleDownpCoroutine");

		creativeUI.transform.parent.localScale = Vector3.zero;
		float scaleUpTime = 0.5f;
		float targetTime = Time.time + scaleUpTime;
		float t = 0f;
		while (Time.time < targetTime)
		{
			t += Time.deltaTime/scaleUpTime;
			creativeUI.transform.parent.localScale = Vector3.Lerp(creativeUI.transform.parent.localScale, new Vector3(0.5f, 0.5f, 0.5f), t);
			yield return null;
		}
		yield return null;
	}

	IEnumerator ScaleDownCoroutine ()
	{
		StopCoroutine("ScaleUpCoroutine");

		float scaleUpTime = 0.5f;
		float targetTime = Time.time + scaleUpTime;
		float t = 0f;
		while (Time.time < targetTime)
		{
			t += Time.deltaTime/scaleUpTime;
			creativeUI.transform.parent.localScale = Vector3.Lerp(creativeUI.transform.parent.localScale, Vector3.zero, t);
			yield return null;
		}
		creativeUI.SetActive(false);
		yield return null;
	}

    void ActivateInstruments ()
    {
        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<Renderer>().enabled = true;
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<BoxCollider>().enabled = true;

            for (int x = 0; x < PresetManager.Instance.instrumentsContainer.GetChild(i).childCount; x++)
            {
                if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).GetComponent<Renderer>() != null)
                {
                    PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).GetComponent<Renderer>().enabled = true;
                }

                foreach (GameObject g in objectsToReactivate)
                {
                    g.SetActive(true);
                }
            }

            objectsToReactivate.Clear();
        }
    }

    void DeactivateInstruments ()
    {
        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<Renderer>().enabled = false;
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<BoxCollider>().enabled = false;

            for (int x = 0; x < PresetManager.Instance.instrumentsContainer.GetChild(i).childCount; x++)
            {
                if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).GetComponent<Renderer>() != null)
                {
                    PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).GetComponent<Renderer>().enabled = false;
                }

                bool isGameObjectActive;
                isGameObjectActive = PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).gameObject.activeInHierarchy;

                if (isGameObjectActive)
                {
                    //PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).GetChild(k).gameObject.SetActive(false);
                    objectsToReactivate.Add(PresetManager.Instance.instrumentsContainer.GetChild(i).GetChild(x).gameObject);
                }

                foreach (GameObject g in objectsToReactivate)
                {
                    g.SetActive(false);
                }
            }
        }
    }
}
