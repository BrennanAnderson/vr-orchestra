﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DepthSlider : MonoBehaviour {

    public float resolution = 1f;
    public float maxDistance = 100f;

    private float previousValue = 1f;
	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () 
    {

	}

    public void MoveDepth(Slider slider)
    {
        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            Vector3 direction = PresetManager.Instance.instrumentsContainer.GetChild(i).transform.position - Vector3.zero;
            Vector3 startPoint = PresetManager.Instance.instrumentsContainer.GetChild(i).transform.position;
            Vector3 endPoint = direction.normalized * maxDistance * -1f;
            Vector3 oneUnit = (Vector3.zero - endPoint) / maxDistance;
            //PresetManager.Instance.instrumentsContainer.GetChild(i).transform.position = Vector3.Lerp(Vector3.zero, endPoint, slider.value / 100);

            //TODO - Make this so that the lengths these move are magnified the farther away they get.
            //TODO - possible make each axis its own slider?

            if (slider.value > previousValue)
            {
                PresetManager.Instance.instrumentsContainer.GetChild(i).transform.position += (oneUnit * (slider.value - previousValue));
            }
            else
            {
                PresetManager.Instance.instrumentsContainer.GetChild(i).transform.position -= (oneUnit * (previousValue - slider.value));
            }

            //Debug.Log(direction.magnitude);
        }

        previousValue = slider.value;
    }
}
