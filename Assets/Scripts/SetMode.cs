﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMode : MonoBehaviour {

    public ModeManager.controllerModeEnum controllerMode = ModeManager.controllerModeEnum.grabMode;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetControllerMode ()
    {
        ModeManager.Instance.SetControllerMode(controllerMode);
    }

    public void SetModeRecordButton ()
    {
        if (ModeManager.Instance.GetControllerMode() == ModeManager.controllerModeEnum.automationMode)
        {
            ModeManager.Instance.SetControllerMode(ModeManager.controllerModeEnum.grabMode);
        }
        else if (ModeManager.Instance.GetControllerMode() == ModeManager.controllerModeEnum.grabMode && !AutomationManager.Instance.canRecord)
        {
            ModeManager.Instance.SetControllerMode(ModeManager.controllerModeEnum.automationMode);
        }
    }
}
