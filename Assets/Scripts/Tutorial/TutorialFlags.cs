﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialFlags : MonoBehaviour {

	private VRInput.SwipeDirection lastSwipe;

	public bool hasBeenPickedUp = false;
	public bool hasBeenDropped = false;

	public bool hasBeenMuted = false;
	public bool hasBeenUnMuted = false;

	public bool hasBeenSoloed = false;
	public bool hasBeenUnSoloed = false;

	public bool hasBeenPulled = false;
	public bool hasBeenPushed = false;

#if UNITY_ANDROID
	// Use this for initialization
	void Start () 
	{
		VRInput.Instance.OnUp += HandleOnUp;

		// This is called every frame, so we use it to update a variable rather than executing behaviors
		VRInput.Instance.OnSwipe += UpdateSwipeVariable;	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnDisable()
	{
		VRInput.Instance.OnUp -= HandleOnUp;
		VRInput.Instance.OnSwipe -= UpdateSwipeVariable;
	}

	private void UpdateSwipeVariable(VRInput.SwipeDirection swipeDirection)
	{
		lastSwipe = swipeDirection;
	}

	private void HandleOnUp ()
	{
		switch (lastSwipe)
		{
		case VRInput.SwipeDirection.NONE:
			InstrumentClicked();
			break;
		case VRInput.SwipeDirection.UP:
			InstrumentSoloed();
			break;
		case VRInput.SwipeDirection.DOWN:
			InstrumentMuted();
			break;
		case VRInput.SwipeDirection.LEFT:
			InstrumentPushed();
			break;
		case VRInput.SwipeDirection.RIGHT:
			InstrumentPulled();
			break;
		}
	}

	void InstrumentClicked ()
	{
		VRInteractiveItem currentInteractible = VREyeRaycaster.Instance.CurrentInteractible;

		if (currentInteractible != null && currentInteractible.gameObject == gameObject)
		{
			if (!hasBeenDropped)
			{
				if (hasBeenPickedUp)
				{
					hasBeenDropped = true;
					TutorialManager.Instance.InstrumentHasBeenPickedUpAndDropped();
				}
				else
				{
					hasBeenPickedUp = true;
				}
			}
		}
	}

	void InstrumentMuted()
	{
		VRInteractiveItem currentInteractible = VREyeRaycaster.Instance.CurrentInteractible;

		if (currentInteractible != null && currentInteractible.gameObject == gameObject)
		{
			if (hasBeenPickedUp && hasBeenDropped)
			{
				if (!hasBeenMuted)
				{
					hasBeenMuted = true;
					TutorialManager.Instance.InstrumentHasBeenMuted();
				}
				else
				{
					hasBeenUnMuted = true;
					TutorialManager.Instance.InstrumentHasBeenUnMuted();
				}
			}
		}

	}

	void InstrumentSoloed ()
	{
		VRInteractiveItem currentInteractible = VREyeRaycaster.Instance.CurrentInteractible;

		if (currentInteractible != null && currentInteractible.gameObject == gameObject)
		{
			if (hasBeenMuted && hasBeenUnMuted)
			{
				if (!hasBeenSoloed)
				{
					hasBeenSoloed = true;
					TutorialManager.Instance.InstrumentHasBeenSoloed();
				}
				else
				{
					hasBeenUnSoloed = true;
					TutorialManager.Instance.InstrumentHasBeenUnSoloed();
				}
			}
		}
	}

	void InstrumentPushed ()
	{
		VRInteractiveItem currentInteractible = VREyeRaycaster.Instance.CurrentInteractible;

		if (currentInteractible != null && currentInteractible.gameObject == gameObject)
		{
			if (hasBeenSoloed && hasBeenUnSoloed)
			{
				hasBeenPushed = true;
				TutorialManager.Instance.InstrumentHasBeenPushed();
			}
		}
	}

	void InstrumentPulled ()
	{
		VRInteractiveItem currentInteractible = VREyeRaycaster.Instance.CurrentInteractible;

		if (currentInteractible != null && currentInteractible.gameObject == gameObject)
		{
			if (hasBeenPushed)
			{
				hasBeenPulled = true;
				TutorialManager.Instance.InstrumentHasBeenPulled();
			}
		}
	}
#endif
}
