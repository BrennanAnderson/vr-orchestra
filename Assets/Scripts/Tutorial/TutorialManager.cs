﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour {

	public GameObject[] tutorialUI;

	public static TutorialManager Instance;
	private bool pickedUpAndDropped = false;
	private bool muted = false;
	private bool unMuted = false;
	private bool soloed = false;
	private bool unSoloed = false;
	private bool pulled = false;
	private bool pushed = false;

	public GameObject mainCamera;

	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		} 
		else 
		{
			Destroy(this);
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void InstrumentHasBeenPickedUpAndDropped ()
	{
		if (!pickedUpAndDropped)
		{
			StartCoroutine(WaitAndSetBool());
			pickedUpAndDropped = true;

			for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
			{
				PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<TutorialFlags>().hasBeenPickedUp = true;
				PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<TutorialFlags>().hasBeenDropped = true;
			}
		}
	}

	public void InstrumentHasBeenMuted ()
	{
		if (!muted)
		{
			StartCoroutine(WaitAndSetBool());
			muted = true;
		}
	}

	public void InstrumentHasBeenUnMuted ()
	{
		if (!unMuted)
		{
			StartCoroutine(WaitAndSetBool());
			unMuted = true;

			for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
			{
				PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<TutorialFlags>().hasBeenMuted = true;
				PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<TutorialFlags>().hasBeenUnMuted = true;
			}
		}
	}

	public void InstrumentHasBeenSoloed ()
	{
		if (!soloed)
		{
			StartCoroutine(WaitAndSetBool());
			soloed = true;
		}
	}

	public void InstrumentHasBeenUnSoloed ()
	{
		if (!unSoloed)
		{
			StartCoroutine(WaitAndSetBool());
			unSoloed = true;

			for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
			{
				PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<TutorialFlags>().hasBeenSoloed = true;
				PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<TutorialFlags>().hasBeenUnSoloed = true;
			}
		}
	}

	public void InstrumentHasBeenPulled ()
	{
		if (!pulled)
		{
			StartCoroutine(WaitAndSetBool(1f));
			pulled = true;
		}
	}

	public void InstrumentHasBeenPushed ()
	{
		if (!pushed)
		{
			StartCoroutine(WaitAndSetBool(1f));
			pushed = true;
		}
	}

	public void DisableInstruments ()
	{
		for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
		{
			ColorManager.Instance.FadeColor(PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SpriteRenderer>(), .5f, 0.2f);

			if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<BoxCollider>().enabled)
			{
				PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<BoxCollider>().enabled = false;
			}
		}
	}

	public void EnableInstruments ()
	{
		for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
		{

			//ColorManager.Instance.FadeColor(ConfigurationManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SpriteRenderer>(), .5f, 0f);

			if (!PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<BoxCollider>().enabled)
			{
				PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<BoxCollider>().enabled = true;
			}

			VolumeManager.Instance.CheckAudible();
		}
	}

	IEnumerator WaitAndSetBool (float delay = 0f)
	{
		yield return new WaitForSeconds(delay);
		yield return new WaitForSeconds(1f);
		GetComponent<Animator>().SetBool("canProgress", true);
	}
}
