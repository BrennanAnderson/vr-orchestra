﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class GetAudio : MonoBehaviour {

    private FileInfo[] info;
    private DirectoryInfo dir;

    public Text streamingAssetsPathText;

	// Use this for initialization
	void Start () 
    {
        dir = new DirectoryInfo(Application.dataPath + "/StreamingAssets");
        streamingAssetsPathText.text += dir.FullName;
        Refresh();

	}

    void Refresh()
    {
        info = dir.GetFiles("*.wav");
    }

    public void ShowExplorer()
    {
        string itemPath = dir.FullName;
        itemPath = itemPath.Replace(@"/", @"\");   // explorer doesn't like front slashes
        System.Diagnostics.Process.Start("explorer.exe", "/root," + itemPath);
    }
}
