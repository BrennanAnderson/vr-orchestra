﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnableInstrumentSpawner : MonoBehaviour {

    public SpawnInstrument spawnInstrument;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnEnable ()
    {
        StartCoroutine(WaitThenDo());
    }

    IEnumerator WaitThenDo ()
    {
        yield return new WaitForSeconds(0.1f);
        spawnInstrument.SetupInitDropdowns();
        yield return null;
    }
}
