﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadPreset : MonoBehaviour {

    private int defaultInt = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadNext ()
    {
        defaultInt = (int)Mathf.Repeat(defaultInt + 1, PresetManager.Instance.GetPresetArray().Length - 1);
        PresetManager.Instance.SetCurrentPreset(defaultInt);
        SongManager.Instance.SetCurrentSong(defaultInt);
        PresetManager.Instance.LoadPreset();
    }

    public void LoadPrevious ()
    {
        defaultInt = (int)Mathf.Repeat(defaultInt - 1, PresetManager.Instance.GetPresetArray().Length - 1);
        PresetManager.Instance.SetCurrentPreset(defaultInt);
        SongManager.Instance.SetCurrentSong(defaultInt);
        PresetManager.Instance.LoadPreset();
    }
}
