﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetInstrumentUIImage : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetVisuals(Dropdown instrumenDropdownSelection)
    {
        GetComponent<Image>().sprite = instrumenDropdownSelection.options[instrumenDropdownSelection.value].image;
    }
}
