﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.IO;

public class PlayWWWAudio : MonoBehaviour {

	public AudioSource audioSource;
	public bool clipMissing;

	private AudioClip audioClip;
	private bool loaded = false;

	void Awake ()
	{
		//StartCoroutine(LoadAudio());
	}

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			PlayAudio();
		}
	}

	IEnumerator LoadAudio ()
	{
		string audioFile = "file:///" + Application.dataPath + "/StreamingAssets/" + GetComponent<InstrumentInfo>().audioFileName;
		WWW www = new WWW(audioFile);
		yield return www;
		audioClip = www.GetAudioClip(true,true);

		if (audioClip.samples < 1)
		{
			// Audio file does not exist!
//			Debug.Log("Audio file does not exist!");
			clipMissing = true;
			yield return null;
		}
		else
		{
			audioSource.clip = audioClip;
			clipMissing = false;
			loaded = true;
			yield return null;
		}
	}

	IEnumerator WaitAndPlay()
	{
		while (!loaded)
		{
			yield return null;
		}
		PlayAudio();
		yield return null;
	}

	public void PlayAudio ()
	{
		if (loaded)
		{
			audioSource.Play();
//			Debug.Log("PLAY SUCCESFUL");
		}
		else
		{
			StartCoroutine(WaitAndPlay());
//			Debug.Log("Still loading. Will attempt again");
		}
	}

	public void StopAudio ()
	{
		audioSource.Stop();
	}

	public void SetAudioVolume (float volume)
	{
		audioSource.volume = volume;
	}
}
