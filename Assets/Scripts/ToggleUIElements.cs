﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleUIElements : MonoBehaviour {

    //public bool toggleRenderer = false;
    public List<GameObject> objectsToToggle;
    public List<GameObject> renderersToToggle;

    private float originalWidth;
    private float originalHeight;
    private Vector3 originalLocation;

	// Use this for initialization
	void Start () 
    {
        originalWidth = transform.parent.GetComponent<RectTransform>().sizeDelta.x;
        originalHeight = transform.parent.GetComponent<RectTransform>().sizeDelta.y;
        originalLocation = transform.parent.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ToggleStuff ()
    {
        //transform.parent.GetComponent<VRTK.VRTK_UICanvas>().enabled = GetComponent<Toggle>().isOn;
        
        foreach (GameObject g in objectsToToggle)
        {
            if (GetComponent<Toggle>() != null)
            {
                g.SetActive(GetComponent<Toggle>().isOn);
            }
            else
            {
                g.SetActive(false);
            }
        }

        foreach (GameObject r in renderersToToggle)
        {
            if (GetComponent<Toggle>() != null)
            {
                r.GetComponent<Canvas>().enabled = GetComponent<Toggle>().isOn;
                r.GetComponent<VRTK.VRTK_UICanvas>().enabled = GetComponent<Toggle>().isOn;
            }
            else
            {
                r.GetComponent<Canvas>().enabled = false;
                r.GetComponent<VRTK.VRTK_UICanvas>().enabled = false;
            }
        }
    }
}
