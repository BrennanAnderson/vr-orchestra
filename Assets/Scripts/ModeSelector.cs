﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeSelector : MonoBehaviour {

    public GameObject selectedModeGameObject;

	// Use this for initialization
	void Start () 
    {
        selectedModeGameObject = transform.GetChild(0).gameObject;	
	}
	
	// Update is called once per frame
	void Update () 
    {
		for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<SetTouched>().objectTouched)
            {
                selectedModeGameObject = transform.GetChild(i).gameObject;
            }
        }
	}
}
