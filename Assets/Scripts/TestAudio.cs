﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class TestAudio : MonoBehaviour {

	public List<string> audioFileList;
	private FileInfo[] info;
	private DirectoryInfo dir;

	// Use this for initialization
	void Start () 
	{
		dir = new DirectoryInfo(Application.dataPath + "/StreamingAssets");
		audioFileList = new List<string>();
		Refresh();
	}

	void Refresh()
	{
		audioFileList.Clear();

		info = dir.GetFiles();
		for (int i = 0; i < info.Length; i++)
		{
			if (info[i].Name.EndsWith(".ogg") || info[i].Name.EndsWith(".wav") || info[i].Name.EndsWith(".mp3"))
			{
				audioFileList.Add(info[i].Name);
			}
		}
	}

	// Update is called once per frame
	void Update () {

	}

}
