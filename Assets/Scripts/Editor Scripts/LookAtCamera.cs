﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour {

	public Transform cameraObject;

	// Use this for initialization
	void Start () 
	{
		cameraObject = ControllersTracker.Instance.cameraRig;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.LookAt(2 * transform.position - cameraObject.position);
	}
}
