﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrennanMouseLook : MonoBehaviour {

	public float speedH = 2.0f;
	public float speedV = 2.0f;

	private float yaw = 0.0f;
	private float pitch = 0.0f;

	void Update () 
	{
	#if UNITY_EDITOR
        if (UnityEngine.XR.XRSettings.loadedDeviceName == "OpenVR")
        {

        }
        else
        { 
		    yaw += speedH * Input.GetAxis("Horizontal");
		    pitch -= speedV * Input.GetAxis("Vertical");

		    transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
        }

	#endif

	#if UNITY_STANDALONE_OSX

		yaw += speedH * Input.GetAxis("Horizontal");
		pitch -= speedV * Input.GetAxis("Vertical");

		transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);

	#endif
	}




}
