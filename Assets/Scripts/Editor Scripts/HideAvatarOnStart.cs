﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideAvatarOnStart : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		transform.GetChild(0).gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
