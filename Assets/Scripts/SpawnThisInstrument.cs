﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnThisInstrument : MonoBehaviour {


	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Spawn()
    {
        if (transform.GetChild(0).GetComponent<Image>().material == InstrumentIconSorter.Instance.greyedOut)
        {
            SpawnInstrument.Instance.SetAudioFile("*Empty*");
            SpawnInstrument.Instance.SetInstrumentString(transform.GetChild(1).GetComponent<Text>().text);
            SpawnInstrument.Instance.Spawn();
            SongManager.Instance.LoadSong();
        }
    }
}
