﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerModeBehaviors : MonoBehaviour {

    private ModeManager.controllerModeEnum controllerMode = ModeManager.controllerModeEnum.grabMode;
    private VRTK.VRTK_InteractGrab[] interactGrabComponents;

	// Use this for initialization
	void Start () 
    {
		controllerMode = ModeManager.Instance.GetControllerMode();
	}
	
	// Update is called once per frame
	void Update () 
    {
        CheckControllerMode();
	}

    // TODO - make this into an event so I don't need to check in update
    void CheckControllerMode ()
    {
        if (controllerMode != ModeManager.Instance.GetControllerMode())
        {
            controllerMode = ModeManager.Instance.GetControllerMode();
            switch (controllerMode)
            {
                case ModeManager.controllerModeEnum.grabMode:
                    interactGrabComponents = GetComponents<VRTK.VRTK_InteractGrab>();
                    foreach (VRTK.VRTK_InteractGrab component in interactGrabComponents)
                    {
                        // We are turning off using the grip press to grab in Grab mode. This allows the grip to be used for moving every instrument
                        if (component.grabButton == VRTK.VRTK_ControllerEvents.ButtonAlias.GripPress)
                        {
                            component.enabled = false;
                        }
                    }
                    //interactGrabComponents[0].enabled = true;
                    //GetComponent<VRTK.VRTK_InteractGrab>().enabled = true;
                    GetComponent<VRTK.VRTK_InteractUse>().enabled = false;
                    GetComponent<VRTK.VRTK_StraightPointerRenderer>().validCollisionColor = Color.cyan;
                    GetComponent<VRTK.VRTK_StraightPointerRenderer>().invalidCollisionColor = Color.cyan;

                    // Make sure the correct radial control is active
                    transform.GetChild(0).gameObject.SetActive(true);
                    transform.GetChild(1).gameObject.SetActive(false);
                    break;

                case ModeManager.controllerModeEnum.parameterMode:
                    interactGrabComponents = GetComponents<VRTK.VRTK_InteractGrab>();
                    foreach (VRTK.VRTK_InteractGrab component in interactGrabComponents)
                    {
                        // We are making it so that you can not use the trigger click to pick up an instrument in parameter mode. We can still use the grip though
                        if (component.grabButton == VRTK.VRTK_ControllerEvents.ButtonAlias.TriggerClick)
                        {
                            component.enabled = false;
                        }
                    }
                    //interactGrabComponents[0].enabled = false;
                    //GetComponent<VRTK.VRTK_InteractGrab>().enabled = false;
                    GetComponent<VRTK.VRTK_InteractUse>().enabled = true;
                    GetComponent<VRTK.VRTK_StraightPointerRenderer>().validCollisionColor = Color.red;
                    GetComponent<VRTK.VRTK_StraightPointerRenderer>().invalidCollisionColor = Color.red;

                    // Make sure the correct radial control is active
                    transform.GetChild(0).gameObject.SetActive(true);
                    transform.GetChild(1).gameObject.SetActive(false);
                    break;

                case ModeManager.controllerModeEnum.automationMode:
                    interactGrabComponents = GetComponents<VRTK.VRTK_InteractGrab>();
                    foreach (VRTK.VRTK_InteractGrab component in interactGrabComponents)
                    {
                        // We are enabling both the grip and trigger to pick up instruments in automation mode. This allows you to choose how to move the automation
                        if (component.grabButton == VRTK.VRTK_ControllerEvents.ButtonAlias.TriggerClick)
                        {
                            component.enabled = true;
                        }
                    }
                    //interactGrabComponents[0].enabled = true;
                    //GetComponent<VRTK.VRTK_InteractGrab>().enabled = false;
                    GetComponent<VRTK.VRTK_InteractUse>().enabled = true;
                    GetComponent<VRTK.VRTK_StraightPointerRenderer>().validCollisionColor = Color.green;
                    GetComponent<VRTK.VRTK_StraightPointerRenderer>().invalidCollisionColor = Color.green;

                    // Make sure the correct radial control is active
                    transform.GetChild(0).gameObject.SetActive(false);
                    transform.GetChild(1).gameObject.SetActive(true);
                    break;
            }
        }
    }
}
