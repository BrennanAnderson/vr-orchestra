﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTimelineOnGrabbed : MonoBehaviour {

    Transform timelineContent;
    private bool canMove = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (GetComponent<SetTouched>().objectGrabbed)
        {
            if (canMove)
            {
                LoopScroll.Instance.MoveTimeline(GetComponent<AutomationNodeInfo>().timestamp);
                canMove = false;
            }
        }
        
        if (!GetComponent<SetTouched>().objectGrabbed)
        {
            canMove = true;
        }
	}
}
