﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetTextValueFromSliderNumber : MonoBehaviour {

    public bool isInDB = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetTextValue (Slider slider)
    {
        if (isInDB)
        {
            GetComponent<Text>().text = (slider.value / 100).ToString();
        }
        else
        {
            GetComponent<Text>().text = (slider.value).ToString();
        }
    }
}
