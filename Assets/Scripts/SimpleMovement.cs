﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (Input.GetAxis("Horizontal") > 0)
        {
            Vector3 currentPos = transform.position;
            transform.position = new Vector3(currentPos.x + 0.1f, currentPos.y, currentPos.z);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            Vector3 currentPos = transform.position;
			transform.position = new Vector3(currentPos.x  - 0.1f, currentPos.y, currentPos.z);
        }

        if (Input.GetAxis("Vertical") > 0)
        {
            Vector3 currentPos = transform.position;
            transform.position = new Vector3(currentPos.x, currentPos.y + 0.1f, currentPos.z);
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            Vector3 currentPos = transform.position;
            transform.position = new Vector3(currentPos.x, currentPos.y - 0.1f, currentPos.z);
        }

        /*
		if (Input.GetKey(KeyCode.Q))
		{
			Vector3 currentPos = transform.position;
			transform.position = new Vector3(currentPos.x, currentPos.y, currentPos.z + 0.1f);
		}
		else if (Input.GetKey(KeyCode.E))
		{
			Vector3 currentPos = transform.position;
			transform.position = new Vector3(currentPos.x, currentPos.y, currentPos.z - 0.1f);
		}
		else if (Input.GetKey(KeyCode.A))
		{
			Vector3 currentPos = transform.position;
			transform.position = new Vector3(currentPos.x  - 0.1f, currentPos.y, currentPos.z);
		}
		else if (Input.GetKey(KeyCode.D))
		{
			Vector3 currentPos = transform.position;
			transform.position = new Vector3(currentPos.x + 0.1f, currentPos.y, currentPos.z);
		}
		else if (Input.GetKey(KeyCode.S))
		{
			Vector3 currentPos = transform.position;
			transform.position = new Vector3(currentPos.x, currentPos.y - 0.1f, currentPos.z);
		}
		else if (Input.GetKey(KeyCode.W))
		{
			Vector3 currentPos = transform.position;
			transform.position = new Vector3(currentPos.x, currentPos.y + 0.1f, currentPos.z);
		}
        */
	}
}
