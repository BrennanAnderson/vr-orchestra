﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllersTracker : MonoBehaviour {

    public static ControllersTracker Instance;
    public Transform leftController;
    public Transform rightController;
    public Transform cameraRig;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        leftController = gameObject.GetComponent<VRTK.VRTK_SDKManager>().scriptAliasLeftController.transform;
        rightController = gameObject.GetComponent<VRTK.VRTK_SDKManager>().scriptAliasRightController.transform;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
