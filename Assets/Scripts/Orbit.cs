﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour {

    public GameObject orbitAround;
    public float horizontalSpeed;
    public float verticalSpeed;
    public float sideSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        OrbitAround();
	}

    void OrbitAround ()
    {
        transform.RotateAround(orbitAround.transform.position, Vector3.up, horizontalSpeed * Time.deltaTime);
        transform.RotateAround(orbitAround.transform.position, Vector3.right, verticalSpeed * Time.deltaTime);
        transform.RotateAround(orbitAround.transform.position, Vector3.forward, sideSpeed * Time.deltaTime);
    }
}
