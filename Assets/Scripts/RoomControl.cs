﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomControl : MonoBehaviour {

    public GameObject Ceiling;
    public GameObject Floor;
    public GameObject LeftWall;
    public GameObject RightWall;
    public GameObject FrontWall;
    public GameObject BackWall;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetWidth (Slider slider)
    {
        LeftWall.transform.position = new Vector3 ((slider.value / 2) * -1, 0, 0);
        RightWall.transform.position = new Vector3((slider.value / 2) , 0, 0);
    }

    public void SetDepth(Slider slider)
    {
        BackWall.transform.position = new Vector3(0, 0, (slider.value / 2) * -1);
        FrontWall.transform.position = new Vector3(0, 0, (slider.value / 2));
    }

    public void SetHeight(Slider slider)
    {
        Floor.transform.position = new Vector3(0, (slider.value / 2) * -1, 0);
        Ceiling.transform.position = new Vector3(0, (slider.value / 2), 0);
    }
}
