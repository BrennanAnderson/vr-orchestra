﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallInstrumentFunction : MonoBehaviour {

    Transform attachTransform;
    Transform targetInstrument;

    bool objectTouched = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SoloInstrumnet()
    {
        string targetObjectString = "tip";

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == targetObjectString)
            {
                attachTransform = transform.GetChild(i).GetChild(0);
            }
        }

        if (attachTransform.childCount > 0)
        {
            targetInstrument = attachTransform.GetChild(0);
            targetInstrument.GetComponent<VolumeControls>().SoloInstrument();
        }
        else
        {
            for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
            {
                if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SetTouched>().objectTouched)
                {
                    if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SetTouched>().touchingObject.transform == transform.parent.GetChild(1))
                    {
                        targetInstrument = PresetManager.Instance.instrumentsContainer.GetChild(i);
                        targetInstrument.GetComponent<VolumeControls>().SoloInstrument();
                    }
                }
            }
        }
    }

    public void MuteInstrumnet()
    {
        string targetObjectString = "tip";

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == targetObjectString)
            {
                attachTransform = transform.GetChild(i).GetChild(0);
            }
        }

        if (PresetManager.Instance.instrumentsContainer.parent == transform)
        {
            for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
            {
                PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<VolumeControls>().MuteInstrument();
            }
        }
        else if (attachTransform.childCount > 0)
        {
            targetInstrument = attachTransform.GetChild(0);
            targetInstrument.GetComponent<VolumeControls>().MuteInstrument();
        }
        else
        {
            for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
            {
                if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SetTouched>().objectTouched)
                {
                    if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SetTouched>().touchingObject.transform == transform.parent.GetChild(1))
                    {
                        targetInstrument = PresetManager.Instance.instrumentsContainer.GetChild(i);
                        targetInstrument.GetComponent<VolumeControls>().MuteInstrument();
                    }
                }
            }
        }
    }

    public void MoveAway()
    {
        string targetObjectString = "tip";

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == targetObjectString)
            {
                attachTransform = transform.GetChild(i).GetChild(0);
            }
        }

        if (PresetManager.Instance.instrumentsContainer.parent == transform)
        {
            for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
            {
                PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<Mover>().MoveAway(transform);
            }
        }
        else if (attachTransform.childCount > 0)
        {
            targetInstrument = attachTransform.GetChild(0);
            targetInstrument.GetComponent<Mover>().MoveAway(targetInstrument.GetComponent<SetTouched>().touchingObject.transform);
        }
        else
        {
         
            
            for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
            {
                if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SetTouched>().objectTouched)
                {
                    if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SetTouched>().touchingObject.transform == transform.parent.GetChild(1))
                    {
                        targetInstrument = PresetManager.Instance.instrumentsContainer.GetChild(i);
                        targetInstrument.GetComponent<Mover>().MoveAway(targetInstrument.GetComponent<SetTouched>().touchingObject.transform);
                    }
                }
            }
            
        }
    }

    public void MoveClose()
    {
        string targetObjectString = "tip";

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == targetObjectString)
            {
                attachTransform = transform.GetChild(i).GetChild(0);
            }
        }

        if (PresetManager.Instance.instrumentsContainer.parent == transform)
        {
            for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
            {
                PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<Mover>().MoveClose(transform);
            }
        }
        else if (attachTransform.childCount > 0)
        {
            targetInstrument = attachTransform.GetChild(0);
            targetInstrument.GetComponent<Mover>().MoveClose(targetInstrument.GetComponent<SetTouched>().touchingObject.transform);
        }
        else
        {
            for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
            {
                if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SetTouched>().objectTouched)
                {
                    if (PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<SetTouched>().touchingObject.transform == transform.parent.GetChild(1))
                    {
                        targetInstrument = PresetManager.Instance.instrumentsContainer.GetChild(i);
                        targetInstrument.GetComponent<Mover>().MoveClose(targetInstrument.GetComponent<SetTouched>().touchingObject.transform);
                    }
                }
            }
        }
    }

    private bool canCreateNode = true;

    public void CreateNode ()
    {
        if (AutomationManager.Instance.GetSelectedInstrument() != null)
        {
            canCreateNode = false;
            Vector3 controllerPos = transform.position;
            Vector3 controllerDirection = transform.forward;
            float spawnDistance = (AutomationManager.Instance.GetSelectedInstrument().transform.position - controllerPos).magnitude;

            Vector3 spawnPos = controllerPos + controllerDirection * spawnDistance;

            //Get forward vector with a distance of the instrument
            Vector3 test = transform.forward + new Vector3(0, 0, 10);
            AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().CreateAutomationNode(spawnPos);
            
            StartCoroutine("Timer");
        }
    }

    public void CreateNodeHold()
    {
        if (AutomationManager.Instance.GetSelectedInstrument() != null)
        {
            if (canCreateNode)
            {
                StopCoroutine("Timer");
                CreateNode();
            }
        }
    }

    IEnumerator Timer ()
    {
        yield return new WaitForSeconds(0.25f);
        canCreateNode = true;
        yield return null;
    }

    public void DeleteNode ()
    {
        string targetObjectString = "tip";

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == targetObjectString)
            {
                attachTransform = transform.GetChild(i).GetChild(0);
            }
        }

        if (attachTransform.childCount > 0)
        {
            targetInstrument = attachTransform.GetChild(0);
            AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().DeleteAutiomationNode(targetInstrument.gameObject);
        }
        else
        {
            for (int i = 0; i < AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.childCount; i++)
            {
                if (AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i).GetComponent<SetTouched>().objectTouched)
                {
                    if (AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i).GetComponent<SetTouched>().touchingObject.transform == transform.parent.GetChild(1))
                    {
                        targetInstrument = AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i);
                        AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().DeleteAutiomationNode(targetInstrument.gameObject);
                    }
                }
            }
        }
    }

    public void MoveAutomationClose ()
    {
        string targetObjectString = "tip";

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == targetObjectString)
            {
                attachTransform = transform.GetChild(i).GetChild(0);
            }
        }

        if (AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.parent == transform)
        {
            for (int i = 0; i < AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.childCount; i++)
            {
                PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<Mover>().MoveClose(transform);
            }
        }
        else if (attachTransform.childCount > 0)
        {
            targetInstrument = attachTransform.GetChild(0);
            targetInstrument.GetComponent<Mover>().MoveClose(targetInstrument.GetComponent<SetTouched>().touchingObject.transform);
        }
        else
        {
            for (int i = 0; i < AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.childCount; i++)
            {
                if (AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i).GetComponent<SetTouched>().objectTouched)
                {
                    if (AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i).GetComponent<SetTouched>().touchingObject.transform == transform.parent.GetChild(1))
                    {
                        targetInstrument = AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i);
                        targetInstrument.GetComponent<Mover>().MoveClose(targetInstrument.GetComponent<SetTouched>().touchingObject.transform);
                    }
                }
            }
        }
    }

    public void MoveAutomationAway ()
    {
        string targetObjectString = "tip";

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == targetObjectString)
            {
                attachTransform = transform.GetChild(i).GetChild(0);
            }
        }

        if (AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.parent == transform)
        {
            for (int i = 0; i < AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.childCount; i++)
            {
                PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<Mover>().MoveAway(transform);
            }
        }
        else if (attachTransform.childCount > 0)
        {
            targetInstrument = attachTransform.GetChild(0);
            targetInstrument.GetComponent<Mover>().MoveAway(targetInstrument.GetComponent<SetTouched>().touchingObject.transform);
        }
        else
        {
            for (int i = 0; i < AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.childCount; i++)
            {
                if (AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i).GetComponent<SetTouched>().objectTouched)
                {
                    if (AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i).GetComponent<SetTouched>().touchingObject.transform == transform.parent.GetChild(1))
                    {
                        targetInstrument = AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().AutomationNodesTransform.GetChild(i);
                        targetInstrument.GetComponent<Mover>().MoveAway(targetInstrument.GetComponent<SetTouched>().touchingObject.transform);
                    }
                }
            }
        }
    }
}
