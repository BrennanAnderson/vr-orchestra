﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteInstrument : MonoBehaviour {

    public GameObject instrumentToDelete;

	public Material closedVisual;
	public Material openVisual;

    private Color originalColor;
	// Use this for initialization
	void Start () 
    {
        transform.GetChild(0).GetComponent<MeshRenderer>().material = closedVisual;	
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (instrumentToDelete != null)
        {
            Delete();
        }
	}

    void OnTriggerEnter (Collider other)
    {
        instrumentToDelete = other.gameObject;

        if (other.GetComponent<SpriteRenderer>() != null)
        {
            originalColor = other.GetComponent<SpriteRenderer>().color;
            other.GetComponent<SpriteRenderer>().color = Color.red;
            transform.GetChild(0).GetComponent<MeshRenderer>().material = openVisual;
        }

		
    }

    void OnTriggerStay(Collider other)
    {
        instrumentToDelete = other.gameObject;
    }

    void OnTriggerExit(Collider other)
    {
        instrumentToDelete = null;
        if (other.GetComponent<SpriteRenderer>() != null)
        {
            other.GetComponent<SpriteRenderer>().color = originalColor;
            transform.GetChild(0).GetComponent<MeshRenderer>().material = closedVisual;
        }

		
    }

    void Delete ()
    {
        if (instrumentToDelete.transform.parent == PresetManager.Instance.instrumentsContainer)
        {
            if (instrumentToDelete.GetComponent<Instrument>() != null)
            {
                if (instrumentToDelete.GetComponent<SetTouched>().GetTimeSinceLastGrabbed() <= 1f)
                {
                    instrumentToDelete.GetComponent<InstrumentSetup>().DeleteInstrument();
                    instrumentToDelete = null;
                    transform.GetChild(0).GetComponent<MeshRenderer>().material = closedVisual;
                }
            }
        }
    }
}
