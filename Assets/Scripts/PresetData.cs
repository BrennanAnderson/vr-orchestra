﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresetData : ScriptableObject 
{
	//public Dictionary<Instrument,Vector3> instrumentPosition;
	public List<Instrument> instrument;
	public List<Vector3> position;
    public List<string> audioEventName;
    public List<string> audioFileName;
    public List<Color> instrumentColor;
    public List<float> audioDelay;
    public List<int> is3D;
}
