﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLineTexture : MonoBehaviour {

    private float scrollSpeed = 1f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        LineRenderer lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.material.SetTextureScale("_MainTex", new Vector2((lineRenderer.GetPosition(1) - lineRenderer.GetPosition(0)).magnitude * 4, 1));
        float offset = Time.time * scrollSpeed;
        lineRenderer.material.SetTextureOffset("_MainTex", new Vector2(-offset, 0));
	}
}
