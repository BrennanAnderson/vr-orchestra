﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleDetailedInfo : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ToggleDetails ()
    {
        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<PositionInfo>().ToggleDetails(GetComponent<Toggle>());
        }
    }
}
