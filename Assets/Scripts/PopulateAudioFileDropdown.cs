﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class PopulateAudioFileDropdown : MonoBehaviour {

    private List<string> audioFileList;
    private FileInfo[] info;
    private DirectoryInfo dir;

    public Dropdown folderDropdown;

	// Use this for initialization
	void Start () 
	{
		//dir = new DirectoryInfo(Application.dataPath + "/StreamingAssets");
        dir = new DirectoryInfo(Application.dataPath + "/StreamingAssets");
		audioFileList = new List<string>();
		Refresh();
		//GetComponent<Dropdown>().AddOptions(audioFileList);
	}

	public void Refresh()
	{
        if (folderDropdown.options[folderDropdown.value].text == "StreamingAssets")
        {
            dir = new DirectoryInfo(Application.dataPath + "/" + folderDropdown.options[folderDropdown.value].text);
        }
        else
        {
            dir = new DirectoryInfo(Application.dataPath + "/StreamingAssets/" + folderDropdown.options[folderDropdown.value].text);
        }

        GetComponent<Dropdown>().ClearOptions();
		audioFileList.Clear();

		info = dir.GetFiles();
		for (int i = 0; i < info.Length; i++)
		{
            if (info[i].Name.EndsWith(".ogg") || info[i].Name.EndsWith(".wav") || info[i].Name.EndsWith(".mp3"))
			{
				audioFileList.Add(info[i].Name);
			}
		}

        audioFileList.Insert(0,"*Empty*");
        GetComponent<Dropdown>().AddOptions(audioFileList);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
