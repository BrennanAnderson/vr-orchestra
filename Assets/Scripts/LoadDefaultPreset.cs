﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadDefaultPreset : MonoBehaviour {

    public int defaultInt;

	// Use this for initialization
	void Start () 
	{
        ToggleUI.Instance.Toggle(true);
		PresetManager.Instance.SetCurrentPreset(defaultInt);
        SongManager.Instance.SetCurrentSong(defaultInt);
		PresetManager.Instance.LoadPreset();
        ToggleUI.Instance.Toggle(true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
