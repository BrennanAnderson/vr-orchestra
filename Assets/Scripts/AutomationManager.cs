﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using System.IO;
# endif

public class AutomationManager : MonoBehaviour {

    public Transform AutomationContainer;
    public Toggle recordButton;
    public Text currentNodeTimeText;
    private GameObject selectedInstrument;
    public bool canRecord = false;
    public GameObject automationToggleGroupObject;
    private Toggle noneMovement;
    private Toggle loopMovement;
    private Toggle pingPongMovement;

    private FileInfo[] automationInfo;
    private DirectoryInfo automationDir;

    [SerializeField]
    PresetData presetData;

    public static AutomationManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

	// Use this for initialization
	void Start () 
    {
        noneMovement = automationToggleGroupObject.transform.GetChild(0).GetComponent<Toggle>();
        loopMovement = automationToggleGroupObject.transform.GetChild(1).GetComponent<Toggle>();
        pingPongMovement = automationToggleGroupObject.transform.GetChild(2).GetComponent<Toggle>();

        automationDir = new DirectoryInfo(Application.dataPath + "/Resources/Automation Data");
        Refresh();
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public GameObject GetSelectedInstrument ()
    {
        return selectedInstrument;
    }

    public void SetSelectedInstrument (GameObject instrumentObject)
    {
        // Hide the current instrument's automation data when a new instrument is selected
        if (selectedInstrument != null && selectedInstrument != instrumentObject)
        {
            selectedInstrument.GetComponent<AutomationData>().HideAutomation();
            // Turn off its green outline
            selectedInstrument.transform.GetChild(3).gameObject.SetActive(false);

            // If its recording, stop recording
            if (selectedInstrument.GetComponent<AutomationData>().isRecording)
            {
                // TODO - link this to the record button to make it turn off.
                selectedInstrument.GetComponent<AutomationData>().RecordAutomation();
                recordButton.isOn = !recordButton.isOn;
            }

            // De-activate grip move component on the selected instrument's automation. Both on automation nodes and lines
            selectedInstrument.GetComponent<AutomationData>().AutomationNodesTransform.GetComponent<GripMove>().enabled = false;
            selectedInstrument.GetComponent<AutomationData>().AutomationLinesTransform.GetComponent<GripMove>().enabled = false;

        }

        if (selectedInstrument != instrumentObject)
        {
            // set the new selected instrument
            selectedInstrument = instrumentObject;

            // Show the new instrument's automation data
            selectedInstrument.GetComponent<AutomationData>().ShowAutomation();

            // Turn on its green outline
            selectedInstrument.transform.GetChild(3).gameObject.SetActive(true);

            // Activate grip move component on the selected instrument's automation. Both on automation nodes and lines
            selectedInstrument.GetComponent<AutomationData>().AutomationNodesTransform.GetComponent<GripMove>().enabled = true;
            selectedInstrument.GetComponent<AutomationData>().AutomationLinesTransform.GetComponent<GripMove>().enabled = true;

            // Set up the automation nodes on the timeline
            LoopScroll.Instance.SetupNodesOnTimeline();
        }

        SetAutomationMovementToggleVisuals();
    }

    public void PlayAutomation ()
    {
        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i ++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<AutomationData>().FollowAutomation();
        }

        LoopScroll.Instance.PlayTimeline();

        if (canRecord)
        {
            CancelRecord();
        }
    }

    public void StopAutomation ()
    {
        // Stop all instruments from automating. Put them back to the position of the first node
        for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
        {
            PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<AutomationData>().StopAutomation();
        }

        LoopScroll.Instance.StopTimeline();

        if (canRecord)
        {
            CancelRecord();
        }
    }

    // TODO - make automation timeline have a spot for you to set record granularity in seconds, and pass that into the below function
    
    public void RecordEnable()
    {
        if (selectedInstrument != null)
        {
            canRecord = !canRecord;

            if (!AutomationManager.Instance.GetSelectedInstrument().transform.GetChild(3).gameObject.activeInHierarchy)
            {
                AutomationManager.Instance.GetSelectedInstrument().transform.GetChild(3).gameObject.SetActive(true);
            }

            ParticleSystem.MainModule settings = AutomationManager.Instance.GetSelectedInstrument().transform.GetChild(3).gameObject.GetComponent<ParticleSystem>().main;
            settings.startColor = new ParticleSystem.MinMaxGradient(Color.red);


            if (!canRecord)
            {
                StopAutomation();
                selectedInstrument.GetComponent<AutomationData>().StopRecordAutomation();

                settings = AutomationManager.Instance.GetSelectedInstrument().transform.GetChild(3).gameObject.GetComponent<ParticleSystem>().main;
                settings.startColor = new ParticleSystem.MinMaxGradient(Color.green);
            }
        }
        else
        {
            recordButton.isOn = false;
        }
        // Want to handle if the instrument is null which instrumentdo we try to record? Or do we prevent the button from being clicked?
    }

    public void CancelRecord()
    {
        recordButton.isOn = false;
        canRecord = false;
        ParticleSystem.MainModule settings = AutomationManager.Instance.GetSelectedInstrument().transform.GetChild(3).gameObject.GetComponent<ParticleSystem>().main;
        settings.startColor = new ParticleSystem.MinMaxGradient(Color.green);
        selectedInstrument.GetComponent<AutomationData>().StopRecordAutomation();
    }
    
    public void RecordAutomation ()
    {
        if (canRecord)
        {
            selectedInstrument.GetComponent<AutomationData>().RecordAutomation();
        }
    }

    public void SetCurrentTimecode(float time)
    {
        // parse seconds into minutes and seconds and milliseconds
        float minutes = Mathf.Floor(time / 60);
        float seconds = Mathf.Floor((time % 60));
        float milliseconds = Mathf.Floor(((time * 1000) % 1000));

        // print time to the display
        currentNodeTimeText.text = minutes.ToString("00") + ":" + seconds.ToString("00") + "." + milliseconds.ToString("0000");
    }

    public void SetCurrentInstrumentAutomationMovement ()
    {
        if (selectedInstrument != null)
        {
            if (noneMovement.isOn)
            {
                selectedInstrument.GetComponent<AutomationData>().automationMovementType = AutomationData.automationMovementTypeEnum.None;
                //Debug.Log("Setting movement to none");
            }
            else if (loopMovement.isOn)
            {
                selectedInstrument.GetComponent<AutomationData>().automationMovementType = AutomationData.automationMovementTypeEnum.Loop;
                //Debug.Log("Setting movement to loop");
            }
            else if (pingPongMovement.isOn)
            {
                selectedInstrument.GetComponent<AutomationData>().automationMovementType = AutomationData.automationMovementTypeEnum.PingPong;
                //Debug.Log("Setting movement to ping pong");
            }
        }
    }

    public void SetAutomationMovementToggleVisuals ()
    {
        AutomationData.automationMovementTypeEnum selectedInstrumentEnum = selectedInstrument.GetComponent<AutomationData>().automationMovementType;

        switch (selectedInstrumentEnum)
        {
            case AutomationData.automationMovementTypeEnum.None :
                noneMovement.isOn = true;
                break;

            case AutomationData.automationMovementTypeEnum.Loop :
                loopMovement.isOn = true;
                break;

            case AutomationData.automationMovementTypeEnum.PingPong :
                pingPongMovement.isOn = true;
                break;
        }
    }


    /// <summary>
    /// This is for saving and loading automation data into and from scriptable objects
    /// </summary>

#if UNITY_EDITOR
    void Refresh()
    {
        automationInfo = automationDir.GetFiles("*.asset");
    }

    void CreateSaveDirectoryAutomation()
    {
        string filePath = Application.dataPath + "/Resources";
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets", "Resources");
        filePath += "/Automation Data";
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets/Resources", "Automation Data");
        AssetDatabase.Refresh();
    }

    public void SaveAutomationToFile()
    {
        // Loop through all automation node containers
        string filePath = Application.dataPath + "/Resources/Automation Data";
        if (!Directory.Exists(filePath))
            CreateSaveDirectoryAutomation();

        foreach (Transform nodeContainer in AutomationContainer)
        {
            // Loop through the actual nodes
            if (nodeContainer.name.Contains("AutomationNodes") && nodeContainer.childCount > 0)
            {
                AutomationPresetData automationConfiguration = ScriptableObject.CreateInstance<AutomationPresetData>();
                automationConfiguration.instrumentSet = PresetManager.Instance.currentPresetText.text;
                automationConfiguration.timestamps = new List<float>();
                automationConfiguration.nodeOrder = new List<int>();
                string aFileName = nodeContainer.GetChild(0).GetComponent<AutomationNodeInfo>().myInstrumnent.GetComponent<InstrumentInfo>().audioFileName;
                automationConfiguration.audioFileName = aFileName;
                automationConfiguration.position = new List<Vector3>();
                automationConfiguration.loopType = nodeContainer.GetChild(0).GetComponent<AutomationNodeInfo>().myInstrumnent.GetComponent<AutomationData>().automationMovementType;
                foreach (Transform node in nodeContainer)
                {
                    //These are the actual nodes
                    automationConfiguration.timestamps.Add(node.GetComponent<AutomationNodeInfo>().timestamp);
                    automationConfiguration.nodeOrder.Add(node.GetComponent<AutomationNodeInfo>().nodePosition);
                    automationConfiguration.position.Add(node.transform.position);
                }
                string subFileName = (nodeContainer.GetChild(0).GetComponent<AutomationNodeInfo>().myInstrumnent.name + "_Automation Data");
                string fileName = string.Format("Assets/Resources/Automation Data/{1}.asset", filePath, subFileName);
                AssetDatabase.CreateAsset(automationConfiguration, fileName);
            }
            Refresh();
        }
        
    }

    public AutomationPresetData GetAutomationAssetName(string presetName, string instrumentName)
    {
        AutomationPresetData retValue = null;
        Refresh();
        foreach (FileInfo file in automationInfo)
        {
            string fileName = file.Name;
            AutomationPresetData automation = Resources.Load("Automation Data/" + fileName.Replace(".asset", "")) as AutomationPresetData;
            if (automation.instrumentSet == presetName)
            {
                if (fileName.Replace("_Automation Data.asset", "") == instrumentName)
                {
                    retValue = automation;
                }
            }
        }
        return retValue;
    }

    public void LoadAutomationFromFile ()
    {
        Refresh();
        foreach (FileInfo file in automationInfo)
        {
            string fileName = file.Name;
            AutomationPresetData automation = Resources.Load("Automation Data/" + fileName.Replace(".asset", "")) as AutomationPresetData;
            if (automation.instrumentSet == PresetManager.Instance.currentPresetText.text)
            {
                // Now we need to place the nodes in the correct spots,  then draw all the lines

                //find the instrument playing the same audio file as audioFileName, and make it the selected instrument
                foreach(Transform t in PresetManager.Instance.instrumentsContainer)
                {
                    string instrumentName = t.gameObject.name;
                    if (fileName.Replace("_Automation Data.asset", "") == instrumentName)
                    {
                        //Set the instrument as the selected instrument
                        SetSelectedInstrument(t.gameObject);
                        
                        // Delete all current nodes
                        foreach (Transform node in selectedInstrument.GetComponent<AutomationData>().AutomationNodesTransform)
                        {
                            selectedInstrument.GetComponent<AutomationData>().DeleteAutiomationNode(node.gameObject);
                        }
                        break;
                    }
                }

                //Call create node on the instrument and pass it all of the info from the automation node data
                int count = 0;
                AutomationNodeInfo aNode = null;
                foreach (Vector3 v in automation.position)
                {
                    aNode = selectedInstrument.GetComponent<AutomationData>().CreateAndReturnAutomationNode(automation.position[count], automation.timestamps[count], automation.nodeOrder[count]);
                    count++;
                }
                aNode.myInstrumnent.GetComponent<AutomationData>().automationMovementType = automation.loopType;
            }
        }
        GetSelectedInstrument().GetComponent<AutomationData>().HideAutomation();

        if (!canRecord)
        {
            GetSelectedInstrument().transform.GetChild(3).gameObject.SetActive(false);
        }
    }

#endif
}
