﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateMenu : MonoBehaviour {

    public GameObject menuUI;
    public GameObject volumeControls;
    public GameObject instrumentMenu;
    public GameObject keyboard;
    public GameObject saveAsMenu;
    public GameObject reverbMenu;
    public GameObject mainMenu;
    public Transform rightController;
    public Transform leftController;
    VRTK.VRTK_ControllerEvents rightControllerEvents;
    VRTK.VRTK_ControllerEvents leftControllerEvents;
    private bool canInteract = true;

    void Awake()
    {
        
    }

	// Use this for initialization
	void Start () 
    {
        leftController = ControllersTracker.Instance.leftController;
        rightController = ControllersTracker.Instance.rightController;
		rightControllerEvents = rightController.GetComponent<VRTK.VRTK_ControllerEvents>();
        leftControllerEvents = leftController.GetComponent<VRTK.VRTK_ControllerEvents>();
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        //Debug.Log(rightController.GetComponent<VRTK.VRTK_ControllerEvents>());
        if (rightControllerEvents.IsButtonPressed(VRTK.VRTK_ControllerEvents.ButtonAlias.ButtonTwoPress) || leftControllerEvents.IsButtonPressed(VRTK.VRTK_ControllerEvents.ButtonAlias.ButtonTwoPress))
        {
            if (canInteract)
            {
                if (menuUI.activeInHierarchy)
                {
                    menuUI.SetActive(false);
                    keyboard.SetActive(false);
                    saveAsMenu.SetActive(false);
                    reverbMenu.SetActive(true);
                    mainMenu.SetActive(true);
                    //volumeControls.SetActive(true);
                }
                else
                {
                    menuUI.SetActive(true);
                    //volumeControls.SetActive(false);
                    instrumentMenu.SetActive(false);
                }
                canInteract = !canInteract;
                StartCoroutine(Wait());
            }
        }
	}

    IEnumerator Wait ()
    {
        yield return new WaitForSeconds(0.5f);
        canInteract = !canInteract;
    }
}
