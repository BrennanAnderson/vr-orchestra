﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomationPresetData : ScriptableObject 
{
    public string instrumentSet;
    public List<float> timestamps;
    public List<int> nodeOrder;
    public string audioFileName;
    public List<Vector3> position;
    public AutomationData.automationMovementTypeEnum loopType;
}
