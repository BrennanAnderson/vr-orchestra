﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeManager : MonoBehaviour {

    public enum controllerModeEnum {grabMode, parameterMode, automationMode}
    private controllerModeEnum controllerMode = controllerModeEnum.grabMode;
    

    public static ModeManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetControllerMode (controllerModeEnum selectedControllerMode)
    {
        switch (selectedControllerMode)
        {
            case controllerModeEnum.grabMode:
                controllerMode = controllerModeEnum.grabMode;
                if (AutomationManager.Instance.GetSelectedInstrument() != null)
                {
                    AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().HideAutomation();

                    if (!AutomationManager.Instance.canRecord)
                    {
                        AutomationManager.Instance.GetSelectedInstrument().transform.GetChild(3).gameObject.SetActive(false);
                    }
                }
                //Debug.Log("You are in grab mode");
                break;

            case controllerModeEnum.parameterMode:
                controllerMode = controllerModeEnum.parameterMode;
                if (AutomationManager.Instance.GetSelectedInstrument() != null)
                {
                    AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().HideAutomation();
                    AutomationManager.Instance.GetSelectedInstrument().transform.GetChild(3).gameObject.SetActive(false);
                }
                //Debug.Log("You are in parameter mode");
                break;

            case controllerModeEnum.automationMode:
                controllerMode = controllerModeEnum.automationMode;
                if (AutomationManager.Instance.GetSelectedInstrument() != null)
                {
                    AutomationManager.Instance.GetSelectedInstrument().GetComponent<AutomationData>().ShowAutomation();
                    AutomationManager.Instance.GetSelectedInstrument().transform.GetChild(3).gameObject.SetActive(true);
                }
                //Debug.Log("You are in automation modes");
                break;
        }
    }

    public controllerModeEnum GetControllerMode ()
    {
        return controllerMode;
    }
}
