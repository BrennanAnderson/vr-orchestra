﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SongManager : MonoBehaviour {

    public Transform instrumentsContainer;

    public Text selectedSongText;
    public Text currentSongText;
    public Text inputSaveName;
    public Text confirmationText;

    private int selectedSongInt = 1;
    private int currentSongInt = 1;
    private string currentSongName = "VR_Orchestra";
    public string saveSongName = "Default";
    private string[] songArray;

    public static SongManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {

    }

    public void SetupFactorySongs()
    {
        SetupFactoryHelper(Resources.Load("Songs/Empty") as PresetData );
        SetupFactoryHelper(Resources.Load("Songs/8-Bit Frantic Battle") as PresetData );
        SetupFactoryHelper(Resources.Load("Songs/Bohemian Rhapsody") as PresetData );
        SetupFactoryHelper(Resources.Load("Songs/VR_Orchestra") as PresetData );
        SetupFactoryHelper(Resources.Load("Songs/Wow_Eagle of Dreanor") as PresetData );
        SetupFactoryHelper(Resources.Load("Songs/Hide The Sunlight") as PresetData );
        SetupFactoryHelper(Resources.Load("Songs/New Moon") as PresetData );
        SetupFactoryHelper(Resources.Load("Songs/Hold On") as PresetData );
        SetupFactoryHelper(Resources.Load("Songs/Blue Agave") as PresetData );
        SetupFactoryHelper(Resources.Load("Songs/16-Bit Difficult Battle") as PresetData );
        
        PlayerPrefs.SetInt("PlayedBefore", 1);
    }

    void SetupFactoryHelper(PresetData song)
    {
        int count = 0;
        foreach (Instrument v in song.instrument)
        {
            // Save the instrument into the song
            ES2.Save(song.instrument[count].ToString(), "Song_" + song.name + "_Instrument_" + count.ToString());

            // Save the Fabric Event Name into the instrument in the song
            ES2.Save(song.audioEventName[count].ToString(), song.instrument[count].ToString() + count.ToString() + "_Song_" + song.name + "_AudioEventName_");

            // Save the audio file name into the instrument song
            ES2.Save(song.audioFileName[count].ToString(), song.instrument[count].ToString() + count.ToString() + "_Song_" + song.name + "_AudioFileName_");

            // Save the audio delay into our instruments 
            ES2.Save(song.audioDelay[count], song.instrument[count].ToString() + count.ToString() + "_Song_" + song.name + "_AudioDelayTime_");

            // Save whether the instrument is played in 2D or 3D
            ES2.Save(song.is3D[count], song.instrument[count].ToString() + count.ToString() + "_Song_" + song.name + "_Is3D_");

            count++;
        }

        // Save how many instruments are in our song
        ES2.Save(count, "Song_" + song.name + "NumInstruments");

        // Add the song to our list of songs
        List<string> songListTmp = ES2.LoadList<string>("Song_List");
        songListTmp.Add(song.name);
        ES2.Save(songListTmp, "Song_List");
    }

    public void SetCurrentSong(int songInt)
    {
        selectedSongInt = songInt;
    }

    void RefreshsongList()
    {
        songArray = ES2.LoadList<string>("Song_List").ToArray();
    }


    // TODO - these two functions create and out of range exception when you delete all player prefs and try to cycle through songs. Fix this
    public void NextSong()
    {
        RefreshsongList();
        selectedSongInt = (int)Mathf.Repeat(selectedSongInt + 1, songArray.Length);
        selectedSongText.text = songArray[selectedSongInt];

    }

    public void PreviousSong()
    {
        RefreshsongList();
        selectedSongInt = (int)Mathf.Repeat(selectedSongInt - 1, songArray.Length);
        selectedSongText.text = songArray[selectedSongInt];
    }

    public void SetSongName(Text chosenName)
    {
        saveSongName = chosenName.text;
    }

    //
    // Saving Stuff
    //

    public void SaveSong()
    {
        RefreshsongList();

        if (currentSongName != "" || currentSongName != null)
        {
            currentSongInt = songArray.Length;
            selectedSongInt = songArray.Length;
            saveSongName = inputSaveName.text;
            currentSongName = saveSongName;
            selectedSongText.text = currentSongName;
            currentSongText.text = currentSongName;
            ES2.Save(instrumentsContainer.childCount, "Song_" + currentSongName + "NumInstruments");
            List<string> songListTmp = ES2.LoadList<string>("Preset_List");
            songListTmp.Add(currentSongName);
            ES2.Save(songListTmp, "Song_List");
            RefreshsongList();
            SaveInstrumentData();
            confirmationText.text = "Saved new song: \n" + currentSongName;
        }
        else
        {
            //TODO - this isn't working for some reason
            confirmationText.text = "Could not save song \"\". Please enter a name when saving...";
        }
    }

    public void SaveOverSong()
    {
        if (selectedSongText.text != "" && selectedSongText.text != "Empty")
        {
            RefreshsongList();
            ES2.Save(instrumentsContainer.childCount, "Song_" + currentSongName + "NumInstruments");
            SaveInstrumentData();
            confirmationText.text = "Saved existing song: \n" + currentSongName;
        }
        else
        {
            confirmationText.text = "Could not save over Empty song. Please choose another...";
        }
    }

    void SaveInstrumentData()
    {
        for (int i = 0; i < instrumentsContainer.childCount; i++)
        {
            Transform instrumentObject = instrumentsContainer.GetChild(i);

            if (instrumentObject.GetComponent<InstrumentInfo>().audioEventName != "")
            {
                ES2.Save(instrumentObject.GetComponent<InstrumentInfo>().audioEventName.ToString(), instrumentObject.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_AudioEventName_");
                ES2.Save(instrumentObject.GetComponent<InstrumentInfo>().audioFileName.ToString(), instrumentObject.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_AudioFileName_");
                ES2.Save(instrumentObject.GetComponent<InstrumentInfo>().audioDelay, instrumentObject.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_AudioDelayTime_");
                ES2.Save(instrumentObject.GetComponent<InstrumentInfo>().is3D, instrumentObject.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_Is3D_");
                ES2.Save(instrumentsContainer.GetChild(i).GetComponent<InstrumentInfo>().instrument.ToString(), "Song_" + currentSongName + "_Instrument_" + i.ToString());
            }
        }
    }

    //
    // Loading Stuff
    //

    public void LoadSong()
    {
        RefreshsongList();
        selectedSongText.text = songArray[currentSongInt];
        currentSongInt = selectedSongInt;
        currentSongName = songArray[currentSongInt];
        selectedSongText.text = currentSongName;
        currentSongText.text = currentSongName;
        confirmationText.text = "Loaded song: \n" + currentSongName;
        InstrumentIconSorter.Instance.SetInfoOnPanel(currentSongName, ES2.Load<int>("Song_" + currentSongName + "NumInstruments"));
        StartCoroutine(LoadInstrumentData());
    }

    IEnumerator LoadInstrumentData()
    {
        yield return null;

        for (int f = 0; f < instrumentsContainer.childCount; f++)
        {
            instrumentsContainer.GetChild(f).GetComponent<InstrumentSetup>().UnloadAudio();
        }

        yield return null;

        for (int i = 0; i < ES2.Load<int>("Song_" + currentSongName + "NumInstruments"); i++)
        {
            InstrumentIconSorter.Instance.AddIconToSongPanel(ES2.Load<string>("Song_" + currentSongName + "_Instrument_" + i.ToString()));
            // Here we search through the current instruments and load the audio data onto them.
            Instrument targetInstrument = (Instrument)System.Enum.Parse(typeof(Instrument), ES2.Load<string>("Song_" + currentSongName + "_Instrument_" + i.ToString()));

            for (int x = 0; x < instrumentsContainer.childCount; x++)
            {
                if (instrumentsContainer.GetChild(x).GetComponent<InstrumentInfo>().instrument == targetInstrument)
                {
                    // This check makes sure that if there are two or more of the same instrument, it will skip it if its has had the audio asigned
                    if (instrumentsContainer.GetChild(x).GetComponent<InstrumentInfo>().audioEventName == "")
                    {
                        instrumentsContainer.GetChild(x).GetComponent<InstrumentInfo>().audioEventName = ES2.Load<string>(instrumentsContainer.GetChild(x).GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_AudioEventName_");
                        instrumentsContainer.GetChild(x).GetComponent<InstrumentInfo>().audioFileName = ES2.Load<string>(instrumentsContainer.GetChild(x).GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_AudioFileName_");
                        instrumentsContainer.GetChild(x).GetComponent<InstrumentInfo>().audioDelay = ES2.Load<float>(instrumentsContainer.GetChild(x).GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_AudioDelayTime_");
                        instrumentsContainer.GetChild(x).GetComponent<InstrumentInfo>().is3D = ES2.Load<int>(instrumentsContainer.GetChild(x).GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_Is3D_");

                        instrumentsContainer.GetChild(x).GetComponent<InstrumentSetup>().SetAudio();
                        instrumentsContainer.GetChild(x).GetComponent<InstrumentSetup>().PlayAudio();

                        if (!instrumentsContainer.GetChild(x).GetComponent<VolumeControls>().audible)
                        {
                            instrumentsContainer.GetChild(x).GetComponent<VolumeControls>().MuteInstrument();
                        }

                        InstrumentIconSorter.Instance.CheckIfInstrumentIsSpawned(ES2.Load<string>("Song_" + currentSongName + "_Instrument_" + i.ToString()));

                        break;
                    }
                }
            }           
        }
        yield return null;
    }

    //
    // Deleting Stuff
    //

    // Choose carfully the song we are deleting. Do we want to delete the currently loaded song, or the currently selected one?

    public void DeleteSong()
    {
        if (selectedSongText.text != "" && selectedSongText.text != "Empty")
        {
            currentSongInt = 0;
            selectedSongInt = 0;
            confirmationText.text = "Deleted song: \n" + selectedSongText.text;
            DeleteInstrumentData();
            List<string> songListTmp = ES2.LoadList<string>("Song_List");
            songListTmp.Remove(selectedSongText.text);
            ES2.Save(songListTmp, "Song_List");

            if (selectedSongText.text == currentSongName)
            {
                currentSongName = "";
                currentSongText.text = currentSongName;
            }

            selectedSongText.text = currentSongName;
        }
        else
        {
            confirmationText.text = "Could not delete song. Please choose another...";
        }
    }

    void DeleteInstrumentData()
    {
        for (int i = 0; i < instrumentsContainer.childCount; i++)
        {
            Transform instrumentObject = instrumentsContainer.GetChild(i);

            ES2.Delete("Song_" + currentSongName + "_Instrument_" + i.ToString());
            ES2.Delete(instrumentObject.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_AudioEventName_");
            ES2.Delete(instrumentObject.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_AudioFileName_");
            ES2.Delete(instrumentObject.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_AudioDelayTime_");
            ES2.Delete(instrumentObject.GetComponent<InstrumentInfo>().instrument.ToString() + i.ToString() + "_Song_" + currentSongName + "_Is3D_");
        }
    }
}
