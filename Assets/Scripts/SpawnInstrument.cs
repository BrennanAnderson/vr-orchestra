﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnInstrument : MonoBehaviour {

    public bool syncAudioOnSpawn = true;
    public GameObject instrumentPrefab;

    public string instrumentString;
    public string audioFile = "*Empty*";
    public Dropdown audioFileDropdown;
    public Dropdown instrumentDropdown;
    public Dropdown audioFolderDropdown;
    public ToggleGroup toggleGroup;

    public static SpawnInstrument Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

	// Use this for initialization
	public void SetupInitDropdowns () 
    {
        string replace = "_Anim_0";
        instrumentString = instrumentDropdown.options[instrumentDropdown.value].image.name.Replace(replace, "");
        SetAudioFile(audioFileDropdown);
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void SetSync (Toggle toggle)
    {
        syncAudioOnSpawn = toggle.isOn;
    }

    public void SetAudioFile (Dropdown dropdown)
    {
        if (audioFolderDropdown.options[audioFolderDropdown.value].text == "StreamingAssets")
        {
            audioFile = dropdown.options[dropdown.value].text;
        }
        else
        {
            audioFile = audioFolderDropdown.options[audioFolderDropdown.value].text + "/" + dropdown.options[dropdown.value].text;
        }
    }

    public void SetAudioFile (string audioFileName)
    {
        audioFile = audioFileName;
    }

    public void SetInstrumentString(Dropdown dropdown)
    {
        string replace = "_Anim_0";
        instrumentString = dropdown.options[dropdown.value].image.name.Replace(replace, "");
    }

    public void SetInstrumentString (string instrumentStringName)
    {
        instrumentString = instrumentStringName;
    }

    public void Spawn ()
    {
        StartCoroutine(SpawnCoroutine());
    }

    public void SpawnDontLoad()
    {
        StartCoroutine(SpawnDontLoadCoroutine());
    }

    // TODO - BUG WARNING - If I spawn a non-synced instrument, then try to spawn a synced instrument, it doubles audio of all instruments. 
    // THIS IS A FABRIC BUG having to do with the external group component. The components are deleted and stopped, but the  audio sources keep playing
    IEnumerator SpawnCoroutine ()
    {
        //Debug.Log(syncAudioOnSpawn);
        if (instrumentString != "")
        {
            if (syncAudioOnSpawn)
            {
                for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
                {
                    PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().StopAudio();
                }
            }

            yield return null;
            GameObject instrument = Instantiate(instrumentPrefab) as GameObject;
            instrument.GetComponent<InstrumentInfo>().instrument = (Instrument)System.Enum.Parse(typeof(Instrument), instrumentString);
            instrument.name = instrument.GetComponent<InstrumentInfo>().instrument.ToString() + PresetManager.Instance.instrumentsContainer.childCount.ToString();
            instrument.GetComponent<InstrumentInfo>().audioEventName = instrument.GetComponent<InstrumentInfo>().instrument.ToString() + PresetManager.Instance.instrumentsContainer.childCount.ToString();
            instrument.GetComponent<InstrumentInfo>().audioFileName = audioFile;
            
            // All this for the color
            Toggle activeToggle = null;

            for (int i = 0; i < toggleGroup.transform.childCount; i++)
            {
                if (toggleGroup.transform.GetChild(i).GetComponent<Toggle>().isOn)
                {
                    activeToggle = toggleGroup.transform.GetChild(i).GetComponent<Toggle>();
                }
            }

            instrument.GetComponent<InstrumentInfo>().color = activeToggle.colors.normalColor;

            instrument.transform.SetParent(PresetManager.Instance.instrumentsContainer);
            instrument.transform.position = transform.position;
            instrument.SetActive(true);
            instrument.GetComponent<InstrumentSetup>().SetVisuals();
            instrument.GetComponent<InstrumentSetup>().SetAudio();
            

            if (!syncAudioOnSpawn)
            {
                instrument.GetComponent<InstrumentSetup>().PlayAudio();
            }

            yield return null;

            if (syncAudioOnSpawn)
            {
                for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
                {
                    PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().SetAudio();
                    PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().PlayAudio();
                }
            }

			if (instrument.GetComponent<InstrumentInfo>().audioFileName == "*Empty*") 
			{
				SongManager.Instance.LoadSong ();
			}

            yield return null;
        }
        yield return null;
    }

    IEnumerator SpawnDontLoadCoroutine()
    {
        //Debug.Log(syncAudioOnSpawn);
        if (instrumentString != "")
        {
            if (syncAudioOnSpawn)
            {
                for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
                {
                    PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().StopAudio();
                }
            }

            yield return null;
            GameObject instrument = Instantiate(instrumentPrefab) as GameObject;
            instrument.GetComponent<InstrumentInfo>().instrument = (Instrument)System.Enum.Parse(typeof(Instrument), instrumentString);
            instrument.name = instrument.GetComponent<InstrumentInfo>().instrument.ToString() + PresetManager.Instance.instrumentsContainer.childCount.ToString();
            instrument.GetComponent<InstrumentInfo>().audioEventName = instrument.GetComponent<InstrumentInfo>().instrument.ToString() + PresetManager.Instance.instrumentsContainer.childCount.ToString();
            instrument.GetComponent<InstrumentInfo>().audioFileName = audioFile;

            // All this for the color
            Toggle activeToggle = null;

            for (int i = 0; i < toggleGroup.transform.childCount; i++)
            {
                if (toggleGroup.transform.GetChild(i).GetComponent<Toggle>().isOn)
                {
                    activeToggle = toggleGroup.transform.GetChild(i).GetComponent<Toggle>();
                }
            }

            instrument.GetComponent<InstrumentInfo>().color = activeToggle.colors.normalColor;

            instrument.transform.SetParent(PresetManager.Instance.instrumentsContainer);
            instrument.transform.position = transform.position;
            instrument.SetActive(true);
            instrument.GetComponent<InstrumentSetup>().SetVisuals();
            instrument.GetComponent<InstrumentSetup>().SetAudio();


            if (!syncAudioOnSpawn)
            {
                instrument.GetComponent<InstrumentSetup>().PlayAudio();
            }

            yield return null;

            if (syncAudioOnSpawn)
            {
                for (int i = 0; i < PresetManager.Instance.instrumentsContainer.childCount; i++)
                {
                    PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().SetAudio();
                    PresetManager.Instance.instrumentsContainer.GetChild(i).GetComponent<InstrumentSetup>().PlayAudio();
                }
            }
            yield return null;
        }
        yield return null;
    }
}
