﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MySceneChanger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeScene (string sceneName)
    {
        ScreenFader.Instance.EndScene(sceneName);
    }
}
