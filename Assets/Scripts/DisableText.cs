﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisableText : MonoBehaviour {

	public Text textToListenTo;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (textToListenTo.text != "")
		{
			gameObject.SetActive(false);
		}
	}
}
