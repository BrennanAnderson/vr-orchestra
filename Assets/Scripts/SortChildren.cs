﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortChildren : MonoBehaviour {

    private bool sorted = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (transform.childCount == PresetManager.Instance.numInstruments)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).GetSiblingIndex() != transform.GetChild(i).GetComponent<InstrumentInfo>().sortOrder)
                {
                    transform.GetChild(i).SetSiblingIndex(transform.GetChild(i).GetComponent<InstrumentInfo>().sortOrder);
                }
            }
        }
	}
}
