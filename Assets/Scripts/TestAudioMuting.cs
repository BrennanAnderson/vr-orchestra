﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAudioMuting : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {
        Fabric.EventManager.Instance.PostEvent("High", gameObject);
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            Fabric.EventManager.Instance.PostEvent("High", Fabric.EventAction.SetVolume, 0f, gameObject);
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            Fabric.EventManager.Instance.PostEvent("High", Fabric.EventAction.SetVolume, 1f, gameObject);
            Debug.Log("UNMUTE");
        }
	}
}
