﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(SetTouched))]
[RequireComponent(typeof(VRTK.VRTK_InteractableObject))]
[RequireComponent(typeof(VRTK.VRTK_InteractHaptics))]

public class ToolTipItem : MonoBehaviour {

    public Text toolNameText;
    public Text toolTipText;
    public string toolNameString;
    public string toolTipString;
    public bool isTrashCan = false;
	public bool displayOnRight = false;
    public bool displayAbove = false;
	private GameObject mobileToolTipPrefab;
	private bool mobileTipSpawned = false;
	private GameObject tempMobileTip;
	private float delayTime = 0.75f;
	private bool canSpawnMobileTip = false;
	private GameObject cameraObject;
	private Vector3 toolTipOffset;

	// Use this for initialization
	void Start () 
    {
		cameraObject = ControllersTracker.Instance.cameraRig.gameObject;
		mobileToolTipPrefab = Resources.Load("MobileToolTip") as GameObject;
        GetComponent<VRTK.VRTK_InteractHaptics>().strengthOnTouch = 0.1f;
        GetComponent<VRTK.VRTK_InteractHaptics>().durationOnTouch = 0.01f;
        if (!isTrashCan)
        {
            GetComponent<BoxCollider>().isTrigger = true;
            GetComponent<BoxCollider>().size = GetComponent<RectTransform>().sizeDelta;
        }
        toolTipOffset = (cameraObject.transform.position - transform.position).normalized;
        //toolNameText = ToolTipManager.Instance.toolNameText;
        //toolTipText = ToolTipManager.Instance.toolTipText;
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (GetComponent<SetTouched>().objectTouched)
        {
            //toolNameText.text = toolNameString;
            //toolTipText.text = toolTipString;

			if (!mobileTipSpawned)
			{
				StartCoroutine("SpawnMobileToolTip");
			}
        }
        else
        {
			if (mobileTipSpawned)
			{
				StartCoroutine("DeleteMobileToolTip");
			}
        }
	}

	IEnumerator SpawnMobileToolTip ()
	{
		StopCoroutine("DeleteMobileToolTip");
		if (tempMobileTip != null)
		{
			Destroy (tempMobileTip);
		}
		mobileTipSpawned = true;
		yield return new WaitForSeconds(delayTime);
		tempMobileTip = Instantiate(mobileToolTipPrefab);
		tempMobileTip.transform.localScale = Vector3.zero;
        tempMobileTip.transform.rotation = transform.rotation;
		tempMobileTip.transform.GetChild(0).GetComponent<Canvas>().sortingOrder = 1;
		
        if (displayAbove)
        {
            tempMobileTip.transform.position = transform.position + (toolTipOffset * 0.2f) + (transform.right * 0f) + (transform.up * 0.75f);
        }
        else if (displayOnRight)
		{
            tempMobileTip.transform.position = transform.position + (toolTipOffset * 0.2f) + (transform.right * 1f) + (-transform.up * 0f);
		}
		else
		{
            tempMobileTip.transform.position = transform.position + (toolTipOffset * 0.2f) + (-transform.right * 1f) + (-transform.up * 0f);
		}
		
		tempMobileTip.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = toolNameString;
		tempMobileTip.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = toolTipString;

		float scaleUpTime = 0.5f;
		float targetTime = Time.time + scaleUpTime;
		float t = 0f;
		while (Time.time < targetTime)
		{
			t += Time.deltaTime/scaleUpTime;
			tempMobileTip.transform.localScale = Vector3.Lerp(tempMobileTip.transform.localScale, Vector3.one, t);
			yield return null;
		}
		yield return null;
	}

	IEnumerator DeleteMobileToolTip ()
	{
		StopCoroutine("SpawnMobileToolTip");
		mobileTipSpawned = false;
		float scaleDownTime = 0.5f;
		float targetTime = Time.time + scaleDownTime;
		float t = 0f;
		while (Time.time < targetTime)
		{
			t += Time.deltaTime/scaleDownTime;
			if (tempMobileTip != null)
			{
				tempMobileTip.transform.localScale = Vector3.Lerp(tempMobileTip.transform.localScale, Vector3.zero, t);
			}
			yield return null;
		}
		if (tempMobileTip != null)
		{
			Destroy (tempMobileTip);
		}
		yield return null;
	}

    void OnDisable ()
    {
        mobileTipSpawned = false;
        if (tempMobileTip != null)
        {
            Destroy(tempMobileTip);
        }
    }
}
