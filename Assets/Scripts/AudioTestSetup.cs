﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class AudioTestSetup : MonoBehaviour {
	
	private FileInfo[] info;
	private DirectoryInfo dir;
	private GameObject groupObject;

	public int numEvents;
	public GameObject fabricObject;
	public GameObject instrumentObject;
	public Transform instruments;
	public GameObject groupComponent;

	void Awake ()
	{
		dir = new DirectoryInfo(Application.dataPath + "/StreamingAssets");
		info = dir.GetFiles("*.wav");
		groupObject = Instantiate(groupComponent) as GameObject;

		groupObject.GetComponent<Fabric.GroupComponent>().UnregisterWithMainHierarchy();
		for (int i = 0; i < numEvents; i++)
		{
			Fabric.EventManager.Instance._eventList.Add(i.ToString());
			GameObject newFabricObject = Instantiate (fabricObject) as GameObject;
			newFabricObject.transform.SetParent(groupObject.transform);
			newFabricObject.name = i.ToString();

			// Add 1 to the index, because Fabric's first item in the list is always "_unset_"
			newFabricObject.GetComponent<Fabric.EventListener>()._eventName = Fabric.EventManager.Instance._eventList[i + 1];

			newFabricObject.GetComponent<Fabric.WwwAudioComponent>().AudioClipReference = info[0].Name;
		}

		groupObject.GetComponent<Fabric.GroupComponent>().RegisterWithMainHierarchy();

//		for (int i = 0; i < numEvents; i++)
//		{
//			GameObject newInstrument = Instantiate(instrumentObject) as GameObject;
//			newInstrument.transform.SetParent(instruments);
//			newInstrument.name = "Instrument_" + i.ToString();
//		}
	}

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Return))
		{
			Fabric.EventManager.Instance.PostEvent("0");
			//Debug.Log("SPACE");
		}
	}
}
