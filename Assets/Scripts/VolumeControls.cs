﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeControls : MonoBehaviour {

	public bool mute = false;
	public bool solo = false;
	public bool audible = true;
	public bool tempUnSolo = false;
	public bool tempUnMute = false;

	public GameObject soloParticle;
	public GameObject muteParticle;
	public GameObject burstParticle;

	public void MuteInstrument ()
	{
		if (solo && !mute)
		{
			solo = false;
			VolumeManager.Instance.numberSoloInstruments--;
		}
				
		if (mute)
		{
            mute = false;
			VolumeManager.Instance.numberMutedInstruments--;
		}
		else
		{
            mute = true;
			VolumeManager.Instance.numberMutedInstruments++;
			Destroy(Instantiate(muteParticle,transform), 0.2f);

		}

		VolumeManager.Instance.CheckAudible();
	}


	public void SoloInstrument ()
	{
		if (solo)
		{
			solo = false;
			VolumeManager.Instance.numberSoloInstruments--;
		}
		else
		{
			solo = true;
			VolumeManager.Instance.numberSoloInstruments++;
			Destroy(Instantiate(soloParticle,transform), 0.2f);
		}

		VolumeManager.Instance.CheckAudible();
	}
}
