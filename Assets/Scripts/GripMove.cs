﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GripMove : MonoBehaviour {

    public DeleteInstrument deleteInstrument;

    public Transform rightController;
    public Transform leftController;
    VRTK.VRTK_ControllerEvents rightControllerEvents;
    VRTK.VRTK_ControllerEvents leftControllerEvents;

    private Transform grabbingControllerTransform;
    private Transform previousParent;
    public bool activateInGrabModeOnly = false;

    void Awake()
    {

    }

    // Use this for initialization
    void Start()
    {
        leftController = ControllersTracker.Instance.leftController;
        rightController = ControllersTracker.Instance.rightController;
        rightControllerEvents = rightController.GetComponent<VRTK.VRTK_ControllerEvents>();
        leftControllerEvents = leftController.GetComponent<VRTK.VRTK_ControllerEvents>();
        
        rightControllerEvents.GripPressed += new VRTK.ControllerInteractionEventHandler(DoGripClicked);
        rightControllerEvents.GripReleased += new VRTK.ControllerInteractionEventHandler(DoGripUnclicked);

        leftControllerEvents.GripPressed += new VRTK.ControllerInteractionEventHandler(DoGripClicked);
        leftControllerEvents.GripReleased += new VRTK.ControllerInteractionEventHandler(DoGripUnclicked);

        previousParent = transform.parent;
        
    }

    void Update()
    {

    }

    private void DoGripClicked(object sender, VRTK.ControllerInteractionEventArgs e)
    {
        if (this != null && this.enabled)
        {
            if (activateInGrabModeOnly)
            {
                if (ModeManager.Instance.GetControllerMode() == ModeManager.controllerModeEnum.grabMode)
                {
                    transform.SetParent(e.controllerReference.model.transform);

                    if (deleteInstrument != null)
                    {
                        deleteInstrument.enabled = false;
                    }

                    grabbingControllerTransform = e.controllerReference.model.transform;
                }
            }
            else
            {
                if (ModeManager.Instance.GetControllerMode() != ModeManager.controllerModeEnum.grabMode)
                {
                    transform.SetParent(e.controllerReference.model.transform);

                    if (deleteInstrument != null)
                    {
                        deleteInstrument.enabled = false;
                    }

                    grabbingControllerTransform = e.controllerReference.model.transform;
                }
            }
        }
    }

    private void DoGripUnclicked(object sender, VRTK.ControllerInteractionEventArgs e)
    {
        if (this != null && this.enabled)
        {
            if (activateInGrabModeOnly)
            {
                if (ModeManager.Instance.GetControllerMode() == ModeManager.controllerModeEnum.grabMode)
                {
                    if (e.controllerReference.model.transform == grabbingControllerTransform)
                    {
                        transform.SetParent(previousParent);
                        if (deleteInstrument != null)
                        {
                            deleteInstrument.enabled = true;
                        }
                    }
                }

            }
            else
            {
                if (ModeManager.Instance.GetControllerMode() != ModeManager.controllerModeEnum.grabMode)
                {
                    if (e.controllerReference.model.transform == grabbingControllerTransform)
                    {
                        transform.SetParent(previousParent);
                        if (deleteInstrument != null)
                        {
                            deleteInstrument.enabled = true;
                        }
                    }
                }
            }
        }
    }
}
