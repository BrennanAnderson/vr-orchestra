﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomationNodeInfo : MonoBehaviour {

	public float timestamp = 0f;
	public int nodePosition = 0;
    public GameObject myInstrumnent;
    public Vector3 lastKnownPosition;

	// Use this for initialization
	void Start () 
    {
        lastKnownPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (transform.position != lastKnownPosition)
        {
            lastKnownPosition = transform.position;
        }
	}
}
