﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimationSpeed : MonoBehaviour {

	Animator animator;

	// Use this for initialization
	void Start () 
	{
		animator = GetComponent<Animator>();
		animator.speed = ColorManager.Instance.instrumentAnimationSpeed;// + Random.Range(0f, 0.2f);

		// TODO - write a formula to sync animation speed to tempo. Will require maths!
	}
}
