﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class ReverbControl : MonoBehaviour {

    public AudioMixer audioMixer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetWetMix (Slider slider)
    {
        audioMixer.SetFloat("ReverbAmount", slider.value);
    }

    public void SetDecayTime(Slider slider)
    {
        audioMixer.SetFloat("DecayTime", slider.value);
    }

    public void SetVolume(Slider slider)
    {
        audioMixer.SetFloat("MasterVolume", slider.value);
    }
}
