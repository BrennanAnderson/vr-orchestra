﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{

    public bool isMoving = false;

    private bool isPickedUp = false;
    private float movementSpeed = 0.05f;
    private float telescopeSpeed = 5f;
    private float startTime;
    private float journeyLength;
    private Transform startMarker;

    public void MoveTo(Transform targetPosition)
    {
        if (!isMoving)
        {
            StartCoroutine(MoveCoroutine(targetPosition.position));

            if (GetComponent<BoxCollider>().enabled)
            {
                GetComponent<BoxCollider>().enabled = false;
            }
        }
    }

    public void MoveTo(Vector3 targetPosition)
    {
        if (!isMoving)
        {
            StartCoroutine(MoveCoroutine(targetPosition));

            if (GetComponent<BoxCollider>().enabled)
            {
                GetComponent<BoxCollider>().enabled = false;
            }
        }
    }

    IEnumerator MoveCoroutine(Vector3 targetPosition)
    {
        isMoving = true;
        startTime = Time.time;
        journeyLength = Vector3.Distance(gameObject.transform.position, targetPosition);
        startMarker = gameObject.transform;

        ColorManager.Instance.FadeColor(GetComponent<SpriteRenderer>(), .5f, 0.2f);

        while (gameObject.transform.position != targetPosition)
        {
            float distCovered = (Time.time - startTime) * movementSpeed;
            float fracJourney = distCovered / journeyLength;
            transform.position = Vector3.Lerp(startMarker.position, targetPosition, fracJourney);
            yield return null;
        }
        VolumeManager.Instance.CheckAudible();

        if (!GetComponent<BoxCollider>().enabled)
        {
            GetComponent<BoxCollider>().enabled = true;
        }
        isMoving = false;
    }

    // TODO - At higher speeds, this makes the controller select and deselect it rapidly. Find a way to fix this so we can move instrument at high speeds

    public void MoveAway(Transform controller)
    {
        Vector3 direction = transform.position - controller.position;
        if (direction.sqrMagnitude < 200f)
        {
            transform.position += controller.forward * Time.deltaTime * telescopeSpeed;
            VRTK.VRTK_ControllerReference controllerReference = VRTK.VRTK_ControllerReference.GetControllerReference(controller.parent.GetChild(1).gameObject);
            VRTK.VRTK_ControllerHaptics.TriggerHapticPulse(controllerReference, 0.05f, 0.01f, 0.5f);
        }
    }

    public void MoveClose(Transform controller)
    {
        Vector3 direction = transform.position - controller.position;

        if (direction.sqrMagnitude > 1f)
        {
            transform.position -= controller.forward * Time.deltaTime * telescopeSpeed;
            VRTK.VRTK_ControllerReference controllerReference = VRTK.VRTK_ControllerReference.GetControllerReference(controller.parent.GetChild(1).gameObject);
            VRTK.VRTK_ControllerHaptics.TriggerHapticPulse(controllerReference, 0.05f, 0.01f, 0.5f);
        }
    }
}