﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableInstrument :  VRTK.VRTK_InteractableObject {

    private ModeManager.controllerModeEnum controllerMode = ModeManager.controllerModeEnum.grabMode;

    // Use this for initialization
    void Start()
    {
        controllerMode = ModeManager.Instance.GetControllerMode();
    }

    // Update is called once per frame
    void Update()
    {
        CheckControllerMode();
    }

    // TODO - make this into an event so I don't need to check in update
    void CheckControllerMode()
    {
        if (controllerMode != ModeManager.Instance.GetControllerMode())
        {
            controllerMode = ModeManager.Instance.GetControllerMode();
        }
    }

    public override void StartUsing(VRTK.VRTK_InteractUse usingObject)
    {
        base.StartUsing(usingObject);
        Use();
    }

    public void Use ()
    {
        switch (controllerMode)
        {
            case ModeManager.controllerModeEnum.grabMode:
                break;

            case ModeManager.controllerModeEnum.parameterMode:
                break;

            case ModeManager.controllerModeEnum.automationMode:
                if (AutomationManager.Instance.GetSelectedInstrument() == gameObject)
                {
                    //GetComponent<AutomationData>().CreateAutomationNode();
                }
                else
                {
                    AutomationManager.Instance.SetSelectedInstrument(gameObject);
                }
                
                break;
        }
    }
}
