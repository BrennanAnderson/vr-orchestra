﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortNodes : MonoBehaviour {

    string instrumentName;
    public int numNodes;
    Transform instrumentToCheck;

	// Use this for initialization
	void Start () 
    {
        instrumentName = transform.name.Replace("_AutomationNodes", "");
        instrumentToCheck = PresetManager.Instance.instrumentsContainer.Find(instrumentName);
	}

    // Update is called once per frame
    void Update()
    {
        if (instrumentToCheck != null)
        {
            if (instrumentToCheck.GetComponent<AutomationData>() != null)
            {
                numNodes = instrumentToCheck.GetComponent<AutomationData>().numNodes;
                if (transform.childCount == numNodes)
                {
                    for (int i = 0; i < transform.childCount; i++)
                    {
                        if (transform.GetChild(i).GetSiblingIndex() != transform.GetChild(i).GetComponent<AutomationNodeInfo>().nodePosition)
                        {
                            transform.GetChild(i).SetSiblingIndex(transform.GetChild(i).GetComponent<AutomationNodeInfo>().nodePosition);
                        }
                    }
                }
            }
        }
    }
}
