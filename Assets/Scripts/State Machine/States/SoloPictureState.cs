﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloPictureState : StateMachineBehaviour {

	private Animator myAnimator;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		TutorialManager.Instance.tutorialUI[9].SetActive(true);
		TutorialManager.Instance.tutorialUI[10].SetActive(true);
		myAnimator = animator;
		VRInput.Instance.OnSwipe += Swipe;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		TutorialManager.Instance.tutorialUI[9].SetActive(false);
		TutorialManager.Instance.tutorialUI[10].SetActive(false);
		TutorialManager.Instance.EnableInstruments();
		VRInput.Instance.OnSwipe -= Swipe;
		animator.SetBool("canProgress", false);
	}

	void Swipe (VRInput.SwipeDirection direction)
	{
		if (direction == VRInput.SwipeDirection.UP)
		{
			myAnimator.SetBool("canProgress", true);
		}
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
