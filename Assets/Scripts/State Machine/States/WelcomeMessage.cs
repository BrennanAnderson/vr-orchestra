﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WelcomeMessage : StateMachineBehaviour {


	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
	{
		TutorialManager.Instance.tutorialUI[0].SetActive(true);
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			//animator.SetBool("canProgress", true);
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
	{
		//animator.SetBool("canProgress", false);
		//Vector3 pos = TutorialManager.Instance.tutorialUI[0].GetComponent<RectTransform>().localPosition;
		//pos = new Vector3 (0, 350, 0);
		//TutorialManager.Instance.tutorialUI[0].GetComponent<RectTransform>().localPosition = pos;
		TutorialManager.Instance.tutorialUI[0].SetActive(false);
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
