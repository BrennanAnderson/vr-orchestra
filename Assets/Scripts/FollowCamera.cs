﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform cameraRig;
    public float uiDistance = 5f;
    public float lerpSpeed;
    public float minY = -1f;
    public float maxY = 1f;

    void Awake ()
    {

    }

    // Use this for initialization
    void Start()
    {
        cameraRig = ControllersTracker.Instance.cameraRig;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void LateUpdate()
    {
        Quaternion cameraRot = cameraRig.rotation;
        Vector3 uiDir = Quaternion.Euler(cameraRot.eulerAngles.x, cameraRot.eulerAngles.y, 0) * transform.parent.forward;
        Vector3 newPos = cameraRig.position + uiDir * uiDistance;
        Vector3 clampedPos = new Vector3( newPos.x, Mathf.Clamp(newPos.y, minY, maxY), newPos.z);

        // TODO - make it so that UI element only moves if the new destination is outside a certain radius
        if (true)
        {
            transform.position = Vector3.Lerp(transform.position, clampedPos, lerpSpeed * Time.deltaTime);
        }
        //transform.position = newPos;
    }
}