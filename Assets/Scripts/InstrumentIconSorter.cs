﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstrumentIconSorter : MonoBehaviour {

    public static InstrumentIconSorter Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public GameObject instrumentIconPrefab;
    public Text instrumentNumberText;
    public Text songNameText;
    public Material greyedOut;
    public Material defaultMaterial;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {

	}

    public void AddIconToSongPanel(string instrumentName)
    {
        Sprite mySprite = null;
        for (int i = 0; i < ImagesList.Instance.sprites.Count; i++)
        {
            if (ImagesList.Instance.sprites[i].name.Replace("_Anim_0", "") == instrumentName)
            {
                mySprite = ImagesList.Instance.sprites[i];
            }
        }
        GameObject instrumentIcon = Instantiate(instrumentIconPrefab) as GameObject;
        instrumentIcon.transform.GetChild(0).GetComponent<Image>().sprite = mySprite; // make this set to the instrument's sprite that called this function
        instrumentIcon.transform.GetChild(1).GetComponent<Text>().text = instrumentName; // Make the text whatever the instrument that spawned this is.
        instrumentIcon.transform.SetParent(transform);
        instrumentIcon.transform.localPosition = new Vector3(0f, 0f, 0f);
        instrumentIcon.transform.localScale = new Vector3(1f, 1f, 1f);
        instrumentIcon.transform.localRotation = Quaternion.identity;
    }

    public void CheckIfInstrumentIsSpawned(string instrumentName)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetChild(1).GetComponent<Text>().text == instrumentName)
            {
                if (transform.GetChild(i).GetChild(0).GetComponent<Image>().material == greyedOut)
                {
                    transform.GetChild(i).GetChild(0).GetComponent<Image>().material = defaultMaterial;
                    break;
                }
            }
        }
    }

    public void RemoveIconFromPanel(string instrumentName)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetChild(1).GetComponent<Text>().text == instrumentName)
            {
                if (transform.GetChild(i).GetChild(0).GetComponent<Image>().material == defaultMaterial)
                {
                    transform.GetChild(i).GetChild(0).GetComponent<Image>().material = greyedOut;
                    break;
                }
            }
        }
    }

    public void SetInfoOnPanel(string songName, int numInstruments)
    {
        songNameText.text = songName;
        instrumentNumberText.text = numInstruments.ToString();

        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public void SpawnAllRequired ()
    {
        StartCoroutine(SpawnAllCoroutine());
    }

    IEnumerator SpawnAllCoroutine ()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetChild(0).GetComponent<Image>().material == InstrumentIconSorter.Instance.greyedOut)
            {
                SpawnInstrument.Instance.SetAudioFile("*Empty*");
                SpawnInstrument.Instance.SetInstrumentString(transform.GetChild(i).GetChild(1).GetComponent<Text>().text);
                SpawnInstrument.Instance.SpawnDontLoad();
            }
            yield return null;
        }
        SongManager.Instance.LoadSong();
    }
}
