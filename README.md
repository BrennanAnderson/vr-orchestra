# VR Orchestra Readme #

## TODOs ##

___

## Automation System: ##

* Render Waveform of current selected audio onto timeline
* Have an option to play back all other automation while you are recording an instrument's automation
* Have a delete all automation button on the instrument
* Have laser pointer turn red when in record mode
* Take you out of record mode if you select a different instrument
* Think about implementing touch automation recording
* Be able to zoom in and out on the timeline
* Have option to show automation when in grab mode, but not edit it (Maybe only show automation when the instrument is highlighted or grabbed???)
* Have a show all automation button. When pressed, it shows all automation. When pressed again it hides all automation except the currently selected instrument's automation
* Be able to save and load automation data as files on each instrument
* Be able to save and load automation data into instrument sets
* Allow scrubbing in timeline
* Have a link button in timeline to link to the audio playback
* Save the Automation movement type in the preset

___

## Parameter Mode: ##

* When in parameter mode, let you switch the active parameter witht he radial pad buttons
* Save and load parameter values with the instrument sets

___

## Regular Stuff: ##

* Make a big header on the controls panel that tells you what mode you are in. Also maybe changee the visuals of the controllers for this as well.
* Contextual menu for the instruments that will let you change the instrument, change the audio file, or load/delete automation data
* Render waveform of playing audio somewhere
* Make a multi-instrument spawner that lets you click a + button to add any number of instruments (like in pro tools when you hit CMD + Shift + N)
* Instead of using the scroll bar when selecting instruments, replace the radial pad with directional buttons and use them to move menu selections
* Make Tutorial(Allow user to exit tutorial at any time).
* Figure out how to restrict access of audio files so that only Unity, and no other program can open them

___

## Back Burner ##

* !!Add checkbox on instrument spawner to not restart all the tracks. This would be for SFX or ambient sounds that you don't want to restart all the instruments when you spawn!! (This is currently not possible because of a bug in Fabric)
* Like actually fix the menu follow script. It should start off being further away from you, and should not be allowed to be too close to you. Make a radius where you can look and it wont follow. Increase the follow speed a bit
* Add backgrounds so that it is clear instruments are moving around a space. Maybe make this an option?

___

VR Orchestra is a VR experience and tool to help the user understand how instruments can be spatialized in different configurations. Users will be able to move the instruments around using different preset configurations, or they will be able to pick up each instrument individually and move them wherever they want.